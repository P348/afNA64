# CastLib3 Integration

This directory shall provide extensions to
[CastLib3](https://github.com/CrankOne/castlib) package, specific for NA64
experiment: additional data, configuration files, extra tasks, etc.

