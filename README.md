# ⚠ This is deprecated and is about to be removed in 2021 ⚠

# afNA64 toolkit

This toolkit is based on the AFrame library and provides a following subset of
tools:
* The analysis pipeline with runtime control
* The event display application
* The GDML rendering util (dummy Geant4 project), with NA64 GDML extensions
support
* The generic-purpose Gean4 simulation project with NA64 GDML extensions
support.

The Geant4 simulation tool here in contradiction of more elaborated
[na64-tools](https://gitlab.cern.ch/P348/na64-tools)
follows the different ideology tending to wider usage of system dependencies.
=======
# AFNA64 Middleware Library {#mainpage}

This project represents a middleware implementing API of
[StromaV](https://github.com/crankone/stromav) library in frame of NA64
experiment. It does provide implementations of data structures specifically for
[NA64 experiment](https://na64.web.cern.ch) by definition of concrete event
classes, data processing tools and so on.

This project can not be used standalone, aside of StromaV library and few
additional libraries defining back-end interfaces and protocols (like
[na64-event-exchange](https://gitlab.cern.ch/P348/na64-event-exchange)
library providing event identification mechanism specifically for NA64).

## CERN SPS NA64 Experiment

Theoretical proposal and experimental setup details can be found
on the [main experiment's page](https://na64.web.cern.ch). Please, refer to
those materials before pointing out any doubtful moments.

This project implements a middleware library defining routines and static data
according to StromaV interfaces. The area of responsibility:
  * Define the interprocess communication and data storage format for both,
    experimental and simulated event object. It is implemented in `.proto`
    file from which multilanguage bindings are generated.
  * Define the detector descriptive digit mapping
  * Define common processors for batch and pipelined event
    processing (in terms of StromaV)
  * Define common statistics I/O interface (in terms of StromaV)

The particular build is defined by libraries that can be found in system.

## <a name="bugs"></a> Issues

Do not hesitate to contact me (Renat) if you're faced with troubles building
the package in different environments. It's probably won't so hard to fix
them, but by effort of our small group it is very hard to foresee outcomes
originating from environment differences.

## <a name="license"></a> License

> Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>,
>                    Bogdan Vasilishin <togetherwithra@gmail.com>
> 
> Permission is hereby granted, free of charge, to any person obtaining a copy of
> this software and associated documentation files (the "Software"), to deal in
> the Software without restriction, including without limitation the rights to
> use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
> the Software, and to permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
> 
> The above copyright notice and this permission notice shall be included in all
> copies or substantial portions of the Software.
> 
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
> IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
> FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
> COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
> IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
> CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## <a name="references"></a> References

   - [A GDML manual](http://gdml.web.cern.ch/GDML/doc/GDMLmanual.pdf).
   - [The p348 experiment porposal](http://arxiv.org/pdf/1312.3309v1.pdf).
   - [Article «New Fixed-Target Experiments to Search for Dark Gauge Forces» / 2009](http://arxiv.org/pdf/0906.0580v1.pdf)
   - [Brief thematical overview up to the state of 2013 «Study of the discovery potential for hidden photon emission at future electron scattering fixed target experiments»](http://arxiv.org/abs/1311.5104)
gives brief considerations of A'-generation used here to perform a signal events.
   - [A Google Protocol Buffers serialization library](https://developers.google.com/protocol-buffers/).
