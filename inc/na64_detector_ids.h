/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**@file detector_ids.h
 * @brief The file providing detector identification table and aux routines.
 *
 * This file defines a x-macro \ref for_all_detectors which defines a designations
 * of detectors used in experiment.
 *
 * There are detector numbers provided with DaqDataDecoding library detector
 * digest mapping mechanisms, which is, however, inconvinient to use in
 * high-level applications due to flat design of those identifiers.
 *
 * Here the union AFR_UniqueDetectorID is introduced in order to perform transparent
 * indexing of detector subsystem.
 */

# ifndef H_AFNA64_INTERNAL_DETECTOR_IDENTIFIER_H
# define H_AFNA64_INTERNAL_DETECTOR_IDENTIFIER_H

# include <StromaV/detector_ids.h>
# include "afNA64_config.h"

# include <stdio.h>

/**\brief Strip number type for APV detectors. */
typedef unsigned short APVStripNo;
/**\brief Wire number type for APV detectors. */
typedef unsigned short APVWireNo;

/**\brief Mapping function type, for certain APV detector and wire number
 * returning strip number(s)
 *
 * For disjoint readouts, returning pointer will refer to single value. For
 * joint ones, the pointer returned refers to array which length is written
 * in value pointed by last argument.
 *
 * Last argument can be NULL. In this case length won't be written there.
 * */
typedef const APVStripNo * (*APVMapper)( APVWireNo, size_t * );

# define P38G4_MJ_DETID_OFFSET 4
/** Should expel last bits after shifting major number << P38G4_MJ_DETID_OFFSET contains
 * family number */
# define P348G4_DETID_FAMILY_MASK 0xff

/**@def for_all_detector_families
 * @brief Detectors major features table.
 *
 * Note, that all the features should be bitwise-shifted --- lower
 * bits are reserved for family numbers.
 * */
# define for_all_detector_features( m )                     \
    /* ft. name|ft code|feature description */              \
    m( Subd,    0x10,   "for SADC: has transversal segmentation"  )   \
    m( Joint,   0x10,   "for APV: has joint wires"      )   \
    m( SADC,    0x20,   "is served by SADC"             )   \
    m( APV,     0x40,   "is served by APV"              )   \
    m( XYSep,   0x80,   "X/Y planes are separated"      )   \
    m( X,       0x100,  "X-plane, when X/Y are separated" ) \
    m( Y,       0x80,   "Y-plane, when X/Y are separated" ) \
    /* ... */

/**@def for_all_detector_families
 * @brief Detectors categories (families) table.
 *
 * Note, that these numbers stored in major section of unique
 * detectorID as numbers shifted by 3 bits. Should be below 0x10 value,
 * as >=0x10 corresponds to features bitflags.
 * */
# define for_all_detector_families( m )                          \
    /* label    ,  custom code              , fam name */        \
    m( MISC     ,  0x0                      , "Miscellaneous"  ) \
    m( BGO      ,  0x1 | f_SADC | f_Subd    , "BGO"            ) \
    m( ECAL     ,  0x2 | f_SADC | f_Subd    , "ECAL"           ) \
    m( HCAL     ,  0x3 | f_SADC | f_Subd    , "HCAL"           ) \
    m( HOD      ,  0x4 | f_SADC | f_Subd | f_XYSep, "Hodoscopes" ) \
    m( MUON     ,  0x5 | f_SADC             , "MuonCtrs"       ) \
    m( MuMega   ,  0x6 | f_APV  | f_Joint | f_XYSep, "Micromegas" ) \
    m( GEM      ,  0x7 | f_APV  | f_XYSep   , "GEMs"           ) \
    m( WCAL     ,  0x8 | f_SADC | f_Subd    , "W-calorimeter"  ) \
    /*m( Misc     ,  0x9                      , "Miscellaneous"  )*/ \
    /*m( StrawTubs,  0xa              ,  "Straw Tubes")*/ \
    /* ... */

# define for_all_SADC_detectors( m ) \
    m( BGO      ,((fam_BGO  << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x6  ) \
    /* ECAL family */ \
    m( ECAL0    ,(((fam_ECAL)         << P38G4_MJ_DETID_OFFSET) | 0x1) , 0xc  ) \
    m( ECAL1    ,(((fam_ECAL)         << P38G4_MJ_DETID_OFFSET) | 0x2) , 0xca ) \
    m( ECALSUM  ,(((fam_ECAL)         << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x2d ) \
    /* HCAL family */ \
    m( HCAL0    ,(((fam_HCAL)         << P38G4_MJ_DETID_OFFSET) | 0x0) , 0xcc ) \
    m( HCAL1    ,(((fam_HCAL)         << P38G4_MJ_DETID_OFFSET) | 0x1) , 0xcd ) \
    m( HCAL2    ,(((fam_HCAL)         << P38G4_MJ_DETID_OFFSET) | 0x2) , 0xce ) \
    m( HCAL3    ,(((fam_HCAL)         << P38G4_MJ_DETID_OFFSET) | 0x3) , 0xcf ) \
    /* Hodoscopes family */ \
    m( HOD0X    ,(((fam_HOD | f_X)    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x2  ) \
    m( HOD0Y    ,(((fam_HOD | f_Y)    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x3  ) \
    m( HOD1X    ,(((fam_HOD | f_X)    << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x4  ) \
    m( HOD1Y    ,(((fam_HOD | f_Y)    << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x5  ) \
    /* Muon counters */ \
    m( MUON0    ,(((fam_MUON)         << P38G4_MJ_DETID_OFFSET) | 0x0) , 0x29 ) \
    m( MUON1    ,(((fam_MUON)         << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x2a ) \
    m( MUON2    ,(((fam_MUON)         << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x2b ) \
    m( MUON3    ,(((fam_MUON)         << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x2c ) \
    /* Tungsten calorimeter (VCAL) */ \
    m( WCAL     ,(((fam_WCAL)         << P38G4_MJ_DETID_OFFSET) | 0x0) , 0x41 ) \
    m( VTWC     ,(((fam_WCAL)         << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x42 ) \
    m( VTEC     ,(((fam_WCAL)         << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x43 ) \
    /* Beam counters */ \
    m( S1       ,(((0x0 | f_SADC )    << P38G4_MJ_DETID_OFFSET) | 0x0) , 0x45 ) \
    m( S2       ,(((0x0 | f_SADC )    << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x46 ) \
    /* Miscellaneous */ \
    m( ATARG    ,(((0x0 | f_SADC | f_Subd ) << P38G4_MJ_DETID_OFFSET) | 0x0) , 0x7  ) \
    m( TRIG     ,(((0x0 | f_SADC | f_Subd ) << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x0  ) \
    m( VETO     ,(((0x0 | f_SADC | f_Subd ) << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x24 ) \
    m( SRD      ,(((0x0 | f_SADC | f_Subd ) << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x47 ) \
    m( LYSO     ,(((0x0 | f_SADC | f_Subd ) << P38G4_MJ_DETID_OFFSET) | 0x5) , 0x1 ) \
    /* ... */

# define for_all_APV_detectors( m ) \
    /* Micromegas */ \
    m( MM01X    ,(((fam_MuMega | f_X ) << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x20 ) \
    m( MM01Y    ,(((fam_MuMega | f_Y ) << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x21 ) \
    m( MM02X    ,(((fam_MuMega | f_X ) << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x22 ) \
    m( MM02Y    ,(((fam_MuMega | f_Y ) << P38G4_MJ_DETID_OFFSET) | 0x2) , 0x23 ) \
    m( MM03X    ,(((fam_MuMega | f_X ) << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x1c ) \
    m( MM03Y    ,(((fam_MuMega | f_Y ) << P38G4_MJ_DETID_OFFSET) | 0x3) , 0x1d ) \
    m( MM04X    ,(((fam_MuMega | f_X ) << P38G4_MJ_DETID_OFFSET) | 0x4) , 0x1e ) \
    m( MM04Y    ,(((fam_MuMega | f_Y ) << P38G4_MJ_DETID_OFFSET) | 0x4) , 0x1f ) \
    /* GEMs (TODO: DDD codes)  */ \
    m( GM01X1__ ,((( fam_GEM | f_X )   << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x8  ) \
    m( GM01Y1__ ,((( fam_GEM | f_Y )   << P38G4_MJ_DETID_OFFSET) | 0x1) , 0x9  ) \
    m( GM02X1__ ,((( fam_GEM | f_X )   << P38G4_MJ_DETID_OFFSET) | 0x2) , 0xa  ) \
    m( GM02Y1__ ,((( fam_GEM | f_Y )   << P38G4_MJ_DETID_OFFSET) | 0x2) , 0xb  ) \
    /* ... */

# define for_all_major_indexes( m, M ) \
    m( "0",         0x0,    M(ECAL), M(MUON), M(HCAL), M(HOD) ) \
    m( "1",         0x1,    M(ECAL), M(MUON), M(HCAL), M(MuMega), M(GEM), M(MISC), M(HOD) ) \
    m( "2",         0x2,    M(MUON), M(HCAL), M(MuMega), M(GEM), M(MISC) ) \
    m( "3",         0x3,    M(MUON), M(HCAL), M(MuMega) ) \
    m( "X",         EnumScope::DetectorFeatures::f_X, M(HOD), M(GEM), M(MuMega) ) \
    m( "Y",         EnumScope::DetectorFeatures::f_Y, M(HOD), M(GEM), M(MuMega) ) \
    m( "SUM",       0x3,    M(ECAL) ) \
    /* ... */

/**@def for_all_detectors
 * @brief Detectors enumeration table.
 *
 * This table indexes major detector IDs specific for each detector. Additional
 * detector indexing (like x,y in DDD) is provided in minor number.
 * See AFR_UniqueDetectorID
 *
 * Note, that native code is usually been corrected by mapping file, so native
 * DDD-code provided here are just some default ones.
 * */
# define for_all_detectors( m )                                                             \
    /* label    ,  custom code                                                  , ddd code */      \
    m( unknown  ,   0x0                                                          , -1   )   \
    for_all_SADC_detectors( m )                                                             \
    for_all_APV_detectors( m )                                                              \
    /* m( ST01Y    ,                   ) */         \
    /* m( ST02X    ,                   ) */         \
    /* m( ST02Y    ,                   ) */         \
    /* m( ST03X1   ,                   ) */         \
    /* m( ST03X2   ,                   ) */         \
    /* m( ST03X3   ,                   ) */         \
    /* m( ST03Y1   ,                   ) */         \
    /* m( ST04X1   ,                   ) */         \
    /* m( ST04X2   ,                   ) */         \
    /* m( ST04Y1   ,                   ) */         \
    /* ... */


/**@def DETID_SCOPE_QUALIFIER
 * An C/C++ variable macro resolving the enum prefix. */
# ifdef __cplusplus
#   define DETID_SCOPE_QUALIFIER EnumScope::
extern "C" {
# else
#   define DETID_SCOPE_QUALIFIER enum
# endif

/**@struct EnumScope
 * @brief C-enum scope
 *
 * This struct must never be instantiated. It provides a namespace-like
 * scope for C++-code.
 * Since C doesn't support scoped enumname resolution,
 * this struct does not affect to C code.
 */
struct EnumScope {
    /**@enum DetectorFeatures
     * @brief Bitflag describing features upported by certain detector. */
    enum DetectorFeatures {
        # define EX_declare_feature_alias( name, code, description ) \
        f_ ## name = (code),
        for_all_detector_features( EX_declare_feature_alias )
        # undef EX_declare_feature_alias
    } _unused1;
    /**@enum DetectorFeatures
     * @brief Bitflag describing category by which certain detector is affilated. */
    enum DetectorFamilies {
        # define EX_declare_family_alias( name, code, verbName ) \
        fam_ ## name = (code),
        for_all_detector_families( EX_declare_family_alias )
        # undef EX_declare_family_alias
    } _unused2;
    /**@enum MajorDetectorsCode
     * @brief A major detector encoding enumeration.
     *
     * This enum is used to uniquely define a detector family
     * in terms of StromaV library. */
    enum MajorDetectorsCode {
        # define EX_declare_alias( name, code, dddCode ) \
        d_ ## name = (code),
        for_all_detectors( EX_declare_alias )
        # undef EX_declare_alias
    } _unused3;
    /**@enum DDD_DetectorsCode
     * @brief DaqDataDecoding library detector digest codes.
     *
     * This enum's values originally comes from DaqDataDecoding
     * library detector digest encoding and used for mapping from
     * DDD detector codes into StromaV detector codes.
     *
     * One can perform translation via compose_cell_identifier()
     * function. */
    enum DDD_DetectorsCode {
        # define EX_declare_alias( name, code, dddCode ) \
        ddd_ ## name = (dddCode),
        for_all_detectors( EX_declare_alias )
        # undef EX_declare_alias
    } _unused4;
};  /* struct EnumScope */

/** Returned by detector_name_by_code() in case when no detectors corresponds
 * to given code */
extern const char afNA64_global_unknownDetectorString[16];

/** Corrects internal mapping table according to provided
 * information. */
char resolve_major_detector_number_mapping( const char *, unsigned );

/** Produces a AFR_UniqueDetectorID based on this custom code and x/y-subindexes.
 * The formers can be unused for not-subdivided detectors like veto
 * counters. */
AFR_DetSignature compose_cell_identifier( AFR_DetMjNo major, uint8_t xIdx, uint8_t yIdx );
/** Extract index along X axis (came from DDD) from detector identification number. */
uint8_t detector_get_x_idx( union AFR_UniqueDetectorID );
/** Extract index along Y axis (came from DDD) from detector identification number. */
uint8_t detector_get_y_idx( union AFR_UniqueDetectorID );

/** For particular detectors inside families, return subgroup number
 * (e.g. N from HCALN or HCALN -- ECAL0, ECAL1, HCAL3, etc). */
uint8_t detector_subgroup_num( AFR_DetMjNo major );

# define declare_flag_checking_function(name, code, description) \
uint8_t detector_is_ ## name( DETID_SCOPE_QUALIFIER MajorDetectorsCode );
for_all_detector_features( declare_flag_checking_function )
# undef declare_flag_checking_function

/** Returns textual label for detector by its NA64-encoded major ID. */
const char * detector_name_by_code( DETID_SCOPE_QUALIFIER MajorDetectorsCode );
/** Returns textual label for detector by its DaqDecodingLibrary ID. */
const char * ddd_detector_name_by_code( DETID_SCOPE_QUALIFIER DDD_DetectorsCode );
/** Writes full unique detector label (Like ECAL0-1-2) into given buffer. */
char * snprintf_detector_name( char * buffer, size_t bufferSize, union AFR_UniqueDetectorID );
/** Returns detector family identifier number. */
AFR_DetFamID detector_family_num( AFR_DetMjNo major );
/** Returns detector family name (BGO/ECAL/HCAL/Hodoscopes) --- useful in
 * category structures. */
const char * detector_family_name( AFR_DetFamID famNum );
/** Returns detector major number by name. */
AFR_DetMjNo detector_major_by_name( const char * );
/** Returns X-detector ID for given Y-detector ID
 * (useful for detectors with X/Y decomposition) */
AFR_DetSignature detector_complementary_X_id( AFR_DetSignature );
/** Returns Y-detector ID for given X-detector ID
 * (useful for detectors with X/Y decomposition) */
AFR_DetSignature detector_complementary_Y_id( AFR_DetSignature );


/** Converts DDD detector encoding number to NA64's. */
DETID_SCOPE_QUALIFIER MajorDetectorsCode det_code_ddd_to_na64( DETID_SCOPE_QUALIFIER DDD_DetectorsCode );

/** Prints formatted mapping table in file provided by C-descriptor. */
void dump_mapping_table( FILE * );

/** Auxilliary function to test all routines provided in
 * \ref detector_ids.h */
int test_detector_table( FILE * );

/** Returns mapper specific for particular detector. */
APVMapper apv_get_mapper_for( union AFR_UniqueDetectorID );

/* If DSuL StromaV detector selector micro-language, we can enable selectors
 * here: */
# ifdef DSuL
/** Auxilliary function to test DSuL selectors
 * \ref detector_ids.h */
int test_dsul( FILE * );

void dump_selector_expression( FILE *, const struct sV_DSuL_Expression * );
# endif

# ifdef __cplusplus
}  /* extern "C" */
# endif

# endif  /* H_AFNA64_INTERNAL_DETECTOR_IDENTIFIER_H */

