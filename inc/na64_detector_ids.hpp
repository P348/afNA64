/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_DETECTOR_IDENTIFIERS_MAPPER_CLASS_H
# define H_NA64_DETECTOR_IDENTIFIERS_MAPPER_CLASS_H

# include "afNA64_config.h"

# include "na64_detector_ids.h"
# include "analysis/apvConstraints.h"

# include <StromaV/app/analysis.hpp>
# include <StromaV/app/app.h>
# include <StromaV/analysis/xform.tcc>
# include <StromaV/detector_ids.hpp>

# include <unordered_map>

namespace na64 {
namespace aux {

/**@class DetectorMapping
 * @brief Class maintaining all the detectors numerical ID mappings.
 *
 * This singleton keeps information about major mapping schemes between
 * numerical detector IDs (in terms of CORAL/DDD) and afNA64 library,
 * names, APV strips mapping, etc. std::unordered_map is fastest
 * containers when it comes to lookup.
 *
 * todo: Since here we have a lot of repeated data implementing look-up
 * procedures by variaous criteria, one need consider using of boost::multiindex
 * container or (and?) even connection with database(s) used in experiment.
 * todo: Currently, only planar squares is supported for APV mapping.
 * */
class DetectorMapping : public sV::aux::iDetectorIndex {
public:
    enum CorrectionResult : char {
        CORRECTION_NO_PREDEFINED = -1,
        CORRECTION_KEPT = 0,
        CORRECTION_SIMPLE = 1,
        CORRECTION_SWAPPED = 2,
    };
    typedef sV::aux::XForm<2, float> APVBoundaries;
private:
    static DetectorMapping * _self;
    /// Dynamic info --- can be updated with correct_entry().
    std::unordered_map<std::string, unsigned>           _nameToDDDCode;
    /// Dynamic info --- can be updated with correct_entry().
    std::unordered_map<unsigned, AFR_DetMjNo>           _dddCodeToMajor;

    /// Static info --- can not be corrected.
    std::unordered_map<std::string, AFR_DetMjNo>        _nameToMajor;
    /// Static info --- can not be corrected.
    std::unordered_map<AFR_DetMjNo, std::string>        _majorToName;

    std::unordered_map<unsigned, std::string>           _unmapped;

    /// Aux internal structure describing allowed indexing lexical unit.
    struct DetectorMjIndexLexicalEntry {
        unsigned int meaningValue;
        std::unordered_set<AFR_DetFamID> allowedFamilies;
    };

    /// Aux lexical dictionary claiming matching between string index and its
    /// numerical value for particular family.
    std::unordered_map<std::string, DetectorMjIndexLexicalEntry> _indexingLexic;

    std::unordered_map<AFR_DetMjNo, APVMapper> _apvStripMappers;
    std::unordered_map<APVMapper, APVBoundaries> _apvBoundaries;

    size_t _correctionCnt_kept,
           _correctionCnt_simple,
           _correctionCnt_swapped,
           _correctionCnt_unknown
        ;
    /// Internal aux method --- raises nonUniq, when first argument is false.
    void _assert_id_collision( bool, uint8_t, const char * );

    /// Fills _indexingLexic with content of for_all_major_indexes() X-macro.
    void _fill_indexing_lexic();

    DetectorMapping();
protected:
    virtual AFR_DetMjNo _V_mj_code( const char * nameStr ) const override {
        return major_by_name( nameStr );
    }
    virtual const char * _V_name( AFR_DetMjNo mjn ) const override {
        return name_by_major( (EnumScope::MajorDetectorsCode) mjn );
    }
public:

    /// Singleton instance ref acquizition.
    static DetectorMapping & self() {
        if( !_self ) { _self = new DetectorMapping(); }
        return *_self;
    }

    /// Corrects mapping to DDD (CORAL) entries.
    char correct_entry( const char *, unsigned );

    /// Returns major code by name string.
    EnumScope::MajorDetectorsCode major_by_name( const char *, bool warn=true ) const; 

    /// Returns major code by its DDD code.
    EnumScope::MajorDetectorsCode major( EnumScope::DDD_DetectorsCode ) const;

    /// Returns name string by its major code.
    const char * name_by_major( EnumScope::MajorDetectorsCode ) const;

    /// (lib) Associates APV mapping function automatically
    /// obtaining its boundaries (for norming).
    void register_apv_mapper( const std::string &, APVMapper );

    /// Returns associated mapping function.
    APVMapper apv_strip_mapper_for( AFR_DetSignature ) const;

    /// Prints detector table to provided stream instance.
    void dump_table( std::ostream & );

    /// Returns appropriate XForm for APV referenced by its detectors major
    /// number.
    const APVBoundaries & apv_boundaries( EnumScope::MajorDetectorsCode ) const;
};  // class DetectorMapping

}  // namespace aux
}  // namespace afNA64


# endif  // H_NA64_DETECTOR_IDENTIFIERS_MAPPER_CLASS_H

