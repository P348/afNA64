/*
 * Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**@file aprimeHooks.hpp
 * @brief Helpers keeping track of A' particle lifecycle.
 * */

# ifndef H_AFNA64_GEANT4_APRIME_HOOKS_H
# define H_AFNA64_GEANT4_APRIME_HOOKS_H

# include "afNA64_config.h"

# if defined(AFNA64_DPhMC_FOUND) && defined(StromaV_GEANT4_MC_MODEL) && defined( StromaV_GEANT4_DYNAMIC_PHYSICS )

# include "dphmc-config.h"
# include "g4extras/CreateDestroyStats_TA.hpp"
# include "g4extras/APhysics.hpp"

namespace na64 {
namespace aux {

class APrimeCreateSelector :
                    public sV::ParticleStatisticsDictionary::iTrackSelector {
private:
    mutable const G4VProcess * _APrimeProductionProcess;
protected:
    virtual bool _V_matches( const G4Track * trackPtr ) const override;
public:
    APrimeCreateSelector();
};  // class APrimeCreateSelector

/**@brief Aux class; has to be changed in favor of more generic configurable
 *        implementation.
 *
 * This class tracks A' particle tracks destroyed (killed). This is a kludge
 * that further has to be re-implemented using advanced run-time configuration.
 */
class APrimeKill :
                public sV::ParticleStatisticsDictionary::iTrackingStatistics {
public:
    struct LastTrackPoint {
        double posX,
               posY,
               posZ;
        double momentumX,
               momentumY,
               momentumZ;
        double totalEnergy;
        double kineticEnergy;
        double lifeDistance;
        bool outsideWCAL;
        //  G4TrackStatus trackStatus;
    };
protected:
    LastTrackPoint _ltp;
    TTree * _aprimeInfo;
protected:
    void _V_fill_track( const sV::ParticleStatisticsDictionary::iTrackSelector &,
                        const G4Track * ) override;
public:
    APrimeKill();
};  // APrimeKill

}  // namespace aux
}  // namespace na64

# endif  // defined(AFNA64_DPhMC_FOUND) && defined(StromaV_GEANT4_MC_MODEL) && defined( StromaV_GEANT4_DYNAMIC_PHYSICS )

# endif  // H_AFNA64_GEANT4_APRIME_HOOKS_H

