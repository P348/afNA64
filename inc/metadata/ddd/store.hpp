/*
 * Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_AFNA64_DDD_METADATA_INDEXES_STORE_H
# define H_AFNA64_DDD_METADATA_INDEXES_STORE_H

# include "afNA64_config.h"

# ifdef na64ee_SUPPORT_DDD_FORMAT

# include "traits.hpp"

# include <StromaV/metadata/store.tcc>

namespace afNA64 {
namespace md {
namespace ddd {

/**@class iDDDIndexesStorage
 * @brief A look-up interface for getting the appropriate file index.
 *
 * This utility class provides look-up mechanism(s) for getting cached indexing
 * object for particular DaqDataDecoding files. The special case when the file
 * is "chunk" (i.e. the file with raw statistics related to particular run) is
 * considered in dedicated method since its look-up may be made significantly
 * faster.
 *
 * This class can be overriding from within python scripts providing smooth
 * integration within higher level ORM. It is used almost exclusively by the
 * DDDFileInfoType metadata type interface implementation.
 */
class Store : public MDTraits::iDisposableSourceManager,
              public MDTraits::iEventQueryableStore {
public:
    NA64_IMPORT_DDD_MD_TYPE_TRAITS;
public:
    virtual Metadata * get_metadata_for( const SourceID & sid) const override;
    virtual void put_metadata( const SourceID & sid,
                               const Metadata & mdRef ) override;
    virtual iEventSource * source( const SourceID & sid ) override;
    virtual void free_source( iEventSource * ) override;
    virtual bool source_id_for( const EventID &, SourceID &) const override;
    virtual void erase_metadata_for( const SourceID & ) override;
    //virtual void collect_source_ids_for_range(
    //                            const EventID &,
    //                            const EventID &,
    //                            std::list<SubrangeMarkup> &) const override;
};  // class DDDMetadataType

}  // namespace ddd
}  // namespace md
}  // namespace afNA64

# endif  // H_AFNA64_DDD_METADATA_INDEXES_STORE_H

# endif  // na64ee_SUPPORT_DDD_FORMAT

