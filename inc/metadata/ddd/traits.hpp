/*
 * Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_AFNA64_EVENT_METADATA_TYPE_TRAITS_H
# define H_AFNA64_EVENT_METADATA_TYPE_TRAITS_H

# include "afNA64_config.h"

# ifdef na64ee_SUPPORT_DDD_FORMAT

# include <StromaV/metadata/traits.tcc>

# include <na64_uevent.hpp>
# include <na64ee_readout.hpp>

namespace afNA64 {
namespace md {
namespace ddd {

/// Template structure aliasing most of the related types.
typedef ::sV::MetadataTypeTraits<::NA64_UEventID,
                                 na64ee::DDDIndex,
                                 na64ee::ChunkID> MDTraits;

# define NA64_IMPORT_DDD_MD_TYPE_TRAITS                 \
    sV_METADATA_IMPORT_SECT_TRAITS( ::NA64_UEventID,    \
                                    ::na64ee::DDDIndex, \
                                    ::na64ee::ChunkID )

}  // namespace ddd
}  // namespace md
}  // namespace afNA64

# endif  // H_AFNA64_EVENT_METADATA_TYPE_TRAITS_H

# endif  // na64ee_SUPPORT_DDD_FORMAT

