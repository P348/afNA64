/*
 * Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_AFNA64_METADATA_DDD_METADATA_TYPE_H
# define H_AFNA64_METADATA_DDD_METADATA_TYPE_H

# include "afNA64_config.h"

# ifdef na64ee_SUPPORT_DDD_FORMAT

# include <StromaV/metadata/type_cached.tcc>

# include "traits.hpp"

namespace afNA64 {
namespace md {
namespace ddd {

class DDDMetadataType : public MDTraits::iMetadataType {
public:
    sV_METADATA_IMPORT_SECT_TRAITS(::NA64_UEventID,
                                 na64ee::DDDIndex,
                                 na64ee::ChunkID);
private:
    size_t _nDuty;
    FILE * _extractionLogFPtr;
    char * _extractionLogBufferPtr;
    size_t _extractionLogBufferLength;
protected:
    //
    // iTCachedMetadataType implementation:
    virtual bool _V_is_complete( const Metadata & ) const override;
    virtual bool _V_extract_metadata(
                            const SourceID *,
                            iEventSource &,
                            Metadata *&,
                            std::list<iMetadataStore *> stores) const override;
    virtual Metadata * _V_merge_metadata(
                            const std::list<Metadata *> & ) const override;

    virtual bool _V_append_metadata( iEventSource & s,
                                     Metadata & md ) const override;
    virtual void _V_cache_metadata( const SourceID &,
                                const Metadata &,
                                std::list<iMetadataStore *> & ) const override;

    virtual void _V_get_subrange(
                    const EventID & low, const EventID & up,
                    const SourceID & sid,
                    typename Traits::SubrangeMarkup & muRef ) const override;
public:
    DDDMetadataType( size_t nDuty=50, bool dbgLogging=false );
    ~DDDMetadataType();
};  // class DDDMetadataType

}  // namespace ddd
}  // namespace md
}  // namespace afNA64

# endif  // na64ee_SUPPORT_DDD_FORMAT

# endif  // H_AFNA64_METADATA_DDD_METADATA_TYPE_H

