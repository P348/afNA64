/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_ANALYSIS_INTERFACES_H
# define H_NA64_ANALYSIS_INTERFACES_H

# include "afNA64_config.h"

# ifdef StromaV_ANALYSIS_ROUTINES
# include <StromaV/app/analysis.hpp>
# include "na64_uevent.hpp"

namespace na64 {
namespace analysis {

typedef ::na64::events::ExperimentalEvent_Payload NA64ExperimentalEvent;
typedef ::sV::aux::iTExperimentalEventPayloadProcessor<NA64ExperimentalEvent>
                                                            ExpEventProcessor;

/// Processor for SADC profiles in experimental events (shortcut).
class iSADCProcessor : public ExpEventProcessor {
public:
    typedef sV::aux::iEventProcessor::ProcRes ProcRes;
    typedef ExpEventProcessor::Event Event;
    iSADCProcessor( const std::string & pn ) : ExpEventProcessor( pn ) {}
private:
    NA64ExperimentalEvent _reentrantSADCProfile;
protected:
    virtual ProcRes _V_process_event_payload(
            NA64ExperimentalEvent & ) override;
    virtual ProcRes _V_finalize_processing_sadc_event(
            NA64ExperimentalEvent & ) { return RC_ACCOUNTED; }
    virtual ProcRes _V_process_SADC_profile_event(
            ::na64::events::SADC_profile & ) = 0;
};

/// Processor for APV profiles in experimental events (shortcut).
class iAPVProcessor : public ExpEventProcessor {
public:
    typedef sV::aux::iEventProcessor::ProcRes ProcRes;
    typedef ExpEventProcessor::Event Event;
    iAPVProcessor( const std::string & pn ) : ExpEventProcessor( pn ) {}
private:
    ::na64::events::APV_sampleSet _reentrantAPVProfile;
protected:
    virtual ProcRes _V_process_event_payload(
            NA64ExperimentalEvent & ) override;
    virtual ProcRes _V_finalize_processing_apv_event(
            NA64ExperimentalEvent & ) { return RC_ACCOUNTED; }
    virtual ProcRes _V_process_APV_samples(
            ::na64::events::APV_sampleSet & ) = 0;
};

}  // namespace analysis
}  // namespace na64

# endif  // StromaV_ANALYSIS_ROUTINES
# endif  // H_NA64_ANALYSIS_INTERFACES_H

