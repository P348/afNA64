/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_ANALYSIS_PROCESSORS_SADC_WAVEFORM_FITTING_C_H
# define H_NA64_ANALYSIS_PROCESSORS_SADC_WAVEFORM_FITTING_C_H

# include <stdio.h>
# include "sadcWF_supp.h"

# ifdef __cplusplus
extern "C" {
# endif

/**\struct SADCWF_FittingInput
 * \brief SADC waveform representation for fitting input
 *
 * This structure represents a source data necessary for fitting
 * procedures. It include only waveform and its (somewhat)
 * reliability estimation that is not necessary is normed.
 * */
struct SADCWF_FittingInput {
    /**\brief Number of samples (typically =32 for experimental event). */
    SADCSamplesSize n;
    /**\brief Samples array. */
    SADCSamples * samples;
    /**\brief Reliability estimation array --- one number per sample. */
    double * sigma;
};

/**\union SADCWF_FittingFunctionParameters
 * \brief Model parameters union to be provided in \ref SADCWF_FittingFunction.
 *
 * Union is designed to hold both, initial and output fitting parameters.
 * One structure field per choosen fitting function instance.
 *
 * \see SADCWF_FittingFunction
 * */
union SADCWF_FittingFunctionParameters {
    /**\brief Model parameters for Moyal function (Landau PDF approximation). */
    struct MoyalParameters { double p[3], err[3], chisq_dof; } moyalPars;
    /* ... */
};

/**\brief Model function callback type for SADC waveform fitting. */
typedef double (*SADCWF_FittingFunction)( union SADCWF_FittingFunctionParameters *, double );

/**@brief Moyal function computing routine.
 *
 * Calculates M(x) = p1*exp( -( lambda + exp(2.22*(x-p)/p3) )/2 )
 * for given p1, p2, p3, x.
 * */
double moyal( union SADCWF_FittingFunctionParameters *, double x );

/* ... */

/**\brief all-in-one fitting function.
 *
 * One must provide initial parameter values by pre-setting them. It is
 * up to user's code.
 *
 * \param           src input experimantal data.
 * \param mdlFunc   model function pointer.
 * \param mdlPars   model parameters.
 * \param logfile   output file pointer for logging.
 * */
int
fit_SADC_samples(
        const struct SADCWF_FittingInput * src,
        SADCWF_FittingFunction mdlFunc,
        union SADCWF_FittingFunctionParameters * mdlPars,
        FILE * logfile
    );

# ifdef __cplusplus
}
# endif

# endif  /* H_NA64_ANALYSIS_PROCESSORS_SADC_WAVEFORM_FITTING_C_H */

