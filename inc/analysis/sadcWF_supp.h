/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_P348_ANALYSIS_PROCESSORS_SADC_WF_ALIGNMENT_H
# define H_P348_ANALYSIS_PROCESSORS_SADC_WF_ALIGNMENT_H

# include <stdint.h>

typedef float SADCSamples;
typedef uint8_t SADCSamplesSize;

# ifdef __cplusplus
extern "C" {
# endif

/**\struct SADCWFCache
 * \brief Auxilliary caching structure for SADC parameters extration.
 *
 * This data accompanies common characteristics calculation and have
 * quite ancillary meaning, that, however can be sometimes interesting
 * outside the \ref calculate_characteristics() function.
 *
 * For a while keeps first derivative array and so-called "linearity"
 * estimation. "Linearity" is calculated after min/max values are
 * extracted and, in principle, should vanish influence of the
 * pikes on waveform by decreasing its reliability value for fitting
 * procedure. Let \f$y_{mx}, y_{mn}\f$ be absolute max/min values of
 * waveform and \f$y_i\f$ be current value. The "linearity" value than is
 * computed through: \f$l_i = ||y_{i+1} - y_{i-1}|/2 - y_i|/|y_{mx} - y_{mn}|\f$.
 * */
struct SADCWFCache {
    /**\brief Waveform derivative of length lesser than samples length by one. */
    SADCSamples * derivatives;
    /**\brief Linearity parameters array of length equal to samples length. */
    float * linearity;
};

struct SADCWFCharacteristicParameters {
    /**\brief Absolute maximum bin number found in waveform
     *  (no smooth/approx. applied) */
    SADCSamplesSize absMaxNBin,
                    /**\brief Absolute minimum bin number found in waveform
                    *  (no smooth/approx. applied) */
                    absMinNBin;
    /**\brief Absolute maximum value found in waveform
     *  (no smooth/approx. applied) */
    SADCSamples absMaxVal,
                /**\brief Absolute minimum value found in waveform
                 *  (no smooth/approx. applied) */
                absMinVal;
    /**\brief Mean amplitude value. */
    float mean,
          /**\brief Weighted mean value of spectrum (normed, one need to
           * multiply it on samples number to obtain mean value X-position). */
          weightedMean,
          /**\brief Most probable value --- defined as bin on weightedMean. */
          mpv,
          /**\brief Spectrum sum value. */
          sum,
          /**\brief Amplitude standard deviatiom. */
          stdDeviation,
          /**\brief Complex reliability parameter. */
          reliability,
          /**\brief Complex time dispersion parameter. */
          timeDispersion
          ;
    

    struct SADCWFCache * cache;
};

/**\brief Allocates new cache instance for \ref SADCWFCharacteristicParameters .*/
void allocate_SADCWF_cache( struct SADCWFCharacteristicParameters * p, SADCSamplesSize nnodes );
/**\brief Deletes cache instance from \ref SADCWFCharacteristicParameters .*/
void free_SADCWF_cache( struct SADCWFCharacteristicParameters * p, SADCSamplesSize nnodes );

/**\func calculate_characteristics
 * \brief Obtains common characteristics of SADC waveform.
 *
 * Provides sequential extraction of common SADC-WF parameters from
 * experimaental SADC waveform. Can, sometimes, be interrupted with undefined
 * result in output struct. In that cases, return code is negative and
 * output structure values must not be further in consideration.
 *
 * \param samples   input data (SADC waveform with, optionally, reliability parameters)
 * \param nSamples  number of samples
 * \param p         allocated instance for obtained parameters be written into.
 *
 * \returns  0  when all parameters extracted normally.
 * \returns -1  on generic algorithmic error: unexpected/nan/inf value;
 * \returns -2  on negative sum case (possibly bad pedestals);
 * \returns -3  on negative linearity max value (entire spectra below x axis, possibly bad pedestals);
 * */
int calculate_characteristics(
        const SADCSamples * samples,
        const SADCSamplesSize nSamples,
        struct SADCWFCharacteristicParameters * p );

# ifdef __cplusplus
}
# endif

# endif  /* H_P348_ANALYSIS_PROCESSORS_SADC_WF_ALIGNMENT_H */

