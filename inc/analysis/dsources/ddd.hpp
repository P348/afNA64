/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_AFNA64_DDD_BASIC_READER_H
# define H_AFNA64_DDD_BASIC_READER_H

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(na64ee_SUPPORT_DDD_FORMAT)

# include <StromaV/app/analysis.hpp>
# include <StromaV/analysis/pipeline.hpp>
# include <StromaV/analysis/evSource_sectional.tcc>
# include <StromaV/utils.h>

# include <DaqEventsManager.h>
# include <DaqEvent.h>

# include <na64_event_id.hpp>
# include <na64ee_readout.hpp>
# include "na64_uevent.hpp"
# include "metadata/ddd/traits.hpp"
# include "na64_detector_ids.h"

# define BENCHMARK_TRANSCODING  // Uncomment it to measure transcoding time.

# ifdef BENCHMARK_TRANSCODING
#   include <time.h>
# endif

namespace afNA64 {
namespace dsources {
namespace ddd {

namespace aux {

/**@class EventsTranscoder
 * @brief Aux class for decoding events from DDD to StromaV/afNA64.
 *
 * Performs transcoding
 *
 * In DDD detectors are encoded by short string (e.g. `ECAL', `TT', etc). 
 * afNA64 declares a singleton dictionary for mapping between this strings and
 * numeric IDs. However, since the mapping can change from run to run, it is
 * desirable to introduce some additional detector ID mapping checks.
 *
 * Transcoding of certain chips can be omitted to enhace the performance. When
 * one deals only with a specific detector family (detectors served by certain
 * chips family) this feature gets sense.
 * */
class EventsTranscoder {
public:
    typedef na64::events::ExperimentalEvent_Payload ExpEventPayload;
    typedef typename sV::AnalysisPipeline::Event Event;
    # define for_each_transcoding_chip( m )         \
        m( 0x1,     SADC,       "Sampling ADC" )    \
        m( 0x2,     APV,        "Analog Pipeline" ) \
        m( 0x3,     F1,         "F1" )              \
        /* ... */
    enum ChipType
    # ifndef SWIG
    : uint8_t
    # endif
    {
        # define expand_enum_element( code, name, desc ) eChip ## name = code,
        for_each_transcoding_chip( expand_enum_element )
        # undef expand_enum_element
    };
private:
    /// Aux map controlling correctness of detector IDs mapping.
    std::unordered_map<unsigned, std::string> _detIDMap;
    std::unordered_map<unsigned, EnumScope::MajorDetectorsCode> _detTranslationDict;

    ExpEventPayload * _reentrantExperimentalPayload;

    /// Ptr to manager instance (owned or not).
    CS::DaqEventsManager * _managerPtr;
    /// True, if this instance owns the manager pointed by _managerPtr;
    bool _ownsManager,
         _treatNonPhysical,
         _detectorsTableValidated,
         _doDetectorNameValidation,
         _keepRaw
         ;
    uint8_t _chipIgnoreMask;
    size_t _eventsNonPhysicalOmitted,
           _eventsNonPhysicalProcessed,
           _eventsDecodingFailureOmitted,
           _digitsProcessed,
           _digitsIgnored,
           _digitsDataTranslationUnimplemented
           ;
protected:
    /// Transcoding method for non-physical events.
    bool _transcode_non_physical( Event *& eventPtr,
                               CS::DaqEvent & event );

    /// Initialize transcoder --- delegated ctr.
    EventsTranscoder( CS::DaqEventsManager * extMngrPtr,
                      bool ownsManager,
                      bool keepRaw ) :
            _reentrantExperimentalPayload(
                    google::protobuf::Arena::CreateMessage<ExpEventPayload>(
                                        sV::mixins::PBEventApp::arena_ptr()) ),
            _managerPtr( extMngrPtr ),
            _ownsManager( ownsManager ),
            _treatNonPhysical( true ),
            _detectorsTableValidated( false ),
            _doDetectorNameValidation( true ),
            _keepRaw( keepRaw ),
            _chipIgnoreMask(0x0) { reset_counters(); }
public:
    /// Initialize transcoder with embedded manager instance.
    EventsTranscoder( const std::string & mapsDirPath, bool keepRaw );

    /// Initialize transcoder around existing manager instance.
    EventsTranscoder( CS::DaqEventsManager & extMngrPtr, bool keepRaw );

    /// Dtr. Clears the DaqEventsManager instance if it was embedded.
    virtual ~EventsTranscoder();

    /// Re-initializes detector dictionary with given maps dir.
    virtual void set_up_detector_mapping();

    /// Common transcoding method.
    bool transcode( Event *& eventPtr, CS::DaqEvent & event );

    /// Payload cache getter (mutable);
    ExpEventPayload & experimental_payload_ref()
            { return *_reentrantExperimentalPayload; };

    /// Payload cache getter (const);
    const ExpEventPayload & experimental_payload_ref() const
            { return *_reentrantExperimentalPayload; };

    /// Returns persistant ref to associated DaqEventsManager instance.
    CS::DaqEventsManager & get_ddd_manager();

    /// Returns persistant ref to associated DaqEventsManager instance (const).
    const CS::DaqEventsManager & get_ddd_manager() const;

    /// Sets all brief statistics counters to 0.
    void reset_counters();

    /// Toggles whether to enable non-physical events transcoding.
    void non_phys_transcode( bool v ) { _treatNonPhysical = v; }

    /// Returns whether to perfom non-physical events transcoding.
    bool non_phys_transcode() const { return _treatNonPhysical; }

    //
    // Chips transcoding omission

    /// Marks chip to be ignored.
    void omit_chip_transcoding( ChipType ct )
        { _chipIgnoreMask |= ((uint8_t) ct); }
    
    /// Clears `ignore chip' flag.
    void perform_chip_transcoding( ChipType ct )
        { _chipIgnoreMask &= ~((uint8_t) ct); }

    /// Returns true if chip has to be omitted.
    bool do_omit_chip( ChipType ct ) const
        { return _chipIgnoreMask & ((uint8_t) ct); }

    /// Returns mask of ignored chips.
    uint8_t chip_omission_mask() const { return _chipIgnoreMask; }

    /// Sets chips to be ignored mask.
    void chip_omission_mask( uint8_t m ) { _chipIgnoreMask = m; }

    //
    // Statistics counters

    /// Return number of omitted non-physical events.
    size_t n_events_omitted_non_phys() const
        { return _eventsNonPhysicalOmitted; }

    /// Returns number of processed non-physical events.
    size_t n_events_transcoded_non_phys() const
        { return _eventsNonPhysicalProcessed; }
    
    /// Returns number of events omitted due to decoding failures.
    size_t n_events_decoding_failed() const
        { return _eventsDecodingFailureOmitted; }

    /// Returns number of processed digits.
    size_t n_digits_processed() const
        { return _digitsProcessed; }

    /// Returns number of digits omitted during transcoding.
    size_t n_digits_omitted() const
        { return _digitsIgnored; }

    /// Returns digits omitted due to unimplemented transcoding routine(s).
    size_t n_digits_omitted_transcoding_unimplemented() const
        { return _digitsDataTranslationUnimplemented; }
};  // class EventsTranscoder

}  // namespace aux


/**@class EvManagerHandle
 * @brief Wrapper for DaqDataDecoding's DaqEventsManager class providing access
 * interface to decoded events.
 *
 * DaqDataDecoding (DDD) library provides rudimentary representation of
 * forward-iterable abstraction incapsulating reading and decoding routines
 * for raw data stored in files called "chunks" or provided by RFIO socket.
 * 
 * This class represents a StromaV's wrapper around the DaqDataDecoding's
 * DaqEventsManager embedding its functions in StromaV's frame.
 *
 * Since interfacing with RFIO socket and decoding routines are already
 * implemented inside of DaqEventsManager such a wrapper can be used as a
 * reference point for further classes besides of direct usage.
 * */
class EvManagerHandle : public sV::aux::iEventSequence,
                        public aux::EventsTranscoder,
                        public sV::AbstractApplication::ASCII_Entry {
public:
    typedef sV::AnalysisPipeline::iEventSequence Parent;
    typedef aux::EventsTranscoder::Event Event;
private:
    bool _lastEvReadingWasGood;
    PBarParameters * _pbParameters;  ///< set to nullptr when unused
    std::vector<goo::filesystem::Path> _srcfilepaths;
    size_t _maxEventsNumber,
           _eventsRead;
protected:
    virtual bool _make_univ_event( Event *& );
    /*virtual bool _process_non_physical( Event *&);*/
    virtual bool _V_is_good() override;
    virtual void _V_next_event( Event *& ) override;
    virtual Event * _V_initialize_reading() override;
    virtual void _V_finalize_reading() override;
    virtual void _V_print_brief_summary( std::ostream & ) const override;
    virtual void _update_stat();
public:
    /// Generic ctr.
    EvManagerHandle( const std::list<goo::filesystem::Path> & filenames,
                     const goo::filesystem::Path & mapsDirName,
                     size_t maxEvents,
                     const std::vector<std::string> & omitChips,
                     bool keepRaw,
                     bool enableProgressbar=false,
                     bool nonPhys=false,
                     bool validateDetectorNames=true);

    /// Virtual ctr hook.
    EvManagerHandle( const goo::dict::Dictionary & );

    virtual ~EvManagerHandle();

    /// Returns persistent ref to last read event.
    CS::DaqEvent & current_event();

    /// Returns persistent ref to last read event (const).
    const CS::DaqEvent & current_event() const;
};

/**@class Chunk
 * @brief A single data chunk representation supporting metadata indexing and
 *        random access.
 *
 * This class may not represent an actual "chunk" but be rather a file of
 * significant size containing the subsets from multiple runs.
 * */
class Chunk : public md::ddd::MDTraits::iEventSource,
              public sV::AbstractApplication::ASCII_Entry {
public:
    sV_METADATA_IMPORT_SECT_TRAITS( ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID );
    typedef sV::iSectionalEventSource<::NA64_UEventID,
                                      na64ee::DDDIndex,
                                      na64ee::ChunkID>  Parent;
    typedef typename sV::AnalysisPipeline::Event        Event;
    typedef na64::events::ExperimentalEvent_Payload     ExpEventPayload;
    typedef na64ee::SpillDataLayout_t::ChunkNo_t        ChunkNo_t;
    typedef na64ee::DDDIndex::RunNo_t                   RunNo_t;
private:
    std::shared_ptr<std::istream> _streamSharedPtr;
    std::istream & _streamRef;   ///< File stream ptr.
protected:
    //
    // Implementation of sV::aux::iEventSequence
    virtual bool _V_is_good() override;
    virtual void _V_next_event( Event *& ) override;
    virtual Event * _V_initialize_reading() override;
    virtual void _V_finalize_reading() override;
    virtual void _V_print_brief_summary( std::ostream & ) const override;

    //
    // Implementation of sV::aux::iRandomAccessEventSource
    //virtual bool _V_event_read_single( const ::NA64_UEventID & ) override;
    //virtual bool _V_event_read_range( const ::NA64_UEventID & lower,
    //                                  const ::NA64_UEventID & upper ) override;

    virtual Event * _V_md_event_read_single( const Metadata & md,
                                             const EventID & eid ) override;
    virtual std::unique_ptr<sV::aux::iEventSequence> _V_md_event_read_range(
                                const Metadata & md,
                                const EventID & eidFrom,
                                const EventID & eidTo ) override;
    virtual std::unique_ptr<sV::aux::iEventSequence> _V_md_event_read_list(
                                const Metadata & md,
                                const std::list<EventID> & eidsList ) override;

    //
    // Implementation of sV::mixins::iIdentifiableEventSource
    virtual std::string _V_textual_id( const SourceID * ) const override;
protected:
    # ifndef SWIG
    /// Ctr for chunk representation with associated stream. External code has
    /// provide lifetime of the referred stream be not less then lifetime of an
    /// instance.
    Chunk( std::istream &,
           const SourceID &,
           TypesDictionary & );
    # endif
public:

    /// More robust ctr
    Chunk( std::shared_ptr<std::istream> & streamPtr,
           const SourceID &,
           TypesDictionary & );

    /// Dtr closes the fstream when instance owns it.
    virtual ~Chunk();

    /// Returns reference to associated stream.
    std::istream & istream_ref() { return _streamRef; }
};  // class Chunk 

}  // namespace ddd
}  // namespace dsources
}  // namespace afNA64

# endif // defined(StromaV_RPC_PROTOCOLS) && defined(na64ee_SUPPORT_DDD_FORMAT)

# endif  // H_AFNA64_DDD_BASIC_READER_H

