/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_MIDDLEWARE_SADC_WAVEFORM_RECONSTRUCTION_H
# define H_NA64_MIDDLEWARE_SADC_WAVEFORM_RECONSTRUCTION_H

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include "analysis/event_interfaces.hpp"
# include <StromaV/app/analysis.hpp>
# include <StromaV/app/analysis_cat_mx.tcc>
# include <StromaV/app/cvalidators.hpp>

// FWD {
class TH1F;
// FWD }

namespace na64 {
namespace dprocessors {
namespace aux {

class WFAligner : public analysis::iSADCProcessor,
                  public sV::mixins::DetectorCatalogue<TH1F**> {
public:
    typedef sV::AnalysisPipeline::iEventProcessor Parent;
    typedef Parent::Event Event;
    typedef sV::mixins::DetectorCatalogue<TH1F**> Catalogue;
    static const char _st_meanFileFmt[64];
    enum DynamicCorrectionMethod {
        disable = 0,
        byEvent = 1,
        byMean  = 2,
    };
protected:
    static const DynamicCorrectionMethod defaultAlignmentMethod;

    int (*_zeroes_finder)(uint16_t *, uint8_t, float*, float*, float);
    virtual ProcRes _V_process_SADC_profile_event( na64::events::SADC_profile & ) override;
    virtual void _V_finalize() const override;
    virtual TH1F** _V_new_entry( AFR_DetSignature, TDirectory * ) override;
    virtual void _V_free_entry( AFR_DetSignature, TH1F ** ) override;

    double _threshold;
    const sV::aux::HistogramParameters1D _hstPars;
    bool _doCollectStats;
    size_t _badZeroes;

    //TDirectory * _zeroesDir;
    //std::unordered_map<UByte, TDirectory *> _directories;
    //std::unordered_map<unsigned, TH1F**> _zeroesDistribution;
    std::unordered_map<AFR_DetSignature, std::pair<float, float> > _meanZeroes;
    const std::string _meansInFileName,
                      _meansOutFileName,
                      _dynamicAlignment;
    DynamicCorrectionMethod _alignmentMethod;
    virtual void _V_print_brief_summary( std::ostream & ) const override;
private:
    void _print_histogram_diagnosis(
            const std::pair<Catalogue::Parent::HashKey,
                            Catalogue::Parent::CategorizedEntry> & ) const;
public:
    WFAligner( const std::string & pn,
               const std::string & algo,
               const sV::aux::HistogramParameters1D &,
               const goo::filesystem::Path & meansInFileName,
               const goo::filesystem::Path & meansOutFileName,
               const std::string & dynamicCorrectionMethod,
               double threshold=.1 );
    WFAligner( const goo::dict::Dictionary & );
    ~WFAligner();
    virtual bool do_collect_stats() const override {
        return _doCollectStats; /*&& mixins::DetectorCatalogue<TH1F**>::do_collect_stats();*/ }
};  // class WFAligner

}  // namespace aux
}  // namespace dprocessors
}  // namespace na64

# endif  // defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# endif  // H_NA64_MIDDLEWARE_SADC_WAVEFORM_RECONSTRUCTION_H

