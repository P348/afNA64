/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_ANALYSIS_PROCESSORS_SUMMARIES_ECAL_H
# define H_ANALYSIS_PROCESSORS_SUMMARIES_ECAL_H

# include "afNA64_config.h"

# if defined(RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include <StromaV/app/analysis.hpp>

# include "na64_uevent.hpp"
# include "analysis/event_interfaces.hpp"

namespace sV {
namespace dprocessors {
namespace evd {

class ECALPreproc : public AnalysisApplication::iSADCProcessor {
public:
    typedef AnalysisApplication::iEventProcessor Parent;
    typedef AnalysisApplication::Event Event;
private:
    /// ECAL summary groups cache.
    std::unordered_map<AFR_DetMjNo, events::DetectorSummary *> _cache_evdMG;
protected:
    /// Besides of treatment of experimental events this routes call of modelled ones.
    virtual bool _V_process_event( Event * ) override;
    /// Sets max integral ampl. in ECAL group to provide.
    bool _V_process_experimental_event( ::sV::events::ExperimentalEvent * ) override;
    /// Calculates waveform integral in group.
    virtual bool _V_process_SADC_profile_event( ::na64::events::SADC_profile * ) override;
    /// Resets detector subgroup cache after event is done (and sent).
    virtual void _V_finalize_event_processing( Event * ) override;
    /// Prints summary of processor after everything done.
    virtual void _V_print_brief_summary( std::ostream & ) const override;
    /// Cleanup.
    virtual void _V_finalize() const override;
};  // class ECALPreproc

}  // namespace evd
}  // namespace dprocessors
}  // namespace sV

# endif  // defined(RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)
# endif  // H_ANALYSIS_PROCESSORS_SUMMARIES_ECAL_H

