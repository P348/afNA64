/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_ANALYSIS_PROCESSORS_SUMMARIES_MM_H
# define H_ANALYSIS_PROCESSORS_SUMMARIES_MM_H

# include "afNA64_config.h"

# if defined(RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include <StromaV/app/analysis.hpp>

# include "na64_uevent.hpp"
# include "analysis/event_interfaces.hpp"

namespace sV {
namespace dprocessors {
namespace evd {

class MicroMegaPreprocessor : public AnalysisApplication::iAPVProcessor {
public:
    typedef AnalysisApplication::iExperimantalEventProcessor Parent;
    typedef AnalysisApplication::Event Event;

    enum BestClusterSelection {
        disable = 0,
        maxAmplitude,
        maxReliability,
    };
private:
    size_t _nConsidered,
           _nSent,
           _emptySets,
           _consideredSets,
           _suspiciousClustersOmitted;
    BestClusterSelection _bestSelectionAlgorithm;
protected:
    /// Should return 'false' if processing in chain should be aborted.
    bool _V_process_APV_samples( ::na64::events::APV_sampleSet * ) override;
    /// Called after single event processed by all the processors.
    virtual void _V_finalize_event_processing( Event * ) override;
    /// Called after all events read and source closed to cleanup statistics.
    virtual void _V_finalize() const override;
    /// Called after all events read and all processors finalized.
    virtual void _V_print_brief_summary( std::ostream & ) const override;
public:
    MicroMegaPreprocessor( BestClusterSelection algo=disable ) :
            _nConsidered(0), _nSent(0), _emptySets(0), _consideredSets(0),
            _suspiciousClustersOmitted(0),
            _bestSelectionAlgorithm(algo) {}
};  // class MicroMegaPreprocessor

}  // namespace evd
}  // namespace dprocessors
}  // namespace sV

# endif  // defined(RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)
# endif  // H_ANALYSIS_PROCESSORS_SUMMARIES_MM_H


