# Directory `detectors/summaries`

This directory contains primitive processors that provides
fast reconstructions for different types of detectors used
in event display applications.

It operates with unified event format filling detector
summaries of each event. This stuff designed to be fast and
adaptive rather than precise or reliable.

Following the preprocessing pipeline approach each of this
processors does exactly one thing (serves only one detector
or detector family).

