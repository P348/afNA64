/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_AFNA64_SIMPLE_APV_CLUSTERING_PROCESSOR_H
# define H_AFNA64_SIMPLE_APV_CLUSTERING_PROCESSOR_H

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include "na64_uevent.hpp"
# include "analysis/apvConstraints.h"
# include "na64_detector_ids.hpp"
# include "analysis/event_interfaces.hpp"

# include <StromaV/app/analysis.hpp>
# include <StromaV/app/analysis_cat_mx.tcc>
# include <StromaV/app/cvalidators.hpp>
# include <TH2F.h>

class TH2F;

namespace na64 {
namespace analysis {
namespace dprocessors {

struct SimpleAPVStatistics {
    TH2I * ccandMapPtr,
         * bestClusterMapPtr;
    TH2F * amplitudeVsRatioDistributionPtr,
         * amplitudeVsRatioDistributionMaxPtr;

    TH2F * clusterAmplitudeVsWidth;
    TH2F * clusterRatios[2];
    TH2F * hitRatios[2];

    size_t stripsConsidered,
           clustersConsidered,
           clustersFound
           ;
};

/**@class SimpleAPVClustering
 * @brief Clustering processor for APV chip.
 *
 * Search clusters in APV data obtained within strip layout. Such readout usually
 * occurs in such detectors as triple GEMs, Micromegas, etc.
 *
 * SimpleAPVClustering class implements most basic cluster search algorithm, quick
 * and dirty.
 *
 * @ingroup analysis
 * */
class SimpleAPVClustering : public iAPVProcessor,
                            public ::sV::mixins::DetectorCatalogue<SimpleAPVStatistics *> {
public:
    typedef ::sV::mixins::DetectorCatalogue<SimpleAPVStatistics *> Catalogue;
    typedef iAPVProcessor::Event Event;
    typedef unsigned short StripNo;
    struct SignalOnStrip {
        float waveform[APV_NSamples];
    };
    /**@brief primitive cluster information holder.
     *
     * This struct designed only for most basic cluster information
     * storaging: x,y from [0:1] and «amplitude» values.
     */
    struct BasicClusterInfo {
        float x, y;
        float amplitude;
        float ratioAmpXY;
        float ratio02[2],
              ratio12[2];
        float widthX, widthY;
    };
    typedef std::map<StripNo, SignalOnStrip> StripsLayout;

    struct ProjectionHit {
        /// Mean weighted position in strips units
        float mwPosition;
        /// Sum of all the strip amplitudes affected by this hit.
        float amplitudeSum;
        /// Mean ratios: a0/a2, a1/a2.
        float ratios[2];
        /// Cluster width;
        StripNo width;
    };

    /// Represents particular APV detector read-out layout.
    /// Inside one event we can have a few such layouts: for
    /// GEM1, GEM2, for MuMega3, etc. This struct caches strips
    /// mapping, keeps reentrant storage of obtained clusters,
    /// maintain objects for statistical monitoring.
    struct DetectorLayout {
        /// Reentrant storage of clusters (freed by destructor).
        std::list<BasicClusterInfo> reentrantClusterStorage;
        /// Reentrant associative array depicting array (todo: may be use custom allocator here?).
        StripsLayout rLayoutX,
                     rLayoutY;
        /// Pointers on clusters only valid until _V_finalize_event_processing() call.
        std::list<BasicClusterInfo *> clusters;
        /// Ptr on per-detector statistics instance.
        SimpleAPVStatistics * statsPtr;
        /// Re-initialized every new event. Reentrant member that
        /// stores a pointer to an APV_sampleSet object inside of
        /// gprotobuf-event. Re-set at the beginning of treatment
        /// of each samples set.
        ::na64::events::APV_sampleSet * sampleSetPtr;

        DetectorLayout() : statsPtr(nullptr) {}
    };
private:
    const size_t _maxClusterWidth,
                 _minClusterWidth
                 ;
    const double _ratioTrust[2];
    bool _doCollectStats;
    const double _minimalAmplitudeOnStrip,
                 _minRatio02,
                 _minRatio12
                 ;
    
    std::unordered_map<AFR_DetSignature, DetectorLayout> _layouts;
    /// Returns reference to particular layout (mutable ref version).
    DetectorLayout & this_layout( AFR_DetSignature, StripsLayout ** = nullptr );
    /// Returns reference to particular layout (const ref version).
    const DetectorLayout & this_layout( AFR_DetSignature ) const;
protected:
    /// Should return 'false' if processing in chain should be aborted.
    virtual ProcRes _V_process_APV_samples( na64::events::APV_sampleSet & ) override;
    /// Called after single event processed by all the processors.
    virtual ProcRes _V_finalize_event_processing( Event & ) override;
    /// Called after all events read and source closed to cleanup statistics.
    virtual void _V_finalize() const override;
    /// Called after all events read and all processors finalized.
    virtual void _V_print_brief_summary( std::ostream & ) const override;
    /// Called after all APV raw data is processed.
    virtual ProcRes _V_finalize_processing_apv_event(
                        na64::events::ExperimentalEvent_Payload & ) override;

    SimpleAPVStatistics * _V_new_entry( AFR_DetSignature id, TDirectory * ) override;
    void _V_free_entry( AFR_DetSignature, SimpleAPVStatistics * ) override;

    /// Performs clusterization algorithm. List will be appended without
    /// clearing.
    virtual void _clusterize( const StripsLayout & layoutX,
                              const StripsLayout & layoutY,
                              std::list<BasicClusterInfo> & reentrantClusterStorage,
                              std::list<BasicClusterInfo *> & clusters,
                              SimpleAPVStatistics * statsPtr=nullptr );
    /// Fills uevent's APV_Cluster structure into GPB u-event.
    virtual void _fill_cluster( BasicClusterInfo &,
                               events::APV_Cluster &,
                               const na64::aux::DetectorMapping::APVBoundaries & xf ) const;
public:
    SimpleAPVClustering( const std::string & pn,
                         bool doAccumulateStatistics,
                         size_t maxClusterWidth,
                         size_t minClusterWidth,
                         double maxRatioTrust,
                         double minRatioTrust,
                         double minimalAmplitudeOnStrip=0.,
                         double minRatio02=1e-6,
                         double minRatio12=1e-6 );
    /// Virtual ctr
    SimpleAPVClustering( const goo::dict::Dictionary & );

    ~SimpleAPVClustering();
};  // class APVClustering

}  // namespace dprocessors
}  // namespace analysis
}  // namespace na64

# endif  // defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# endif  // H_AFNA64_SIMPLE_APV_CLUSTERING_PROCESSOR_H

