/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_BIPLOT_PROCESSOR_H
# define H_NA64_BIPLOT_PROCESSOR_H

# include "afNA64_config.h"

# if defined(StromaV_ANALYSIS_ROUTINES) && defined (StromaV_RPC_PROTOCOLS)

# include "na64ee_readout.hpp"
# include <StromaV/app/analysis.hpp>
# include "na64_uevent.hpp"
# include <TH2F.h>
# include <StromaV/app/cvalidators.hpp>

namespace na64 {
namespace dprocessors {

typedef ::na64::events::ExperimentalEvent_Payload NA64ExperimentalEvent;
typedef ::sV::aux::iTExperimentalEventPayloadProcessor<NA64ExperimentalEvent>
                                                            ExpEventProcessor;
/**@class Biplotter
 * @brief This processor makes a standard TH2F histogram (ECAL vs HCAL energy)
 *
 * Should follow after any reconstruction processor (e.g. na64officialReco)
 * and the all processors which are wanted to be implemented to data
 * (e.g. cuts, apv/adc routines)
 *
 * @ingroup analysis
 * */

class Biplotter : public ExpEventProcessor {

public :
    Biplotter(const std::string &,
              const goo::dict::Dictionary & dct);
    virtual ~Biplotter();
    Biplotter( const goo::dict::Dictionary & dct) :
        Biplotter("biplotter",
                  dct ) {};
protected:
    virtual ProcRes _V_process_event_payload( NA64ExperimentalEvent & ) override;
private:
    TH2F * _biplot;
};  // class Biplotter

}  // namespace dprocessors
}  // namespace na64

# endif  // defined(StromaV_ANALYSIS_ROUTINES) && defined (StromaV_RPC_PROTOCOLS)
# endif  // H_NA64_BIPLOT_PROCESSOR_H
