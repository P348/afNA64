/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_HCALDRIFT_ANALYZER_PROCESSOR_H
# define H_NA64_HCALDRIFT_ANALYZER_PROCESSOR_H

# include "config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include "na64ee_readout.hpp"

# include <StromaV/app/analysis.hpp>
# include "analysis/event_interfaces.hpp"
# include "na64_uevent.hpp"

# include <TTree.h>
# include <list>

namespace na64 {
namespace dprocessors {
namespace HCALdriftAnalyzer {

typedef ::na64::events::ExperimentalEvent_Payload NA64ExperimentalEvent;
typedef ::na64::analysis::iSADCProcessor SADCProcessor;

struct SpillStruct {
    double sumSpill,
           rms,
           sumEvent;
    int entries,
        spillNo;
    std::list<double> edepsList;
    TTree * tree;
    SpillStruct( AFR_DetMjNo );
};

class HCALdriftAnalyzer : public SADCProcessor {
                      // TODO public sV::mixins::DetectorCatalogue<DriftStatistics *> {
                      // TODO public DDDIndex {
public :
    HCALdriftAnalyzer( const std::string & );
    virtual ~HCALdriftAnalyzer();
protected :
    // TODO storage for histos (spill, detector, type, etc.)
    virtual ProcRes _V_process_event_payload( NA64ExperimentalEvent & ) override;
    virtual ProcRes _V_process_SADC_profile_event(
                    ::na64::events::SADC_profile & ) override;
    virtual ProcRes _V_finalize_event_processing( Event & ) override;
    virtual void _V_finalize() const override;
    TTree * _hcalTree;
    std::unordered_map < AFR_DetMjNo, SpillStruct * > _detMap;
    std::unordered_map < AFR_DetMjNo, SpillStruct * >::iterator
                                        _add_new_detector_entry( AFR_DetMjNo );
    void _calculate_rms();
    void _clear_map_after_spill();
    void _add_event_entries();
    void _fill_tree_after_spill();
    // void _create_histos_after_processing () const;

    std::set < AFR_DetFamID > * _detSet;  // TODO selecter
    bool _drop_spill();
    uint32_t _currentSpill;
    bool _spillNumberSetted;
    bool _eventProcessed;
};  // class HCALdriftAnalyzer

}  // HCALdriftAnalyzer
}  // namespace na64
}  // namespace dprocessors

# endif  // StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES
# endif  // H_NA64_HCALDRIFT_ANALYZER_PROCESSOR_H

