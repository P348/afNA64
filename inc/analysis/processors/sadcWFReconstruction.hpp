/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_MIDDLEWARE_SADC_WAVEFORM_RECONSTRUCTION_H
# define H_NA64_MIDDLEWARE_SADC_WAVEFORM_RECONSTRUCTION_H

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include "analysis/event_interfaces.hpp"
# include "analysis/sadcWF_fit.h"

# include <StromaV/app/analysis.hpp>
# include <StromaV/app/analysis_cat_mx.tcc>

// FWD {
class TProfile;
class TH2F;
class TH1F;
namespace sV {
namespace aux {
class HistogramParameters1D;
class HistogramParameters2D;
}  // namespace aux
}  // namespace sV
// FWD }

namespace na64 {
namespace dprocessors {
namespace aux {

struct DistributionProfile {
    TProfile * _profilePtr;
        TH2F * _distributionPtr;
    DistributionProfile(
                const std::string & nameSuffix,
                const std::string & labelSuffix,
                sV::aux::HistogramParameters2D pars );
    void fill( float x, float y );
};  // struct DistributionProfile

struct ReconstructionStatistics {
    DistributionProfile absMax,
                        mean
                        ;
    TH1F * _reliabilityDistribution,
         * _stdDeviation,
         * _sumDistribution,
         * _timeDispersion
         ;
    ReconstructionStatistics(
            const std::string & detectorName,
            const sV::aux::HistogramParameters2D & maxHstmsPars,
            const sV::aux::HistogramParameters2D & meanHstmsPars,
            const sV::aux::HistogramParameters1D & relDstHstmPars,
            const sV::aux::HistogramParameters1D & stdDeviationHstPars,
            const sV::aux::HistogramParameters1D & sumDstHstPars,
            const sV::aux::HistogramParameters1D & timeDispersionPars
            );
    void consider_exp_sadc_stats( AFR_UniqueDetectorID /*uid*/, const na64::events::SADC_suppInfo & sInfo );
    static float apply_calibration_by_max( na64::events::SADC_profile & );
};  // ReconstructionStatistics

class SADC_WF_Reconstruction : public na64::analysis::iSADCProcessor,
                               public sV::mixins::DetectorCatalogue<ReconstructionStatistics *> {
public:
    typedef na64::analysis::iSADCProcessor Parent;
    typedef Parent::Event Event;
protected:
    struct SADCWF_FittingInput samples;
    size_t _nConsidered,
           _nRefused_badAlgo,
           _nRefused_negativeSum,
           _nRefused_nonNumCh
           ;

    SADCWF_FittingFunction _fitting_function;
    SADCWF_FittingFunctionParameters fittingParameters;

    struct SADCWFCharacteristicParameters _chParsReentrant;

    virtual ProcRes _V_process_SADC_profile_event( na64::events::SADC_profile & ) override;
    virtual void _V_print_brief_summary( std::ostream & ) const override;
    ReconstructionStatistics * _V_new_entry( AFR_DetSignature id, TDirectory * ) override;
    void _V_free_entry( AFR_DetSignature, ReconstructionStatistics * ) override;
public:
    SADC_WF_Reconstruction( const std::string & pn );
    SADC_WF_Reconstruction( const goo::dict::Dictionary & dct );  // TODO
    ~SADC_WF_Reconstruction();
};  // class SADC_WF_Reconstruction

}  // namespace aux
}  // namespace dprocessors
}  // namespace na64

# endif  // defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# endif  // H_NA64_MIDDLEWARE_SADC_WAVEFORM_RECONSTRUCTION_H


