/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_NA64_OFFICIAL_RECO_PROCESSOR_H
# define H_NA64_OFFICIAL_RECO_PROCESSOR_H

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES) && \
                               defined(AFNA64_RECO_FOUND)

# include <StromaV/app/analysis.hpp>
//# include "StromaV/analysis/pipeline.hpp"
# include "na64_uevent.hpp"
# include "p348reco.h"
# include "na64_detector_ids.h"
//# include <StromaV/app/analysis.hpp>
# include <fstream>  // XXX

namespace na64 {
namespace dprocessors {

typedef ::na64::events::ExperimentalEvent_Payload NA64ExperimentalEvent;
typedef ::sV::aux::iTExperimentalEventPayloadProcessor<NA64ExperimentalEvent>
                                                            ExpEventProcessor;

class OfficialReco : public ExpEventProcessor {
public:
    typedef sV::AnalysisPipeline::iEventProcessor Parent;
    typedef ::na64::events::ExperimentalEvent_Payload NA64ExperimentalEvent;
protected:
    virtual ProcRes _V_process_event_payload( NA64ExperimentalEvent & ) override;
    void _process_reco_event( const RecoEvent&, NA64ExperimentalEvent & );
    CS::DaqEventsManager * _daqManager;
    uint32_t _lastEventRunNumber;

    // XXX
    std::ofstream * _outfile;
public:
    OfficialReco( const std::string & pn, const std::string& mapDir);
    OfficialReco( const goo::dict::Dictionary & );
    virtual ~OfficialReco();

};  // class OfficialReco

}  // namespace dprocessors
}  // namespace sV

# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES) ...
# endif  // H_NA64_OFFICIAL_RECO_PROCESSOR_H

