# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# Author: Bogdan Vasilishin <togetherwithra@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


# The StromaV library implements a frame in which this middleware embeds itself.
# Depending on options enabled in StromaV this middleware must be configured.
find_package( StromaV REQUIRED )

#
# THIRD-PARTY LIBRARIES AND PACKAGES
###################################
# Boost library is common for most modern Linux distros. Implements a lot
# of extended functionality: containers, threading, I/O, etc.
find_package( Boost ${Boost_FORCE_VERSION}
              COMPONENTS system
                         thread
                         unit_test_framework
                         iostreams
              REQUIRED )

find_package( ROOT
              COMPONENTS Eve Gui Ged Geom RGL EG REQUIRED )
# Configure build accordingly to features enabled in StromaV:
# - RPC protocols
if( StromaV_RPC_PROTOCOLS )
    message( STATUS "RPC protocols enabled." )
    set( PROTOBUF_IMPORT_DIRS "${StromaV_INSTALL_PREFIX}/share/StromaV" )
    find_package( Protobuf REQUIRED )
    #set(PROTOBUF_GENERATE_CPP_APPEND_PATH FALSE)
    PROTOBUF_GENERATE_CPP(GPROTO_MSGS_SRCS GPROTO_MSGS_HDRS
        na64_event.proto )
    set_source_files_properties(${GPROTO_MSGS_SRCS} PROPERTIES COMPILE_FLAGS -w )
    set_source_files_properties(${GPROTO_MSGS_SRCS} PROPERTIES GENERATED TRUE )
endif( StromaV_RPC_PROTOCOLS )

if( StromaV_ANALYSIS_ROUTINES )
    find_package( na64ee )
endif( StromaV_ANALYSIS_ROUTINES )

# Note: this option enables look up for DATE, RFIO (aka shift, developed by
# CERN CASTOR team at 2000-20005) libraries. RFIO library is now completly
# changed and included into major CASTOR software facility while DATE is
# known to be darmant.
# As these libs are included in CERN's Scientific Linux and probably can
# not be redistributed, this option is dedicated out of walk-through build.
# One can only need them when performing build on actual CERN experiments.
find_library( RFIO_AKA_SHIFT_LIB NAMES libshift.so )
find_library( DATE_LIBMONITOR NAMES libmonitor.so
              HINTS /afs/cern.ch/compass/online/daq/dateV37/monitoring/Linux/ )

if( StromaV_GEANT4_MC_MODEL )
    find_package(Geant4)
    if( StromaV_G4_MDL_GUI )
        find_package(Geant4 REQUIRED ui_all vis_all)
        # Try to find Qt if G4 GUI was enabled
        set( DESIRED_QT_VERSION "4" )
        find_package(Qt)
    else( StromaV_G4_MDL_GUI )
        find_package(Geant4 REQUIRED)
    endif( StromaV_G4_MDL_GUI )
    include(${Geant4_USE_FILE})
    # See explaination for this snippet at the StromaV's CMakeLists.txt:
    get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
    set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES "")
    #include_directories( SYSTEM ${dirs} )
endif( StromaV_GEANT4_MC_MODEL )

# look for A' physics module
find_package( DPhMC )
if( DPhMC_FOUND )
    set( AFNA64_DPhMC_FOUND ON )
    message( STATUS "DPhMC package found." )
else( DPhMC_FOUND )
    message( STATUS "DPhMC package wasn't found." )
endif( DPhMC_FOUND )

find_package( na64-tools-simulation )
if( na64-tools-simulation_FOUND )
    set( AFNA64_NA64_TOOLS_SIMULATION_FOUND_ON )
endif( na64-tools-simulation_FOUND )


# Try to find and link p348Reco lib
find_library( P348RECO_LIB
              NAMES libP348RECO.so
              HINTS ${RECO_LIB_PATH} )
if ( P348RECO_LIB )
    message( STATUS "Found P348Reco library at ${P348RECO_LIB}" )
    get_filename_component( P348RECO_INCLUDE_DIR_HINT ${P348RECO_LIB} DIRECTORY )
    find_path( P348RECO_INCLUDE_DIR p348reco.h
        HINTS
            ${P348RECO_INCLUDE_DIR_HINT}/../../include/p348reco
            ${P348RECO_INCLUDE_DIR_HINT}/../../include/na64
            ${P348RECO_INCLUDE_DIR_HINT}/../../include/na64/p348reco )
    message( STATUS "Found P348Reco header at ${P348RECO_INCLUDE_DIR}.")
    set( AFNA64_RECO_FOUND ON )
else(P348RECO_LIB)
    message(WARNING "P348Reco library not found. You may specify path hint "
    "with -DRECO_LIB_PATH=... variable")
    set( AFNA64_RECO_FOUND OFF )
endif( P348RECO_LIB )

