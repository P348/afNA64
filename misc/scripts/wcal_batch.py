# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

# Script for batch run of svmc performing calculation of WCAL sensitivity. See
# EPL-TPU task #132.

import os, subprocess
from time import sleep
from random import randint

nWorkers = 4

# interesting mixing parameter range for WCAL of 35 Wf/Sc layers, according to
# MK initial calculations:
epsilonRange = [
    # for i in range(0, 12): print '%3.3e'%(((1.75**i)*1e-5))
    #1.000e-05,      1.750e-05,      3.063e-05,
    #5.359e-05,      9.379e-05,      1.641e-04,
    #2.872e-04,      5.027e-04,      8.796e-04,
    #1.539e-03,      2.694e-03,      4.714e-03,
    # for i in range(0, 12): print '%3.3e'%(((1.75**i)*1e-6)):
    #1.000e-06,      1.750e-06,      3.062e-06,
    #5.359e-06,      9.379e-06,      1.641e-05,
    #2.872e-05,      5.027e-05,      8.796e-05,
    #1.539e-04,      2.694e-04,      4.714e-04,
    # for i in range(0, 12): print( '%3.3e'%(((1 + 1.9**i)*1e-6)) )
    #2.000e-06,      2.900e-06,      4.610e-06,
    #7.859e-06,      1.403e-05,      2.576e-05,
    #4.805e-05,      9.039e-05,      1.708e-04,
    #3.237e-04,      6.141e-04,      1.166e-03,
    # From Dm.K (letter "Ns_vs_signal", 17/03/017):
    0.00005,    # 0.805
    #0.00005,    # 1.285
    0.00006,    # 2.22
    0.000065,   # 3.23
    0.00007,    # 2.294
    #0.00007,    # 4.12
    0.00009,    # 5.523
    0.00012,    # 13.06
    0.00021,    # 34.01
    0.00042,    # 38.46
    0.00070,    # 7.444
    #0.0007,     # 35.79
    0.00080,    # 3.927
    #0.0008,    23.14
    0.00090,    # 1.190
    #0.0009,     # 8.91
    0.00095,    # 4.55
    0.00100,    # 0.477
    0.00105,    # 3.153
    0.00115,    # 1.66
    0.00120,    # 0.769
]

running = []

def run_svmc( *args, **kwargs ):
    stdoutLog = open(kwargs.pop( 'stdoutLog' ), 'w')
    stderrLog = open(kwargs.pop( 'stderrLog' ), 'w')
    env = dict(os.environ)
    env["LD_PRELOAD"] = str(os.path.abspath("../install/lib/libDPhMC.so.0.1.dev"))
    cmd = ' '.join(args)
    print( '  $', cmd )
    p = subprocess.Popen( cmd,   stdout=stdoutLog,
                                            stderr=stderrLog,
                                            shell=True,
                                            env=env )
    return p

def main():
    commonArguments=[
            '../install/bin/svmc', '-v3',
            '-c', './wcal-X-boson.cfg',
            '--g4.batch'
        ]
    ratio = 2e+8*((2.5e-4)**2)
    # LD_PRELOAD=$(realpath ./install/lib/libDPhMC.so) ./install/bin/svmc -v3 --geometry=/tmp/gdml/00_root.gdml -c /tmp/gdml/afNA64.cfg --g4.batch
    for eps in epsilonRange:
        factor=ratio/(eps*eps)
        filePrefix='%.3e-%.3e'%(eps, factor)
        instArgs = commonArguments \
                 + [ '--aprimeEvGen.factor=%e'%factor,
                     '--aprimeEvGen.mixingFactor=%e'%eps,
                     '--root.output-file=out/wcal-%s.root'%filePrefix,
                     '--g4.randomSeed=%d'%randint(0, 5000),
                     '--extraPhysics.physicsAe.TFoam.trand3Seed=%d'%randint(0, 5000) ]
        p = run_svmc( *instArgs,
                  stdoutLog='logs/%s-stdout.log'%filePrefix,
                  stderrLog='logs/%s-stderr.log'%filePrefix )
        running.append(p)
        print( "    ...Running eps=%.3e, factor=%.3e, PID=%d"%(eps, factor, p.pid) )
        while len(running) == nWorkers:
            sleep(1)
            for cp in running:
                if cp.poll() is not None:
                    running.remove(cp)
                    break


if "__main__" == __name__:
    main()

