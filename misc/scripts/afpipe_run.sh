# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# This script launches an analysis application for event multicasting event (to be
# received then by event display GUI application). All the options provided to the
# script will be appended to application call.

DATA_DIR=/home/crank/Statistics/CERN/na64

#CWD=${CWD:-.}
CWD="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ANALYSIS_UTIL_EXEC=$CWD/afpipe-rdbg

#
# The processors on pipeline will be applied to dataflow sequentially:
PIPELINE_HANDLERS=( sadcProfiles        \
                    SADCWFAligner       \
                    sadcProfiles
                    )

#
# Data source
DATA_SOURCE=$DATA_DIR/2016/cdr01001-002551.dat
DATA_FORMAT=ddd

# For DDD format the mapping XML
DDD_MAP_DIR=$DATA_DIR/maps/2016.xml/

#
# Prepend PIPELINE_HANDLERS entries with -p option
PIPELINE_HANDLERS=( "${PIPELINE_HANDLERS[@]/#/ -p}" )

export COREDUMP_ON_SEGFAULT=enable
export ATTACH_GDB_ON_SEGFAULT=enable
#export PRESERVE_ROOT_SIGNAL_HANDLERS=enable

#
# Go!
go() {
    #export LD_PRELOAD=$(realpath ../lib/na64/libafNA64-rdbg.so)
    set -x
    $ANALYSIS_UTIL_EXEC --config=$(realpath $CWD/../etc/StromaV/) -A \
                     -i $DATA_SOURCE -F $DATA_FORMAT \
                     -O analysis.data-sources.na64.ddd.map-dir=$DDD_MAP_DIR \
                     -O analysis.processors.na64.SADC.WaveformAlignment.enable-dynamic-correction=mean \
                     -O analysis.processors.na64.SADC.WaveformAlignment.means-file-in=/tmp/zeroes.txt \
                     ${PIPELINE_HANDLERS[@]} \
                     -n 1000 \
                     -l $CWD/../lib/na64/libafNA64-rdbg.so
                     #--sadc-wf-alignment.enable-dynamic-correction=mean \
                     #--sadc-wf-alignment.means-file-in=SADC-pedestals-01151-001509.txt \
                     #$@
}

rm -f /tmp/sV_latest.root
rm -f /tmp/sV_diff.root

go

# This will output differences histo into the /tmp/sV_diff.root (XXX, iss 169)
root -q -b $(realpath $CWD/../../sources/afNA64/misc/scripts/iss_192.C)


