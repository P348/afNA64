#!/bin/bash

# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Bogdan Vasilishin <togetherwith@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# This script provides possibility to read multiple data source. For each data
# chunk separate root file will be produced.

#
## path to directory contains data
DATA_PATH=/home/smfx/projects/na64data/2016/run2/
#
DATA_FORMAT=ddd
#
## path to dir contains all detectors maps
DDD_MAP_DIR=/home/smfx/projects/p348-daq/maps/
#
## whether non-phys (e.g. CALIBRATION) events will be proceeding
ENABLE_NON_PHYS=yes
#
CFG_FILE=../etc/StromaV/afNA64.cfg
#
ANALYSIS_UTIL_EXEC=./afpipe-rdbg
#
## where output root files will be stored
ROOT_DIR_OUT=/tmp/
#
## short postfix description of the output root files
ROOT_POSTFIX=-LEDdriftAnalysis.root

#
# The processors on pipeline will be applied to dataflow sequentially:
PIPELINE_HANDLERS=( onlyLED \
                    na64reco  \
                    HCALdriftAnalyzer \
                    HCALsum \
                    )

#
# Prepend PIPELINE_HANDLERS entries with -p option
PIPELINE_HANDLERS=( "${PIPELINE_HANDLERS[@]/#/ -p }" )

#
## list of data files to be processed
data_files=( "cdr01001-002470.dat" \
             "cdr01002-002470.dat" \
             "cdr01003-002470.dat" \
             "cdr01004-002470.dat" \
           )

process_one_chunk() {
    export LD_PRELOAD=$(realpath ../lib/na64/libafNA64-rdbg.so)
    set -x
    $ANALYSIS_UTIL_EXEC --config=$CFG_FILE \
                        -i $DATA_PATH$1 \
                        -F $DATA_FORMAT \
                        --ddd.map-dir=$DDD_MAP_DIR \
                        --root.output-file $ROOT_DIR_OUT${1%.*}$ROOT_POSTFIX \
                        ${PIPELINE_HANDLERS[@]}
}

process_data() {
    for i in "${data_files[@]}";
    do
        process_one_chunk $i
    done
}

process_data

