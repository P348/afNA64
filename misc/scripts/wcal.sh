# A shell script running the WCAL in batch mode. To be run from NA64-meta/
# metaproject dir, after deploying with --profile=mc:

workdir=wcalTests.2rem
sourcesdir=../sources  # relatively to $workdir
directoies=(
    "$workdir/figs"
    "$workdir/gdml"
    "$workdir/logs"
    "$workdir/out"
    "$workdir/scripts"
)

cd $workdir

for dirname in ${directoies[@]} ; do
    mkdir -p $dirname
done

ln -s $sourcesdir/afNA64/misc/configs/wcal-X-boson.cfg $workdir
ln -s $sourcesdir/afNA64/misc/configs/wcal_batch_plot.gpi $workdir
ln -s $sourcesdir/afNA64/misc/configs/wcal_postprocess.C $workdir

source ./install/bin/activate
render_gdml.py --templates-dir="$sourcesdir/na64-geomlib/tgdml" \
               --output-dir="$workdir/gdml" \
               "$sourcesdir/na64-geomlib/tgdml/00_root.tgdml"
# FIXME ^^^: render_gdml.py relies on internal placement dictionary, so WCAL
# may be placed at wrong position or may not be placed in scene at all.

#
# GO!
python "$sourcesdir/afNA64/misc/scripts/wcal_batch.py"
# ^^^ After this command is done, one may use postprocessing CINT script
# (referenced by symlink wcal_postprocess.C) to extract W-boson born/kill
# stats that further may be plotted with gnuplot-script (referenced by
# wcal_batch_plot.gpi symlink).
# Please, take care of appropriate filenames in wcal_postprocess.C script ---
# they need to be set manually with the generated content of out/ dir as well
# as N_EOT[...] parameter[s] (their meaning commented there).

