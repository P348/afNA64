# -*- coding: utf-8 -*-
# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# Author: Bogdan Vasilishin <togetherwithra@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
"""

from ___future___ import print_function
import afNA64.na64DetectorsIds as dids
import StromaV.sVEvents as sVEve
import afNA64.na64Events as na64Eve

# TODO import ExpEventProcessor somehow
class Biplotter( ExpEventProcessor ):
    ECAL = [ dids.EnumScope.d_ECAL0, dids.EnumScope.d_ECAL1 ]
    HCAL = [ dids.EnumScope.d_HCAL0, dids.EnumScope.d_HCAL1,
             dids.EnumScope.d_HCAL2, dids.EnumScope.d_HCAL3 ]
    def _V_process_event_payload( expEvent ):
        ecalEnergy = 0
        hcalEnergy = 0
        for sadc in range(0, event.sadc_data_size()):
            sadcProfile = event.sadc_data(sadc)
            detID = sadcProfile.detectorid()
            if detID in ECAL:
                ecalEnergy += sadcProfile.edep()
            else if detID in HCAL:
                hcalEnergy += sadcProfile.edep()
# TODO crate and fill histo


