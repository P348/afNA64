__all__ = [ 'test_detectorsIds'
          , 'test_pipe_payload'
          , 'test_rawstat_models'
    ]

from StromaV.appUtils import PythonSession

na64PyApp = PythonSession.init_from_string( 'na64App', 
    'Python testing app for NA64 UTs.', '' )

