# -*- coding: utf-8 -*-
# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
This testing unit checks the pipeline classes performing experimental/simulated
payload(s) (un)packing.

The data sources simulates reading of SADC (sampling ADC chip digits) data from
the imaginary source generating floating point values pairwise shifted on
arbitrary value. The structure of event may be expressed as follows:
    Event <>-1- (Any) Experimental payload <>-gNSADCDets- SADC data
The particular structure of SADC doesn't affect the event passing and have been
introduced just to check basic protobuf gets/set methods wrapped by SWIG.

The produced event then will be passed through the mock processor handler
(testing handler) implemented here to perform discrimination selectively.

The second processor will recieve undiscriminated events and check the
discrimination logic validity.
"""

from __future__ import print_function
import unittest, pickle
from StromaV.pipeline import AnalysisPipeline, iEventSequence, \
                        iEventProcessor, dereference_event_ptr_ref
import StromaV.sVEvents as sVEve

#from StromaV.appUtils import PythonSession              # XXX
#na64PyApp = PythonSession.init_from_string( 'na64App',  # XXX
#        'Python testing app for NA64 UTs.', '' )        # XXX

import afNA64.na64Events as na64Eve
from afNA64.processors import iSADCProcessor

from math import pi, sqrt, exp
from random import uniform, triangular

def MoyalF(x, mu, sigma):
    """
    Moyal PDF approximation function. Values for SADC typical form (very
    approximate) is: mu=8, sigma=1.8. factor=1e4.
    """
    return exp(-exp( -(x-mu)/sigma )/2 - (x-mu)/(2*sigma))/(sqrt(2*pi)*sigma)

# Number of SADC detectors
gNSADCDets = 24
gNEventsMax = 100

discriminatedI_ids = []
abortedI_ids = []
modifiedI_ids = []
# will contain pairs of event number and samples number that must reach second
# processor:
samplesToReach = {}
samplesReached = {}

#
# Mock event source
class MockExperimentalEventSequence(iEventSequence):
    """
    This class implements an event source class stub that generates testing
    NA64 experimental event instances with mock SADC data. Will use simple
    Moyal function to mimic the real waveform profile, shifted by random
    values.
    """
    def generate_mock_SADC(self):
        self.ut.assertFalse( self.reentrantExpEvent.sadc_data_size() )
        discriminateIds = []
        omittedIDs = []
        samplesToReach[self.idx] = []
        for i in range(0, gNSADCDets):
            omitMe = not (int(uniform(0, 6))%3)
            if omitMe:
                omittedIDs.append(i)
                continue
            discriminateMe = not (int(uniform(0, 6))%3)
            if discriminateMe:
                discriminateIds.append(i)
            sadcData = self.reentrantExpEvent.add_sadc_data()
            for j in range(0, 32):
                value = self.sadcPedestals[i][j%2] + 1e4*MoyalF(j, uniform(7., 9.), uniform(1.7, 1.9))
                sadcData.add_samples( value )
            sadcData.set_detectorid( i )
            self.ut.assertEqual( 32, sadcData.samples_size() )
            if not discriminateMe:
                samplesToReach[self.idx].append( i )
        self.ut.assertEqual( self.reentrantExpEvent.sadc_data_size(), gNSADCDets - len(omittedIDs) )
        # We use raw event data here to pack discrimination instructions for
        # checking processors.
        eventInfo = {
                'eventNo' : self.idx,
                'abort' : not (self.idx%21),
                'discriminateEvent' : not (self.idx%9),
                'modifyEvent' : not (self.idx%3),
                'samples2discriminate' : discriminateIds,
                'omittedIDs' : omittedIDs
            }
        if eventInfo['abort']:
            del samplesToReach[self.idx]
        #print( 'source: packed %d samples in event #%d.'%(
        #                self.reentrantExpEvent.sadc_data_size(), self.idx) )
        self.reentrantExpEvent.set_rawevent( pickle.dumps(eventInfo) )

    def pack_data(self, event):
        self.reentrantExpEvent.set_runno( 123 )
        self.reentrantExpEvent.set_spillno( 321 )
        self.reentrantExpEvent.set_evnoinspill( self.idx )
        if self.idx == 0 or self.idx >= gNEventsMax:
            # mock non-physical events:
            self.reentrantExpEvent.set_is_physical(False)
            if not self.idx:
                self.reentrantExpEvent.set_nonphyseventtype( na64Eve.ExperimentalEvent.RunStart )
            elif self.idx > gNEventsMax:
                self.reentrantExpEvent.set_nonphyseventtype( na64Eve.ExperimentalEvent.RunEnd )
        else:
            # mock `physical' events
            self.reentrantExpEvent.set_is_physical(True)
            self.generate_mock_SADC()
            #self.reentrantExpEvent  ...
            self.ut.assertTrue( self.reentrantExpEvent.sadc_data_size() > 0 )
        event.mutable_experimental() \
                    .mutable_payload() \
                    .PackFrom( self.reentrantExpEvent )

    def _V_initialize_reading(self):
        """
        Overloaded function called once the reading process begins.
        """
        # Reentrant event substrate instance to be transmitted via the pipeline:
        self.reentrantEvent = sVEve.Event()
        # The concrete experimental event to be packed inside the substrate:
        self.reentrantExpEvent = na64Eve.ExperimentalEvent()
        self.idx = 0
        # This usually performed by _V_next_event() call:
        self.pack_data( self.reentrantEvent )
        self.idx = 1
        return self.reentrantEvent

    def _V_is_good(self):
        return self.idx <= gNEventsMax

    def _V_next_event(self, eventRef):
        evPtr = dereference_event_ptr_ref(eventRef)
        # ensure that event pointer had not been changed:
        self.ut.assertEqual( int(evPtr.this), int(self.reentrantEvent.this) )
        # Clear and pack new SADC samples:
        self.reentrantEvent.Clear()
        self.reentrantExpEvent.Clear()
        self.pack_data( evPtr )
        if self.idx > 0 and self.idx < gNEventsMax:
            self.ut.assertTrue( self.reentrantEvent.has_experimental() )
            self.ut.assertTrue( self.reentrantExpEvent.sadc_data_size() > 0 )
        self.idx += 1

    def _V_finalize_reading(self):
        pass

    def __init__(self, ut):
        self.ut = ut
        self.reentrantEvent = None
        self.idx = 0
        # The SADC waveform usually shifted by random pedestel values:
        self.sadcPedestals = []
        for i in range(0, gNSADCDets):
            self.sadcPedestals.append( (uniform(-10, 100), uniform(-10, 100)) )
        super(MockExperimentalEventSequence, self).__init__( 0 )

#
# Mock event processor #1
class MockSADCProcessor1(iSADCProcessor):
    """
    This mock processor performs basic operations that ordinary SADC processor
    has to do: modifies data and selectively discriminates events or data
    samples within a particular event.
    This implementation actually does not make decisions. It uses the
    supplementary aux data dictionary accompanying the particular event to do
    something.
    """
    def __init__(self, ut):
        self.ut = ut
        self.nEventsProcessed = 0
        # Will keep special data accompanying mock event:
        self.mockData = None
        # Will be used to control wether the event was aborted and finalizing
        # routine was invoked:
        self.finalizeAuxDat = None
        super(MockSADCProcessor1, self).__init__('mock-SADC-1')

    def _V_process_event(self, event):
        # Only experimental payloads have to pass here.
        self.ut.assertTrue( event.has_experimental() )
        # For non-experimental event (0 == idx || idx >= gNEventsMax),
        # invokation below won't propagate to
        # _V_process_event.../_V_process_SADC_... methods.
        res = super(MockSADCProcessor1, self)._V_process_event(event)
        self.nEventsProcessed += 1
        return res

    def _V_process_event_payload( self, expEvent ):
        if self.nEventsProcessed  == 0 or self.nEventsProcessed  >= gNEventsMax:
            self.ut.assertFalse( expEvent.is_physical() )
        else:
            # Check that event SADC payload actually contains something.
            self.ut.assertTrue( expEvent.sadc_data_size() )
            # All the passed by events have to have the 'physical' flag set.
            self.ut.assertTrue( expEvent.is_physical() )
            # Here we unpack the current event mock data to check whether the
            # certain actions were performed by pipeline.
            self.mockData = pickle.loads(expEvent.rawevent())
        rc = super(MockSADCProcessor1, self)._V_process_event_payload(expEvent)
        if self.mockData:
            if self.mockData['discriminateEvent']:
                # An entire event has to be marked as discriminated and has to
                # be cut away by the first processor.
                rc |= iEventProcessor.DISCRIMINATE
            if self.mockData['abort']:
                rc |= iEventProcessor.ABORT_CURRENT
            self.mockData = None
        if not (rc & iEventProcessor.ABORT_CURRENT):
            # If event was not aborted, we expect that it will be finalized. We
            # have to control that it will be just the same event, so we use
            # its number:
            wasDiscriminated = (rc & iEventProcessor.DISCRIMINATE)
            wasModified = not (rc & iEventProcessor.NOT_MODIFIED)
            self.finalizeAuxDat = ( expEvent.evnoinspill(), wasDiscriminated, wasModified )
        else:
            self.finalizeAuxDat = None
        return rc

    def _V_process_SADC_profile_event(self, sadcData):
        self.ut.assertEqual( 32, sadcData.samples_size() )
        # Default return code contains NOT_MODIFIED and CONTINUE flags.
        ret = iEventProcessor.RC_ACCOUNTED
        if sadcData.detectorid() in self.mockData['samples2discriminate']:
            # Current sample has to be discriminated. Setting the indication
            # flag:
            ret |= iEventProcessor.DISCRIMINATE
        if self.mockData['modifyEvent']:
            # We have to check in the downstream processors that modifications
            # were actually propagated through the pipeline. The simplest
            # recognizable modification is to merely nullate all the samples.
            for j in range(0, 32):
                sadcData.set_samples(j, 0.)
            # After modification, it is very important to indicate that data
            # modification has been performed with appropriate flag in return
            # code. Note the different syntax for setting flag off:
            ret &= ~iEventProcessor.NOT_MODIFIED
        # We have to check that events do not interfere with each other. One of
        # the uniqe properties of each mock event is omitted IDs list:
        self.ut.assertFalse( sadcData.detectorid() in self.mockData['omittedIDs'] )
        return ret

    def _V_finalize_event_processing(self, event):
        rc = super(MockSADCProcessor1, self)._V_finalize_event_processing( event )
        # Create aux instance were experimental event payload will be unpacked:
        ee = na64Eve.ExperimentalEvent()
        # Unpack:
        event.experimental() \
                    .payload() \
                    .UnpackTo( ee )
        # Omit events marked as non-physical here.
        if not ee.is_physical():
            return rc
        # First, check that we do not finalize aborted event:
        self.ut.assertFalse( self.finalizeAuxDat is None )
        # Since we process only experimental events here:
        self.ut.assertTrue( event.has_experimental() )
        mockData = pickle.loads(ee.rawevent())
        # Check that we're finalizing the latest processed event:
        self.ut.assertEqual( self.finalizeAuxDat[0], ee.evnoinspill() )
        if self.finalizeAuxDat[2]:
            self.ut.assertTrue( mockData['modifyEvent'] )
            for i in range(0, ee.sadc_data_size()):
                sadcData = ee.sadc_data(i)
                # If event was modified, check that we're finalizing packed event,
                # that:
                # 1. does not contain the discriminated samples:
                self.ut.assertFalse( sadcData.detectorid() in mockData['samples2discriminate'] )
                # 2. and all the samples are set to 0:
                self.ut.assertEqual( 32, sadcData.samples_size() )
                for j in range(0, 32):
                    self.ut.assertFalse( sadcData.samples(j) )
        return rc


#
# Mock event processor #2
class MockSADCProcessor2(iSADCProcessor):
    """
    This mock processor performs basic checks after the mock processor #1.
    """
    def __init__(self, ut):
        self.ut = ut
        self.nEventsProcessed = 0
        self.mockData = None  # Will keep special data accompanying mock event
        super(MockSADCProcessor2, self).__init__('mock-SADC-2')

    def _V_process_event(self, event):
        # Only experimental payloads have to pass here.
        self.ut.assertTrue( event.has_experimental() )
        # For non-experimental event (0 == idx || idx >= gNEventsMax),
        # invokation below won't propagate to
        # _V_process_event.../_V_process_SADC_... methods.
        res = super(MockSADCProcessor2, self)._V_process_event(event)
        self.nEventsProcessed += 1
        return res

    def _V_process_event_payload( self, expEvent ):
        if self.nEventsProcessed  == 0 or self.nEventsProcessed  >= gNEventsMax:
            self.ut.assertFalse( expEvent.is_physical() )
        else:
            # Check that event SADC payload actually contains something.
            self.ut.assertTrue( expEvent.sadc_data_size() )
            # All the passed by events have to have the 'physical' flag set.
            self.ut.assertTrue( expEvent.is_physical() )
            # Here we unpack the current event mock data to check whether the
            # certain actions were performed by pipeline.
            self.mockData = pickle.loads(expEvent.rawevent())
        self.eventIdx = expEvent.evnoinspill()
        # We set the eventIdx attribute here to know the event number within
        # samples processing method. The checking of the uniquiness is also
        # good testing method to ensure that event is processed once.
        self.eventIdx = expEvent.evnoinspill()
        if expEvent.is_physical():
            self.ut.assertFalse( self.eventIdx in samplesReached.keys() )
            samplesReached[self.eventIdx] = []
        rc = super(MockSADCProcessor2, self)._V_process_event_payload(expEvent)
        self.eventIdx = None
        if self.mockData:
            # Discriminated event may reach (!) this processor:
            #self.ut.assert???( self.mockData['discriminateEvent'] )
            # Aborted events must not reach this processor:
            self.ut.assertFalse( self.mockData['abort'] )
        return rc

    def _V_process_SADC_profile_event(self, sadcData):
        self.ut.assertEqual( 32, sadcData.samples_size() )
        # Default return code contains NOT_MODIFIED and CONTINUE flags.
        ret = iEventProcessor.RC_ACCOUNTED
        # Discriminated SADC samples that must not reach this processor:
        self.ut.assertFalse(sadcData.detectorid() in self.mockData['samples2discriminate'])
        if self.mockData['modifyEvent']:
            # All the samples in modified event have to be set to zero.
            for j in range(0, 32):
                self.ut.assertFalse(sadcData.samples(j))
        else:
            # All the samples in modified event have to be non-zero.
            for j in range(0, 32):
                self.ut.assertTrue(sadcData.samples(j))
        # We have to check that events do not interfere with each other. One of
        # the uniqe properties of each mock event is omitted IDs list:
        self.ut.assertFalse( sadcData.detectorid() in self.mockData['omittedIDs'] )
        # Will be compared by test case object with samplesToReach:
        assert( not self.eventIdx is None )
        samplesReached[self.eventIdx].append( sadcData.detectorid() )
        return ret

    def _V_finalize_event(self, event):
        rc = super(MockSADCProcessor2, self)._V_finalize_event( event )
        return rc

#
# Test Cases
###########

class TestMockingPipeline(unittest.TestCase):
    """
    This case performs data transmission within StromaV pipeline using mock
    classes generating testing NA64 events.
    """
    def setUp(self):
        """
        Creates a pipeline consisting with data source and mock processor.
        """
        # Test class hierarchy for typecasting resolution
        self.assertTrue( issubclass(iSADCProcessor, iEventProcessor) )
        # instantiate couple processors
        self.proc1 = MockSADCProcessor1(self)
        self.proc2 = MockSADCProcessor2(self)
        # instantiate a pipeline
        self.pipeline = AnalysisPipeline()
        # stack these few processors in a pipeline
        self.pipeline.push_back_processor( self.proc1 )
        self.pipeline.push_back_processor( self.proc2 )
        # instantiate a data source for further use
        self.source = MockExperimentalEventSequence(self)
        # TODO: see #169-18
        #a = self.source.initialize_reading()
        #a.thisown = 0
        

    def test_dataflow_logic(self):
        # generate few events and transmit it via the pipeline
        self.pipeline.process( self.source )
        # Check that all samples that supposed to reach second processor
        # actually reached it:
        self.assertEqual( len(samplesToReach), len(samplesReached) )
        for evNo, sampleNos in samplesToReach.iteritems():
            rs = samplesReached[evNo]
            self.assertEqual( set(rs), set(sampleNos) )

#for e in dir(na64Eve):  # XXX
#    if not e.startswith('_'):
#        print('-', e)

if __name__ == "__main__":
    unittest.main()

