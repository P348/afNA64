# -*- coding: utf-8 -*-
# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# Author: Bogdan Vasilishin <togetherwithra@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
Python testing unit performs checks of the na64_detectors_ids's functions.
The example data will be created based on srcDat dictionary. This topology
then will be tested for few common queries.
"""

from __future__ import print_function
import unittest, os

from castlib3.models import DeclBase
from castlib3.models.filesystem import File
from afNA64.models.rawstat import StoragingNode, RemoteFolder, Run, Chunk

srcDat = {
    'node1' : {
        '/rFolder1-1' : [
            'chunk-42-1',
            'chunk-42-2',
            'chunk-43-1',
        ],
        '/rFolder1-2' : [
            'chunk-42-2',
        ],
    },
    'node2' : {
        '/rFolder2-1' : [
            'chunk-43-1',
            'chunk-44-12',
        ]
    }
}

def test_chunk_name_extract(nm):
    if not nm.startswith('chunk-'):
        raise RuntimeError( 'Not a chunk name: %s'%nm )
    toks = nm.split('-')
    return int(toks[-2]), int(toks[-1])

class RawstatModels(unittest.TestCase):
    """
    Will perform basic tests of database models involving operations with
    remote nodes, folders, runs, chunks, etc.
    """
    def setUp(self):
        """
        Creates mock engine around in-memory sqlite instance to test the basic
        operations.
        """
        from sqlalchemy import create_engine
        #self.engine = create_engine('sqlite:///test.sqlite')
        self.engine = create_engine('sqlite:///:memory:')
        from sqlalchemy.orm import Session
        self.session = Session(self.engine)
        #session.configure(bind=engine)
        DeclBase.metadata.create_all(self.engine)

    def create_simple_structure(self):
        # Test aux extraction functions (used only in this module)
        tr = test_chunk_name_extract('chunk-42-2')
        self.assertEquals( (42, 2), tr )
        tr = test_chunk_name_extract('chunk-11-211')
        self.assertEquals( (11, 211), tr )

        # Create structure:
        for nodeName, nodeDscr in srcDat.iteritems():
            node = StoragingNode(identifier=nodeName, scheme = 'file://')
            for p, chunksDscr in nodeDscr.iteritems():
                fld = RemoteFolder( name=os.path.split(p)[-1], node=node, path=p )
                for cDscr in chunksDscr:
                    rn, cn = test_chunk_name_extract( cDscr )
                    f = File(name=cDscr, parent=fld)
                    r = self.session.query(Run).filter(Run.id==rn).first()
                    if not r:
                        r = Run(id=rn)
                        self.session.add(r)
                    c = self.session.query(Chunk).filter(Chunk.id==cn, Chunk.run==r).first()
                    if not c:
                        c = Chunk(id=cn, run=r, entryModSignature='tst')
                    c.files.append(f)
                    self.session.flush()
            self.session.add(node)
        self.session.commit()

    def test_basic_rawstat_model(self):
        self.create_simple_structure()
        cq = self.session.query( Chunk )
        rq = self.session.query( Run )
        # There must be exactly 4 distinct chunks:
        self.assertEquals(cq.count(), 4)
        # ... and 3 runs:
        self.assertEquals(rq.count(), 3)
        # We have to create two distinct chunks with number 1
        self.assertEquals(cq.filter(Chunk.id==1 ).count(), 2)
        # And only by one chunks with numbers 12 and 1
        self.assertEquals(cq.filter(Chunk.id==2 ).count(), 1)
        self.assertEquals(cq.filter(Chunk.id==12).count(), 1)
        # There must be 2 chunks in 42 run, 1 in 43 and 1 in 44:
        self.assertEquals(cq.filter(Chunk.run==rq.filter(Run.id==42).one()).count(), 2)
        self.assertEquals(cq.filter(Chunk.run==rq.filter(Run.id==43).one()).count(), 1)
        self.assertEquals(cq.filter(Chunk.run==rq.filter(Run.id==44).one()).count(), 1)
        # This use case is extremely important: querying all the file paths by
        # given chunk ID:
        c = cq.filter(Chunk.id==12, Chunk.runID==44).one()
        self.assertEquals( len(c.files), 1 )
        self.assertEquals( 'rFolder2-1', '/'.join([a.name for a in c.files[0].mp.query_ancestors().all()]) )
        c = cq.filter(Chunk.id==2, Chunk.runID==42).one()
        self.assertEquals( len(c.files), 2 )
        pt = set(['rFolder1-1', 'rFolder1-2'])
        tp = set()
        for n, f in enumerate(c.files):
            tp.add('/'.join([a.name for a in c.files[0].mp.query_ancestors().all()]))
        self.assertTrue(pt, tp)

    def tearDown(self):
        self.session.close()

if __name__ == "__main__":
    unittest.main()
