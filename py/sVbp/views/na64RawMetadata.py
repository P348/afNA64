# -*- coding: utf-8 -*-
# Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

import flask, json, os, datetime
import math as m
from flask import Response, Blueprint, request, render_template, url_for
from flask_api import status as HTTP_STATUS_CODES
from sqlalchemy import func, exists, and_

from castlib3.models.filesystem import File
from afNA64.models.rawstat import StoragingNode, RemoteFolder, Run, Chunk
from afNA64.resources.parsing import timestamp_from_datetime_string

from sVresources import apps
from sVresources.db.instance import get_or_create
from sVresources.utils.contentType import expected_content_type
from sVresources.utils.sqladict import object_to_dict
from afNA64.resources.parsing import rxChunkFilename

bp = Blueprint('na64-metadata', __name__,
               template_folder='../templates',
               static_folder='../static',
               url_prefix='/na64-mdat')

# ...

@bp.route('/import/files', methods=['POST'])
@expected_content_type
def recieve_files_import():
    """
    This view recieves the JSON within the POST request creating the files
    entries in the server's database. Recieved entries will be affilated to
    the particular remote resource.
    TODO: we have to decide how server would figure out client identity
    further.
    """
    jsDat = request.json
    if not jsDat:
        return {'details' : 'Request did not bring the JSON data.'}, \
            HTTP_STATUS_CODES.HTTP_400_BAD_REQUEST
    nEntriesConsidered = 0
    nEntriesCreated = 0
    nChunksFound = 0
    # TODO: kludge below. Client's identity and URI scheme have to be either
    # set by server routines, or brought by client data.
    clientIdentity = {
                'identifier' : 'castor.cern.ch',
                'scheme' : 'castor'
            }
    rNode, nodeCreated = get_or_create( StoragingNode, session=apps.db, **clientIdentity )
    nEntriesConsidered += 1
    if nodeCreated:
        apps.db.add(rNode)
        apps.db.flush()
        nEntriesCreated += 1
    for path, files in jsDat.iteritems():
        name = os.path.split(path)[-1]
        rFolderIdentity = {
                    'name' : name,
                    'path' : path
                    # TODO: try to retrieve timestamps (modified)
                }
        rFolder, folderCreated = get_or_create( RemoteFolder, session=apps.db,
                **rFolderIdentity )
        if folderCreated:
            apps.db.add(rFolder)
            apps.db.flush()
            nEntriesCreated += 1
        nEntriesConsidered += 1
        # TODO: considerChunks have to be set depending on internal data. It's
        # a decision that server shall made.
        considerChunks = True
        # TODO: server has to make decision about whether to trust the client
        # sending information about previously unknown chunks chunks.
        trustNewChunks = True
        for f in files:
            # file entry has fields:
            #   - a32 --- adler32 checksum
            #   - s --- size in bytes,
            #   - m --- modification timestamp
            #   - n --- filename
            fModified = False
            rFile, fModified = get_or_create( File, session=apps.db,
                        name=f['n'],
                        parent=rFolder )
            if f.get('s', None) != rFile.size:
                rFile.size = int(f['s'])
                fModified = True
            if f.get('m', None) != rFile.modified:
                rFile.modified = int(f['m'])
                fModified = True
            if f.get('a32', None) != rFile.adler32:
                rFile.adler32 = f['a32']
                fModified = True
            if fModified:
                nEntriesCreated += 1
                apps.db.add(rFile)
            mtch = rxChunkFilename.match( rFile.name )
            if considerChunks and mtch:
                # Look who found a chunk!
                runNo, chunkNo = int(mtch.groupdict()['runno']), int(mtch.groupdict()['chunkno']) - 1000
                chunk = apps.db.query(Chunk).filter(Chunk.runID==runNo, Chunk.id==chunkNo).first()
                if not chunk:
                    if trustNewChunks:
                        run, runCreated = get_or_create( Run, session=apps.db, id=runNo )
                        if runCreated:
                            apps.db.add(run)
                            apps.db.flush()
                            nEntriesCreated += 1
                        chunk = Chunk( id=chunkNo, run=run, entryModSignature = 'client-import-initial' )
                        apps.db.add(chunk)
                    else:
                        # If this situation appeared, the current index of chunks
                        # is older, or client tries to send us bullshit. TODO: what
                        # do we do here?
                        print('!!! Unknown chunk: %d:%d'%(runNo, chunkNo))  # XXX
                else:
                    #chunk.locations.append(rFolder)
                    chunk.entryModDate = datetime.datetime.utcnow()
                    chunk.entryModSignature = 'client-import'  # todo: meaningful
                    apps.db.add(chunk)
                    nChunksFound += 1
                if chunk and rFile:
                    if rFile not in chunk.files:
                        chunk.files.append(rFile)
                        apps.db.add(chunk)
            nEntriesConsidered += 1
    apps.db.flush()
    print( 'Chunks discovered: %d'%nChunksFound )  # XXX
    apps.db.commit()
    return Response( '', HTTP_STATUS_CODES.HTTP_200_OK )  # TODO

@bp.route('/tmp/xxx-to-remove', methods=['GET'])
@expected_content_type
def kludge__files_to_remove():
    """
    Temporary view rendering files that may be removed from pcdmfs01
    """
    #chunkFileName = 'cdr%05d-%06d.dat'%(1000 + chunk.id, chunk.run.id)
    #q = apps.db.query(Chunk) \
    #        .join(Chunk.files) \
    #        .group_by(Chunk) \
    #        .having(File.adler32)

    q = apps.db.query(Chunk) \
            .join(Chunk.files) \
            .group_by(Chunk) \
            .having(func.count(File.id)>1)
    mismatchCounter = 0
    for c in q.all():
        if c.files[0].adler32 != c.files[1].adler32:
            #p1 = '/'.join([a.name for a in c.files[0].mp.query_ancestors().all()])
            #p2 = '/'.join([a.name for a in c.files[1].mp.query_ancestors().all()])
            print('Checksum mismatch:', c, c.files[0].adler32, c.files[1].adler32)
            mismatchCounter+=1
            if mismatchCounter > 100:
                break
    
    return Response( '%d'%q.count(), HTTP_STATUS_CODES.HTTP_200_OK )


@bp.route('/sync-runs', methods=['PUT'])
@expected_content_type
def synchronize_runs():
    """
    This view expects JSON array in form defined in extract-runs-list.py:
    [
        {'Run' : <int-string>|None,
         'Chunk' : <int-string>,
         'Started' : <str like '2016-11-03 03:40:58'>,
         'Status' : None|'done'|'running',
         'FileName' : </abs/path/to/file/tok-chunkno-runno.dat>,
         'Size' : <size-in-bytes>,  // (optional)
         'CheckSum' : <adler32-8-hex-num>,
        },
        { ... },
        ...
    ]
    """
    entries = request.json
    #print( 'XXX', len(request.data), 'entries=', len(data),
    #            request.headers.get('Content-Type', '<unknown content type>') )
    print( '#entries received:', len(entries) )
    latestRun = entries[0]['Run']
    if not latestRun:
        return {'details' : 'Data did not supplied the run number in first entry.'}, \
            HTTP_STATUS_CODES.HTTP_400_BAD_REQUEST
    else:
        latestRun = int(latestRun)
    for entry in entries:
        if len(entry['Run']) and latestRun != entry['Run']:
            latestRun == int(entry['Run'])
        run, newRunCreated = get_or_create( Run
            , id=latestRun
            # ...
            , session=apps.db )
        chunk, newChunkCreated = get_or_create( Chunk
            , runID=run.id
            , id=int(entry['Chunk'])
            # ...
            , session=apps.db )
        # TODO: update
    return Response( '', HTTP_STATUS_CODES.HTTP_200_OK )

@bp.route('/observe/indexes')
def observe_indexes():
    return render_template( 'observe_mdat.html'
                , AJAXTreeAddr='dyn-indexes'
                , AJAXGroupDetails=url_for('na64-metadata.dyn_group_details')
                , AJAXItemDetails=url_for('na64-metadata.dyn_entry_details')
                , AJAXParameters=['runNoRange','runNo', 'groupID', 'chunkNo', 'entryID']
                , groupFields=['group', 'runNoRange', 'runNo', 'groupID', 'chunkNo', 'entryID'] )

@bp.route('/dyn/group-details')
@expected_content_type
def dyn_group_details():
    groupID = request.args.get( 'groupID', None )
    if groupID is None:
        return {'details' : 'request did not submitted groupID'}, \
                HTTP_STATUS_CODES.HTTP_400_BAD_REQUEST
    if groupID.startswith('run-'):
        runNo = int(request.args.get( 'runNo'))
        r = apps.db.query(Run).filter(Run.id==runNo).first()
        return object_to_dict(r, omitRels=['.chunks.files']), \
                HTTP_STATUS_CODES.HTTP_200_OK
    return {'name' : 'mock group node data'
           ,'groupID' : groupID }, HTTP_STATUS_CODES.HTTP_200_OK

@bp.route('/dyn/entry-details')
@expected_content_type
def dyn_entry_details():
    runNo = request.args.get( 'runNo', None )
    # This property is set for chunk leaf entries:
    chunkNo = request.args.get( 'chunkNo', None )
    # This property is set for non-chunk leaf entries:
    entryID = request.args.get( 'entryID', None )
    if runNo or chunkNo:
        if runNo is None or chunkNo is None:
            return {'details' : 'Request did not submitted run number or chunk number.'}, \
                HTTP_STATUS_CODES.HTTP_400_BAD_REQUEST
        runNo = int(runNo)
        chunkNo = int(chunkNo)
        c = apps.db.query(Chunk).filter(Chunk.runID==runNo, Chunk.id==chunkNo).first()
        if not c:
            return {'details': 'No chunk %d-%d in database.'%(int(runID), int(chunkNo)) }, \
                    HTTP_STATUS_CODES.HTTP_404_NOT_FOUND
        return object_to_dict(c, omitRels=['.run', '.files.chunks', '.files.parent', '.files.mp_*']), \
                HTTP_STATUS_CODES.HTTP_200_OK
    else:
        pass  # return other leaf entry information
    return {'name' : 'mock child node data'}, HTTP_STATUS_CODES.HTTP_200_OK

@bp.route('/observe/dyn-indexes', methods=['POST'])
@expected_content_type
def indexed_files():
    """
    View retrieving real data.
    In NA64 we already have few thousands of runs that are not convinient to
    display at once, so here we offer a bit more convinient way. The "stock"
    runs (raw ones that have been written by DAQ and were not being processed)
    will be grouped by hundreds, e.g. 12xx, 13xx, ... .
    """
    group = request.form.get( 'group', None )
    if group is None:
        # Return parent structure:
        return [
                { 'name' : 'Stock', 'isParent' : True, 'group' : 'stock', 'groupID' : 'stock-catalogue' },
                { 'name' : 'Users', 'isParent' : True, 'group' : 'users', 'groupID' : 'users-catalogue' },
                { 'name' : 'summary', 'entryID' : 'summary' }
            ], HTTP_STATUS_CODES.HTTP_200_OK
    elif group == 'stock':
        # We're in "stock" folder and have to return list of folders grouping 
        # runs by hundreds.
        rIDLimits = apps.db.query(
                        func.min(Run.id).label('firstRunID'),
                        func.max(Run.id).label('lastRunID'),
                    ).one()
        if rIDLimits.firstRunID is None or rIDLimits.lastRunID is None:
            return [], HTTP_STATUS_CODES.HTTP_200_OK
        minRunHndrd, maxRunHndrd = int(m.floor(rIDLimits.firstRunID/100.)) \
                                 , int(m.ceil(rIDLimits.lastRunID/100.))
        retLst = []
        for nHndrd in range(minRunHndrd, maxRunHndrd):
            mn, mx = nHndrd*100, (nHndrd+1)*100
            (ret, ), = apps.db.query( exists().where(and_(Run.id > mn, Run.id < mx) ) )
            if ret:
                retLst.append( { 'name' : 'Runs %d-%d'%(mn, mx)
                               , 'isParent' : True
                               , 'group' : 'runs-list'
                               , 'groupID' : 'runs-list-%d-%d'%(mn, mx)
                               , 'runNoRange' : 'runs-list-%d-%d'%(mn, mx) } )
            #retLst.append(  )
        return retLst, HTTP_STATUS_CODES.HTTP_200_OK
    elif group == 'runs-list':
        # We're now in "hundred of run" group and have to return runs list
        # for displaying.
        mn, mx = map(int, request.form.get('runNoRange').split('-')[-2:])
        # Return list of hundreds:
        runsList = list([ { 'name' : '%d'%run.id
                          , 'isParent' : True
                          , 'group' : 'run'
                          , 'groupID' : 'run-%d'%run.id
                          , 'runNo' : run.id } \
                        for run in apps.db.query(Run) \
                            .filter(and_(Run.id > mn, Run.id < mx)) \
                            .order_by(Run.id).all() ])
        return runsList, HTTP_STATUS_CODES.HTTP_200_OK
    elif 'run' == group:
        # "Run" group --- we have to return list of chunks affilated with
        # particular run.
        runNo = int(request.form.get('runNo'))
        chunksList = list([ { 'name'  : 'chunk-%d'%chunk.id,
                              'runNo' : runNo,
                              'chunkNo' : chunk.id} \
                        for chunk in apps.db.query(Chunk) \
                            .filter(Chunk.runID == runNo) \
                            .order_by(Chunk.id) \
                            .all() ])
        return chunksList, HTTP_STATUS_CODES.HTTP_200_OK
    print( 'Unknown group: %s'%group )  # XXX
    return [], HTTP_STATUS_CODES.HTTP_400_BAD_REQUEST

@bp.route('/observe/test-indexes', methods=['POST'])
@expected_content_type
def test_indexed_files():
    """
    Testing stub view, to check the JS Tree API.
    """
    #print( 'Request brought: ', request.form )
    group = request.form.get( 'group', None )
    if group is None:
        # Return parent structure:
        return [
                { 'name' : 'Stock', 'isParent' : True, 'group' : 'stock' },
                { 'name' : 'Users', 'isParent' : True, 'group' : 'users' },
                { 'name' : 'summary' }
            ], HTTP_STATUS_CODES.HTTP_200_OK
    elif group == 'stock':
        # Return list of known runs:
        return [
                { 'name' : '2119', 'isParent' : True, 'group' : 'run', 'runNo' : 2119 },
                { 'name' : '2120', 'isParent' : True, 'group' : 'run', 'runNo' : 2120 }
                # ... TODO: get real run numbers from database
            ], HTTP_STATUS_CODES.HTTP_200_OK
    elif group == 'users':
        # Return list of metadata pushed by user's software:
        return [
                { 'name' : 'choosen events' },
                { 'name' : 'suspicious events' },
                { 'name' : '...whatsoever' }
            ], HTTP_STATUS_CODES.HTTP_200_OK
    elif 'run' == group:
        # shall raise HTTP_400_BAD_REQUEST on error:
        runNo = request.form['runNo']
        return [
                { 'name' : '10001' },
                { 'name' : '10002' }
                # ... TODO: get real chunk data from database
            ], HTTP_STATUS_CODES.HTTP_200_OK
    # Well, nothing triggered a return up here. This has be malformed request:
    return [], HTTP_STATUS_CODES.HTTP_400_BAD_REQUEST

sVresources_Blueprint = bp

