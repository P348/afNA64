# -*- coding: utf-8 -*-
# Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

import flask, json, os, datetime
import math as m
from flask import Response, Blueprint, request, render_template, url_for
from flask_api import status as HTTP_STATUS_CODES
from sqlalchemy import func, exists, and_

from sVresources import apps

# XXX?

bp = Blueprint('rpc', __name__,
               template_folder='../templates',
               static_folder='../static',
               url_prefix='/na64-mdat')

"""
Various views responsible for remote procedure calls.
"""


@bp.route('/cstl3-instances/', methods=['POST'])
def manage_cstl3_instances():
    """
    This view is a page dedicated to management of the nodes running listening
    CastLib3 process: as a daemon, as a daemonized child under control of
    supervisord, or as a Celery task.
    """
    pass
