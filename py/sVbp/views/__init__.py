from na64RawMetadata import sVresources_Blueprint as mdatBP

sVresources_Blueprints = [
        mdatBP
        #, ...
    ]

sVresources_ObservablePages = [
        # title, destination
        ('Metadata indexes', 'na64-metadata.observe_indexes')
    ]

