# -*- coding: utf-8 -*-
# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

"""
File containing logging configuration for afNA64.
"""

import logging

# create logger
gLogger = logging.getLogger('afNA64')
gLogger.setLevel( logging.DEBUG )

# create console handler and set level to debug
sh = logging.StreamHandler()
sh.setLevel(logging.DEBUG)  # INFO?
formatter = logging.Formatter('%(name)s/%(levelname)s: %(message)s')
sh.setFormatter(formatter)

ch = logging.FileHandler('/tmp/afNA64.log' )
ch.setLevel( logging.DEBUG )

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

gLogger.addHandler(ch)
gLogger.info('*** New afNA64 session started ***')
gLogger.addHandler(sh)

# TODO: bind it with wrapped py-app (the C++ class has to forward its logging
# to the gLogger object).

# XXX:
#gLogger.debug('debug message')
#gLogger.info('info message')
#gLogger.warn('warn message')
#gLogger.error('error message')
#gLogger.critical('critical message')


