# -*- coding: utf-8 -*-
# Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

from __future__ import print_function

"""
This module contains model declarations representing raw data entities in NA64.
As for the experimental sessions in NA64, the setup and experimental conditions
are kept immutable (fixed trigger system and detector set) during periods of
time called "runs".

During each run the SPS accelerator drops the beam in ~200 bursts (here called
"spills"). Each burst lasts few seconds while the DAQ system actively writes
the data. The SPS then goes to another cycle of acceleration that lasts for
10-20 secs before next burst will be dropped again. Typically, one spill
generates ~5e6 events to be stored by the DAQ system.

Within each run the DAQ system accumulates statistics and writes it into the
files called "chunks" here. Typically, each run consists of ~8 chunks. The
chunk is merely a file, no more than 1Gb of size that keeps ~12 spills.

In this models we reflect the hierarchy Run/Spill/Event in form suitable
for database.

Though we haven't yet met the situation when one spill was partially written in
two chunks, we have to envisage this.
"""

import datetime, urlparse

from sqlalchemy import Table, Column, ForeignKey, Integer, String, DateTime, \
                ForeignKeyConstraint
from sqlalchemy.orm import relationship

# NOTE: we're using here declarative base gotten from castlib3 instead of
# direct resources server. They're not basically the same for cases when 
# this python models are used within dedicated environment (IPython/unittest).
from castlib3.models import DeclBase
from castlib3.models.filesystem import Folder, File, RemoteFolder, StoragingNode

remote_chunks_files = Table('remote_chunks_files', DeclBase.metadata,
        Column('run_id',   Integer ),
        Column('chunk_id', Integer ),
        Column('file_id',  Integer, ForeignKey(File.id)),
        ForeignKeyConstraint( ['run_id', 'chunk_id'], ['na64_chunks.runID', 'na64_chunks.id'] )
    )

class Run(DeclBase):
    """
    Model representing single NA64 run.
    """
    __tablename__ = 'na64_runs'
    id = Column(Integer, primary_key=True)
    chunks = relationship("Chunk", back_populates="run")


class Chunk(DeclBase):
    """
    Model representing single NA64 data chunk formed by DAQ system.
    """
    __tablename__ = 'na64_chunks'
    id = Column(Integer, primary_key=True)
    runID = Column(Integer, ForeignKey("na64_runs.id"), primary_key=True)

    run = relationship("Run", back_populates="chunks")

    # The data retrieved by utility code. Refers to start of the first spill
    # and end of the last.
    startTimestamp = Column(Integer)
    endTimestamp = Column(Integer)

    # Single chunk may be distributed among multiple hosts/storaging back-ends:
    # CASTOR and EOS CERN storages are supported as well as local hosts.
    files = relationship( File,
                          secondary=remote_chunks_files,
                          backref='chunks'
                        )

    # Aux information referencing latest modification.
    entryModDate = Column(DateTime, default=datetime.datetime.utcnow)
    entryModSignature = Column(String, default='unknown')

    def __str__(self):
        return "Chunk(id=%d-%d, %s-%s)"%(
                self.runID, self.id, self.startTimestamp, self.endTimestamp)

    def str_locations_list(self):
        """
        Examples:
            castor:///castor/cern.ch/na64/data/cdr
            nfs://pcdmfs01.cern.ch/data/cdr
            file:///data/cern/p348/2016
        Returns list of strings referring particular locations where chunk may
        be found.
        """
        ret = []
        for l in self.files:
            ret.append( urlparse.urljoin( l.node.scheme + l.node.identifier, l.path ) )
        return ret

