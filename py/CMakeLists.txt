# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# Author: Bogdan Vasilishin <togetherwithra@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

include(${Goo_CMAKE_MODULES_DIR}/PyExtensions.cmake)

if( StromaV_RPC_PROTOCOLS AND na64ee_SUPPORT_DDD_FORMAT )
    list( APPEND AFNA64_PYMODULES dataSources.i )
endif()

if( StromaV_RPC_PROTOCOLS AND StromaV_ANALYSIS_ROUTINES )
    list( APPEND AFNA64_PYMODULES processors.i )
endif()

if( StromaV_RPC_PROTOCOLS )
    list( APPEND AFNA64_PYMODULES na64Events.i )
endif()

list( APPEND AFNA64_PYMODULES na64DetectorIds.i)

if( AFNA64_PYMODULES )
    set( CMAKE_SWIG_FLAGS ${CMAKE_SWIG_FLAGS} -nodefaultctor -Wextra -templatereduce -I${StromaV_INSTALL_PREFIX}/${StromaV_SWIG_IFACES_DIR} )
    Goo_PY_EXTENSIONS( INTERFACES ${AFNA64_PYMODULES}
                       LINK_LIBS ${PY_EXT_LINK_LIBS}
                       WRAPPER_SOUCES WRAPPER_SRCS
                       SETUP_TEMPLATE setup.py.in )
    # Create symlink to make python's eggs mechanics happy:
    add_custom_target( py_link_target ALL
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_BINARY_DIR}/py           ${CMAKE_BINARY_DIR}/afNA64
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/py/resources ${CMAKE_BINARY_DIR}/afNA64/resources
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/py/models    ${CMAKE_BINARY_DIR}/afNA64/models
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/py/cstl3-ext ${CMAKE_BINARY_DIR}/afNA64/cstl3-ext
        COMMAND ${CMAKE_COMMAND} -E create_symlink ${CMAKE_SOURCE_DIR}/py/sVbp      ${CMAKE_BINARY_DIR}/afNA64/sVbp)
    # Note: workarounds for recent C++ compiler warnings
    set_source_files_properties( ${WRAPPER_SRCS}
        PROPERTIES COMPILE_FLAGS "-Dregister= -Wno-self-assign -Wno-shadow" )

    if( StromaV_RPC_PROTOCOLS )
        #protobuf_generate_python( PY_MSG_MODULES ../na64_event.proto )
        #add_custom_target( NA64PyEvent ALL DEPENDS ${PY_MSG_MODULES} )
        #install( FILES ${PY_MSG_MODULES} DESTINATION ${PY_MODULES_INSTALL_DIR}/${PYPKGNM} )

        # TODO: refused as _common*.i is not a target. Best way would be
        # individually modify properties of each .i file pushed into dpe graph by
        # conditional blocks above.
        #FILE(GLOB COMMON_INCLUDES _common*.i)
        #message( STATUS "...${COMMON_INCLUDES}" )
        #add_dependencies( NA64PyEvent ${COMMON_INCLUDES} )
    endif( StromaV_RPC_PROTOCOLS )
else( AFNA64_PYMODULES )
    message( STATUS "No py-bindings to be generated" )
endif( AFNA64_PYMODULES )

