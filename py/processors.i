%module(directors="1") processors

/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

%import "_commonIgnore.i"

%include "sV/_gooExceptionWrapper.i"
//%ignore sV::iMetadataTypeBase;

%import(module="StromaV.pipeline") "sV/pipeline.i"
%import(module="StromaV.sVEvents") "sV/sVEvents.i"  // xxx?
%import(module="afNA64.na64Events") "na64Events.i"

%rename(SADC_WF_Aligner)        na64::dprocessors::aux::WFAligner;
//%rename(MetadataDict)           ::sV::MetadataDictionary<na64ee::EventID>;

%{

# include "afNA64_config.h"

#if !defined( StromaV_RPC_PROTOCOLS ) || !defined( StromaV_ANALYSIS_ROUTINES )
#error "Either StromaV's RPC_PROTOCOLS or StromaV_ANALYSIS_ROUTINES not \
defined. Unable to build event processors in module."
#else
#include "analysis/processors/sadcWFAlignment.hpp"
#include "analysis/processors/sadcWFReconstruction.hpp"
#endif

#include <StromaV/analysis/pipe_fj.hpp>

using na64::analysis::NA64ExperimentalEvent;
using na64::analysis::ExpEventProcessor;

%}

%include "afNA64_config.h"

typedef ::na64::events::ExperimentalEvent_Payload NA64ExperimentalEvent;
%template(ExpEventProcessor) sV::aux::iTEventPayloadProcessor<sV::events::ExperimentalEvent, NA64ExperimentalEvent>;
typedef ::sV::aux::iTExperimentalEventPayloadProcessor<NA64ExperimentalEvent>
                                                            ExpEventProcessor;
%template(NA64ExpEventProcessor) ::sV::aux::iTExperimentalEventPayloadProcessor<NA64ExperimentalEvent>;

%feature("director") na64::analysis::iSADCProcessor;
%feature("director") na64::analysis::iAPVProcessor;
%include "analysis/event_interfaces.hpp"
%include "analysis/processors/sadcWFAlignment.hpp"
%include "analysis/processors/sadcWFReconstruction.hpp"

/*%include "tst_2rem.hpp"*/

// vim: ft=swig
