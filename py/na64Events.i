%module na64Events

/*
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

%import "_commonIgnore.i"
%include "std_string.i"
%include "sV/_gooExceptionWrapper.i"
%import(module="StromaV.sVEvents") "sV/sVEvents.i"

%import "google/protobuf/stubs/port.h"

/* See note about scoped names concatenation issue in py/sVEvents.i
 */
%rename(ExperimentalEvent)           na64::events::ExperimentalEvent_Payload;

%ignore na64::events::protobuf_InitDefaults_na64_5fevent_2eproto_impl;
%ignore na64::events::protobuf_AddDesc_na64_5fevent_2eproto_impl;
%ignore na64::events::protobuf_AssignDesc_na64_5fevent_2eproto;
%ignore na64::events::protobuf_ShutdownFile_na64_5fevent_2eproto;

%include "../na64_event.pb.h"

%{

# include "afNA64_config.h"

#if !defined( StromaV_RPC_PROTOCOLS ) || !defined( StromaV_ANALYSIS_ROUTINES )
#error "Either StromaV's RPC_PROTOCOLS or StromaV_ANALYSIS_ROUTINES not"
#error "defined. Unable to build event processors in module."
#else
#include "../na64_event.pb.h"
#endif

/*#include "tst_2rem.hpp"*/

%}

/*%include "tst_2rem.hpp"*/

// vim: ft=swig
