%module(directors="1") dataSources
/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* This SWIG interface module defines data resources and metadata handles
 * specific for NA64 making them available from Python (and, probably, other
 * programming languages).
 */

%include "std_string.i"
%include "stdint.i"

%include "std_shared_ptr.i"
%include "std/std_iostream.i"

%import "_commonIgnore.i"

%shared_ptr( std::istream );
%shared_ptr( std::ostream );

%clear std::shared_ptr<std::istream>;
%clear std::shared_ptr<std::ostream>;

%include "sV/std_stream.i"
%include "sV/std_unique_ptr.i"
%include "sV/_gooExceptionWrapper.i"

%include "sV_config.h"
%import(module="StromaV.pipeline") "sV/pipeline.i"

%ignore sV::AbstractApplication::ASCII_Entry;

//%import "StromaV/analysis/pipeline.hpp"

%rename(DDDEvManagerHandle)     afNA64::dsources::ddd::EvManagerHandle;
%rename(DDDChunk)               afNA64::dsources::ddd::Chunk;

%rename(lower)                  *::from;
%rename(upper)                  *::to;

%defaultctor ::NA64_UEventID;

%rename("print_to_stl_ostream", fullname=1) na64ee::DDDIndex::print(std::ostream &) const;
%rename("experimental_payload") afNA64::dsources::DDD::aux::EventsTranscoder::experimental_payload_ref() const;
%rename("ddd_manager") afNA64::dsources::DDD::aux::EventsTranscoder::get_ddd_manager() const;
%rename("mutable_current_event") afNA64::dsources::DDD::EvManagerHandle::current_event();

%feature("director") sV::ITMetadataStore<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID >;

%extend na64ee::DDDIndex {
%pythoncode {
    def __reduce__(self):
        size = self.serialized_size()
        series = bytearray( size )
        serializedSize = self.serialize(series, size)
        return DDDIndex_deserialize, (series,)
}
}

%typemap(in) uint8_t* () {
    if( ! PyByteArray_Check($input) ) {
        SWIG_Error(SWIG_TypeError, "File bytearray expected.");  
        SWIG_fail;
    }
    $1 = (uint8_t*) PyByteArray_AsString($input);
}

%feature("novaluewrapper") std::vector<uint8_t>;
%typemap(out) std::vector<uint8_t> () {
    $result = Py_BuildValue( "s#", $1.data(), $1.size() );
}

%init {
    // Mandatory printf-initialization:
    const char fmt[] = fmt_EVENTID;
    NA64EE_register_event_id_print( fmt[0] );
}

%{

#include "na64ee_config.h"
#include "na64ee_exception.hpp"
#include "sV_config.h"
#include "afNA64_config.h"

#if !defined( StromaV_RPC_PROTOCOLS ) || !defined( na64ee_SUPPORT_DDD_FORMAT )
#error "Either StromaV's RPC_PROTOCOLS or na64ee_SUPPORT_DDD_FORMAT not " \
"defined. Unable to build data DDD source module."
#endif  // !defined( StromaV_RPC_PROTOCOLS ) || !defined( na64ee_SUPPORT_DDD_FORMAT )

#include <na64_event_id.h>
#include <na64_event_id.hpp>
#include <na64ee_readout.hpp>
#include "analysis/dsources/ddd.hpp"
#include "metadata/ddd/md_type.hpp"
#include <StromaV/analysis/pipe_fj.hpp>

%}

wrap_unique_ptr(ProxyEventSequence, sV::aux::iEventSequence);

%include "na64ee_config.h"
%include "sV_config.h"
%include "afNA64_config.h"

#define __attribute__(x) /* SWIG does not like me */
%include "na64_event_id.h"
%include "na64_event_id.hpp"
%include "na64ee_readout.hpp"
#undef __attribute__

/*
 * Classes to wrap here (order matters!)
 */

%include "metadata/c_types.h"
%include "metadata/type_base.hpp"
%template(iNA64MetadataType) sV::aux::iTemplatedEventIDMetadataType<
                                ::NA64_UEventID >;

/* Metadata type template base */
%include "metadata/type.tcc"
%template(iDDDChunkMetadataType) sV::iTMetadataType<
                                ::NA64_UEventID,
                                na64ee::DDDIndex >;

/* Instantiate sectional event source */
%include "analysis/evSource_identifiable.tcc"
%template(iDDDIdendifiableChunk) sV::mixins::iIdentifiableEventSource<
                                    na64ee::ChunkID >;

%include "analysis/evSource_RA.tcc"
%template(iDDDRandomAccessEventStream) sV::IRandomAccessEventStream<
                                    ::NA64_UEventID>;
%template(iDDDRandomAccessEventSource) sV::aux::iRandomAccessEventSource<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex >;

%include "metadata/traits.tcc"
%template(DDDMetadataTypeTraits) sV::MetadataTypeTraits<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID>;
%include "metadata/ddd/traits.hpp"

%include "metadata/dictionary.tcc"
%template(MetadataDict) sV::MetadataDictionary<
                                    ::NA64_UEventID >;

%include "metadata/store.tcc"

%template(DDDReadingRange) sV::aux::RangeReadingMarkupEntry<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID >;

%template(iDDDMetadataStore) sV::ITMetadataStore<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID >;

%template(iDDDDisposableSourceManager) sV::ITDisposableSourceManager<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID >;

%template(iDDDEventQueryableStore) sV::ITEventQueryableStore<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID >;

%include "metadata/type_cached.tcc"

%template(iDDDRunMetadataType) sV::iTCachedMetadataType<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID >;
%include "analysis/evSource_sectional.tcc"
%template(iDDDSectionalEventSource) sV::iSectionalEventSource<
                                    ::NA64_UEventID,
                                    na64ee::DDDIndex,
                                    na64ee::ChunkID >;
// Tell SWIG that it is ok to be abstract sometimes:
//%warnfilter(403) sV::aux::iTemplatedEventIDMetadataType< ::NA64_UEventID >;

%include "metadata/ddd/md_type.hpp"
%include "analysis/dsources/ddd.hpp"

// vim: ft=swig
