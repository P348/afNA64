#!/usr/bin/env python2

from __future__ import print_function

import sys
sys.path.append('/home/crank/Projects/CERN/NA64-meta/install/lib/python2.7/site-packages')

import afNA64.Processors
import afNA64.DataSources

import json, tempfile

def ls_objs_dir( mod ):
    ls_ = dir( mod )
    ls = sorted(list(ls_))
    for entry in ls:
        if "cvar" == entry:
            print( "    + cvar" )
            print_c_variables_in_module( mod.cvar )
        elif "_" != entry[0] and "_swigregister" != entry[-13:]:
            print( "    -", entry )

def print_c_variables_in_module( cvarObj ):
    # TODO:
    #print( ">>> ", cvarObj )
    #for k in cvarObj.__dict__:
    #    print( "        - ", k, " <- " )
    pass

def print_module_content( mod ):
    print( mod.__package__, "/", mod.__name__, ":" )
    ls_objs_dir( mod )

print_module_content( afNA64.Processors )
print_module_content( afNA64.DataSources )

#
# Construct objects and checks some access features:

mdDct = afNA64.DataSources.MetadataDict()
dddMdType = afNA64.DataSources.DDDMetadataType()

print( " *** Checking parental classes to be accessible:" )
print( "List of all methods of class", mdDct.__class__.__name__, ":" )
ls_objs_dir( mdDct )
print( "List of all methods of class", dddMdType.__class__.__name__, ":" )
ls_objs_dir( dddMdType )
print( dddMdType )
#print( "Check top, the DDDFileInfoType class:" )
#print( dddMdType.swig_acc_chk_m2() )
print( "Check parent, the iMetadataType<na64ee::UEventID, na64ee::DDDIndex>:" )
print( "  dddMdType.type_index() <-", dddMdType.type_index() )
print( "  dddMdType.get_index() <-", dddMdType.get_index() )
print( "  iDDDChunkMetadataType_type_index <-",
                afNA64.DataSources.iDDDChunkMetadataType_type_index() )
print( "(it's ok that they're zeroed, we hadn't yet registered 'im)" )
print( "Check lowest, the iMetadataTypeBase:" )
print( dddMdType.name() )
mdDct.register_metadata_type( dddMdType )
print( "  dddMdType.type_index() <-", dddMdType.type_index() )
print( "  dddMdType.get_index() <-", dddMdType.get_index() )
print( "  iDDDChunkMetadataType_type_index <-",
                afNA64.DataSources.iDDDChunkMetadataType_type_index() )
print( "(now, there have to appear some non-zero ID)" )
print( " *** Workaround passed." )

#
# This lines were written to check, wether SWIG correctly resolves implicit
# upcast of derived instances:
#obj = afNA64.Processors.Derived()
#ckr = afNA64.Processors.Checker()
#ckr.check( obj )            # Goes smoothly.
#ls_objs_dir( dddMdType )    # Indicates ['this']
#ls_objs_dir( ckr )          # Indicates ['this', 'check']
#obj.is_you_derived()        # Indeed.

ls_objs_dir( afNA64.DataSources.DDDChunk )

#istreamRef = afNA64.DataSources.istream()
#istreamRef.open('/data/disk1/Statistics/CERN/na64/2016/cdr01007-002535.dat')

# at Fresnel:
#artefact = open( '/data/disk1/Statistics/CERN/na64/2016/cdr01007-002535.dat', 'r' )
#c = afNA64.DataSources.DDDChunk(
#        artefact,
#        afNA64.DataSources.ChunkID( 2535, 7 ),
#        mdDct )
# at Pegasus:
filePathStr = '/data/cern/p348/2016/cdr01005-001445.dat'
#filePathStr = '/data/disk1/Statistics/CERN/na64/2016/cdr01007-002535.dat'
artefact = open( filePathStr, 'r' )
c = afNA64.DataSources.DDDChunk(
        artefact,
        afNA64.DataSources.ChunkID( 2535, 7 ),
        mdDct )

#ls_objs_dir( c )
#print( mdDct )
md = c.metadata( )
print( "Metadata acquired." )

print( "List of all methods of class", md.__class__.__name__, ":" )
ls_objs_dir( md )
#print( md.serialized_size() )

#na64.AnalysisDictionary_list_processors()
#print dir(na64.na64_processors)
#help( na64.na64_processors.File )
#ap = sV.analysis.AnalysisPipeline()

#import code
#code.interact(local=locals())

#jsStream = cStringIO.StringIO()  # doesn't work this way since cStringIO()
                                  # can not provide "pyfile_asfile" descriptor
#jsStream = open( '/dev/shm/one.json', 'rw' )
with tempfile.TemporaryFile() as jsStream:
    md.to_JSON_str( jsStream )
    jsStream.seek(0)
    jsContent = jsStream.read()

print( "Tempfile shouldn't exist here." )

jsContent = '{"runs":' + jsContent + '}'

#print( jsContent )
mData = json.loads( jsContent )
#print( mData )

# We have to explicitly delete chunk object here to prevent wrong destructors
# order. Sometimes, the file's destructor will be called before the chunk
# destructor.
del c

print("Done.")
