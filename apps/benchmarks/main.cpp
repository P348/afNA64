/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64_detector_ids.hpp"
# include "analysis/dsources/ddd.hpp"
# include "DaqEventsManager.h"

int
main( int argc, char * const argv[] ) {
    if( argc != 3 ) {
        fprintf( stderr, "Usage: %s <ddd-maps-dir-path> <.dat-file>\n",
            argv[0] );
        return EXIT_FAILURE;
    }

    CS::DaqEventsManager * extMngr = new CS::DaqEventsManager(
        "manager-0x1",
        false );
    extMngr->SetMapsDir( argv[1] );
    extMngr->AddDataSource( argv[2] );
    extMngr->ReadEvent();

    afNA64::dsources::ddd::aux::EventsTranscoder transcoder( *extMngr );
    transcoder.set_up_detector_mapping();

    test_detector_table( stdout );
    test_dsul( stdout );
    return EXIT_SUCCESS;
}

