/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <TCanvas.h>
# include <root/TStyle.h>
# include "evGen/parameters.h"
# include "app_aprimeEvGen.hpp"

namespace aprime {

Application::Application( Config * vm ) :
        AbstractApplication( vm ),
        Parent( vm ),
        RootParent( vm, "tstAprimeEvGen" ) {
}

std::vector<p348::po::options_description>
Application::_V_get_options( ) const {
    auto res = Parent::_V_get_options();
    p348::po::options_description generic("Generic options");

    p348::po::options_description wwCmpCfg("Weizsacker-Williams testing routine parameters");
    wwCmpCfg.add_options()
        ("aprimeEvGen.materialLabel",
            p348::po::value<std::string>(),
            "Human-readable text label for target material (usually, chemical el. notation).")
        ;
    res.push_back( wwCmpCfg );

    return res;
    # if 0
    po::options_description rootUIOpts("ROOT graphics options");
    rootUIOpts.add_options()
        ("TGraphics.width",
            po::value<uint16_t>()->default_value(uint16_t(1200)),
            "Default TCanvas width." )
        ("TGraphics.height",
            po::value<uint16_t>()->default_value(uint16_t(800)),
            "Default TCanvas height." )
        ("TGraphics.palette",
            po::value<uint16_t>()->default_value(uint16_t(800)),
            "gStyle->SetPalette(?)." )
        ;

    po::options_description opts;
    opts.add(generic)
        .add(rootUIOpts)
        .add(wwCmpCfg)
        .add(wwtfCfg)
        ;

    po::variables_map vm;

    po::store(po::command_line_parser(argc, argv).
        options(opts).
        run(), vm);

    std::string confFilePath = Parent::_dftConfigFileLocation;
    if( vm.count("config") ) {
        confFilePath = vm["config"].as<std::string>();
        std::cerr << "Using user's config file: " << confFilePath << std::endl;
    } else {
        std::cerr << "Using default config file: " << confFilePath << std::endl;
    }
    std::ifstream configFile( confFilePath, std::ifstream::in );
    if( ! configFile ) {
        std::cerr << "Couldn't open config file \"" << confFilePath
                  << "\". Aborting execution." << std::endl;
    }
    po::store( po::parse_config_file( configFile, opts, /*allow_unregistered*/ true ), vm );
    configFile.close();

    po::notify( vm );

    if(vm.count("help")) {
        std::cout << opts << std::endl;
        exit(1);
    }
    if(vm.count("printout-tasks")) {
        std::cout << "List of available tasks with names and descriptions:"
                  << std::endl;
        this->list_callbacks( std::cout );
        exit(1);
    }

    verbosity( vm["verbosity"].as<int>() );
    if( verbosity() > 3 ) {
        std::cerr << "Invalid verbosity level set: " << verbosity();
        verbosity(3);
        std::cerr << ". Set to " << verbosity() << "." << std::endl;
    }
    if( !(vm.count("batch-mode") && vm["batch-mode"].as<bool>()) ) {
        RootApplication::_configure_graphics(
                vm["TGraphics.width"].as<uint16_t>(),
                vm["TGraphics.height"].as<uint16_t>()
            );
        gStyle->SetPalette( vm["TGraphics.palette"].as<uint16_t>() );        
    } else {
        goo_log2( "Batch mode enabled -- no graphics will be produced." );
    }

    return vm;
    # endif
}

void
Application::_V_concrete_app_configure() {
    // TODO
}

# if 0
static int
_static__chi_test_task( const po::variables_map &, int, char *[]  ) {
    plot_chi_check();
    return EXIT_SUCCESS;
}

REGISTER_APRIME_EVGEN_TASK(
    "chi-check",
    "Dumps a few chi function calculated as depicted at \\cite{JDBjorken} fig. 10",
    _static__chi_test_task,
    nullptr )
# endif

} // namespace ecal


