/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <TRandom.h>
# include <TRandom3.h>

# include "app_aprimeEvGen.hpp"
# include "evGen/aprimeEvGen.hpp"
# include "p348g4_utils.hpp"

namespace aprime {

static int
_static__check_tabulation_discrepancy( const p348::po::variables_map & ) {
    const double E0 = goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.EBeam_GeV"),
                 AM = goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.massA_GeV"),
  discWarnThreshold = goo::app<aprime::Application>().cfg_option<double>("aprimeEvGen.tabTestThreshold")
                 ;
    std::set<double> energies = p348::aux::parse_aprime_tabulation_series(
                    goo::app<aprime::Application>().cfg_option<std::string>("aprimeEvGen.energyTabulationFactors"),
                    AM, E0 );
    //for( auto it = energies.begin(); it != energies.end(); ++it ) {
    //    std::cout << *it << std::endl;
    //}
    std::map<double, double> integralValues;
    TRandom * pseRnd = new TRandom3();
    for( auto it = energies.begin(); it != energies.end(); ++it ) {
        // For each incident energy, create a PDF and compute its integrals:
        p348::APrimeWWPDF pdf(
                # define obtain_physparameter_arg( type, txtName, name, descr ) \
                        goo::app<aprime::Application>().cfg_option<type>("aprimeEvGen." txtName),
                    for_all_p348lib_aprimeCS_parameters( obtain_physparameter_arg )
                # undef obtain_physparameter_arg
                # define obtain_numsparameter_arg( type, txtName, dft, name, descr ) \
                        goo::app<aprime::Application>().cfg_option<type>("extraPhysics.physicsAe.gslIntegration." txtName),
                    for_all_p348lib_aprimeCS_GSL_chi_comp_parameter( obtain_numsparameter_arg )
                # undef obtain_numsparameter_arg
                nullptr
            );
        pdf.EBeam_GeV( *it );
        char bf[128];
        snprintf( bf, sizeof(bf), "APrimeGenerator-test-%eGeV", *it );
        p348::APrimeGenerator G( bf, &pdf, 
                    # define obtain_tfoam_parameter( type, txtName, dft, name, descr ) \
                        goo::app<aprime::Application>().cfg_option<type>("physicsAe.TFoam." txtName),
                        for_all_p348_lib_TFoam_generator_parameters( obtain_tfoam_parameter )
                    # undef obtain_tfoam_parameter
                    NULL );
        G.initialize( pseRnd );
        integralValues[*it] = G.integrand().probability();
    }
    for( auto it  = integralValues.cbegin();
              it != integralValues.cend(); ++it ) {
        if( it != integralValues.cbegin() ) {
            auto pIt = it; pIt--;
            double disc = (it->second - pIt->second)/(it->second + pIt->second);
            if( disc > discWarnThreshold ) {
                p348g4_logw( "  \\\n" );
                p348g4_logw( "    Large discrepancy: %e > %e (factor %f vs. factor %f).\n",
                             disc, discWarnThreshold,
                             (pIt->first - AM)/(E0 - AM),
                             ( it->first - AM)/(E0 - AM) );
                p348g4_logw( "  /\n" );
            }
        }
        p348g4_log1( "  %e -- %e (%e mm^2)\n", it->first, it->second,
            p348::APrimeWWPDF::convert_cross_section_units_to_CLHEP_units(it->second) );
    }
    return EXIT_SUCCESS;
}

REGISTER_APRIME_EVGEN_TASK(
    "tab-check",
    "Produces output containing calculated series of cross-sections calculated "
    "according to given tabulation sequence." // TODO: arguments descr
    ,
    _static__check_tabulation_discrepancy,
    nullptr )

# if 0
static int
_static__compute_border_sigma( const p348::po::variables_map & vm ) {
    // TODO: XXX
    p348::APrimeWWPDF pdf(
            # define obtain_physparameter_arg( type, txtName, name, descr ) \
                    vm[ "aprimeEvGen." txtName ].as<type>(),
                for_all_p348lib_aprimeCS_parameters( obtain_physparameter_arg )
            # undef obtain_physparameter_arg
            # define obtain_numsparameter_arg( type, txtName, dft, name, descr ) \
                    vm[ "extraPhysics.physicsAe.gslIntegration." txtName ].as<type>(),
                for_all_p348lib_aprimeCS_GSL_chi_comp_parameter( obtain_numsparameter_arg )
            # undef obtain_numsparameter_arg
            nullptr
        );

    const size_t nPlots = 1000;
    const double scale = 2*M_PI / nPlots;
    //Double_t xArgs[] = { 0., M_PI };
    for( size_t n = 0; n < nPlots + 1; ++n ) {
        std::cout << pdf.density(1 - 1e-12, -M_PI + scale*n) << std::endl;
    }
    return EXIT_SUCCESS;
}

REGISTER_APRIME_EVGEN_TASK(
    "bounding-cs",
    "Compute and print out cross section sets at marginal points.",
    _static__compute_border_sigma,
    nullptr )
# endif

}  // namespace aprime

