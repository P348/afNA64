/*
 * Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "metadata/ddd/md_type.hpp"

# ifdef na64ee_SUPPORT_DDD_FORMAT

# include "analysis/dsources/ddd.hpp"

/* Notes:
 *  #1)
 * the is_complete() mechanics currently has no sense with chunks metadata. It
 * will be useful further.
 */

namespace afNA64 {
namespace md {
namespace ddd {

DDDMetadataType::DDDMetadataType(
                size_t nDuty,
                bool dbgLogging) : MDTraits::iMetadataType("DDD"),
                        _nDuty(nDuty),
                        _extractionLogFPtr(NULL),
                        _extractionLogBufferPtr(NULL),
                        _extractionLogBufferLength(0) {
    if( dbgLogging ) {
        _extractionLogFPtr = open_memstream(
                    &_extractionLogBufferPtr,
                    &_extractionLogBufferLength );
    }
}

DDDMetadataType::~DDDMetadataType() {
    if( _extractionLogFPtr ) {
        fclose( _extractionLogFPtr );
        free( _extractionLogBufferPtr );
    }
}

bool
DDDMetadataType::_V_is_complete( const na64ee::DDDIndex & ) const {
    return true;  // todo #1
}

bool
DDDMetadataType::_V_extract_metadata(
                const SourceID * /*sidPtr*/,
                iEventSource & s,
                Metadata *& mdRefPtr,
                std::list<iMetadataStore *> /*stores*/) const {
    if( !s.is_id_initialized() ) {
        emraise( badState, "Unable to extract metadata from source with "
            "uninitialized identifier --- chunk#&run# aren't set for "
            "DDD chunk instance %p.\n", &s );
    }
    na64ee::DDDIndex::RunDataLayout rdl;
    na64ee::DDDIndex::index_chunk(
                /* std::istream instance with data ... */ 
                        static_cast<dsources::ddd::Chunk &>(s).istream_ref(),
                /* where to fill indexes ............. */ rdl,
                /* duty parameter (can be 0) ......... */ _nDuty,
                /* guessed run number ................ */ s.id_ptr()->runNo,
                /* guessed chunk number .............. */ s.id_ptr()->chunkNo,
                /* logging stream C-handle ........... */ _extractionLogFPtr
            );
    if( mdRefPtr ) {
        mdRefPtr->merge( s.id_ptr()->runNo, rdl );
    } else {
        mdRefPtr = new na64ee::DDDIndex( s.id_ptr()->runNo, rdl );
    }
    return true;
}

DDDMetadataType::Metadata *
DDDMetadataType::_V_merge_metadata(
                const std::list<Metadata *> & mdats ) const {
    if( mdats.size() > 1 ) {
        _TODO_  // TODO
    } else if( mdats.empty() ) {
        return const_cast<Metadata *>(mdats.front());
    } else {
        emraise( badParameter, "No metadata instances provided for merging." );
    }
}

bool
DDDMetadataType::_V_append_metadata(
                        iEventSource & /*s*/,
                        Metadata & /*md*/ ) const {
    _TODO_  // TODO
}

void
DDDMetadataType::_V_cache_metadata(
                    const SourceID & sid,
                    const Metadata & mdInstance,
                    std::list<iMetadataStore *> & stores ) const {
    // This method has to implement saving of the metadata at specific
    // storaging instance(s). It may to operate with one particular store or
    // distribute metadata parts among available stores provided at third
    // argument. Metadata may be associated with source by its ID
    // with store's `put_metadata()` method.
    for( auto & storePtr : stores ) {
        storePtr->put_metadata( sid, mdInstance );
    }
}

void
DDDMetadataType::_V_get_subrange(
        const EventID & /*low*/, const EventID & /*up*/,
        const SourceID & /*sid*/,
        typename Traits::SubrangeMarkup & /*muRef*/ ) const {
    _TODO_  // TODO
}

}  // namespace ddd
}  // namespace md
}  // namespace afNA64

# endif   // na64ee_SUPPORT_DDD_FORMAT

