/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <stdlib.h>
# include <assert.h>
# include <string.h>
# include <stdio.h>

# include <StromaV/app/app.h>
# include "na64_detector_ids.h"

# include <goo_utility.h>

char *
snprintf_detector_name( char * buffer,
                        size_t bufferSize,
                        union AFR_UniqueDetectorID detID ) {
    const char detectorStringFmt[] = "%s",
               detectorStringFmtSbdvd[] = "%s-%dx%d"
               ;
    enum MajorDetectorsCode mjCode = (enum MajorDetectorsCode) detID.byNumber.major;
    if( detector_is_SADC( mjCode ) && detector_is_Subd( mjCode ) ) {
        snprintf( buffer, bufferSize,
                  detectorStringFmtSbdvd,
                  detector_name_by_code( mjCode ),
                  (int) detector_get_x_idx(detID),
                  (int) detector_get_y_idx(detID) );
    } else {
        snprintf( buffer, bufferSize,
                  detectorStringFmt,
                  detector_name_by_code( mjCode ) );
    }
    return buffer;
}


/** Family identifiers are listed in \ref for_all_detector_families
 * x-macro table.*/
AFR_DetFamID
detector_family_num( AFR_DetMjNo major ) {
    return (major >> P38G4_MJ_DETID_OFFSET) & P348G4_DETID_FAMILY_MASK;
}

/** Family identifiers with its names are listed in \ref
 * for_all_detector_families x-macro table.
 * Function uses static `const char **` array.*/
const char *
detector_family_name( AFR_DetFamID fID ) {
    switch( fID ) {
    # define EX_case_dict_entry( name, code, verbName ) case code : { return verbName; }
    for_all_detector_families( EX_case_dict_entry )
    # undef EX_case_dict_entry
    default:
        return "misc";
    };
}

/**@param major a major detector ID number converted to integral value
 * (\ref MajorDetectorsCode).
 * @param xIdx an `x` index of detector cell (if available)
 * @param yIdx an `y` index of detector cell (if available)
 * @return combined unique detector identification number.
 */
AFR_DetSignature
compose_cell_identifier( AFR_DetMjNo major, uint8_t xIdx, uint8_t yIdx ) {
    union AFR_UniqueDetectorID res = {{major, 0}};
    res.byNumber.major = major;
    if( detector_is_Subd((enum MajorDetectorsCode) major) ) {
        res.byNumber.minor = xIdx | (((uint16_t) yIdx) << 8);
    }
    return res.wholenum;
}

uint8_t
detector_get_x_idx( union AFR_UniqueDetectorID id ) {
    return id.byNumber.minor & 0xff;
}

uint8_t
detector_get_y_idx( union AFR_UniqueDetectorID id ) {
    return ((id.byNumber.minor & 0xff00) >> 8 );
}

uint8_t
detector_is_SADC( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_SADC);
}

uint8_t
detector_is_APV( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_APV);
}

uint8_t
detector_is_Subd( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    if( detector_is_SADC(mj) )
        return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_Subd );
    sV_logw( "Checking non SADC-detector for transversal segmentation.\n" );
    return 0;
}

uint8_t
detector_is_Joint( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    if( detector_is_APV(mj) )
        return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_Joint );
    sV_logw( "Checking non APV-detector for joint wires.\n" );
    return 0;
}

uint8_t
detector_is_XYSep( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_XYSep );
}

uint8_t
detector_is_X( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    if( detector_is_XYSep(mj) )
        return 0 != ((mj >> P38G4_MJ_DETID_OFFSET) & f_X );
    sV_logw( "Checking non X/Y-separated planes of detector for X feature.\n" );
    return 0;
}

uint8_t
detector_is_Y( DETID_SCOPE_QUALIFIER MajorDetectorsCode mj ) {
    if( detector_is_XYSep(mj) )
        return 0 == ((mj >> P38G4_MJ_DETID_OFFSET) & f_X );
    sV_logw( "Checking non X/Y-separated planes of detector for Y feature.\n" );
    return 0;
}

uint8_t
detector_subgroup_num( AFR_DetMjNo major ) {
    return major & 0xf; /* TODO!!! */
}

AFR_DetSignature
detector_complementary_X_id( AFR_DetSignature uIDs ) {
    union AFR_UniqueDetectorID uID;
    uID.wholenum = uIDs;
    if( ! detector_is_XYSep( uID.byNumber.major ) ) {
        sV_logw( "Requesting comlementary X/Y pair for detector 0x%x without X/Y separation.\n",
                      uID.wholenum );
        return uID.wholenum;
    }
    if( detector_is_X( uID.byNumber.major ) ) {
        return uID.wholenum;
    }
    union AFR_UniqueDetectorID repID = uID;
    repID.byNumber.major |= (f_X << P38G4_MJ_DETID_OFFSET);
    return repID.wholenum;
}

AFR_DetSignature
detector_complementary_Y_id( AFR_DetSignature uIDs ) {
    union AFR_UniqueDetectorID uID;
    uID.wholenum = uIDs;
    if( ! detector_is_XYSep( uID.byNumber.major ) ) {
        sV_logw( "Requesting comlementary X/Y pair for detector 0x%x without X/Y separation.\n",
                      uID.wholenum );
        return uID.wholenum;
    }
    if( detector_is_Y( uID.byNumber.major ) ) {
        return uID.wholenum;
    }
    union AFR_UniqueDetectorID repID = uID;
    repID.byNumber.major &= ~(f_X << P38G4_MJ_DETID_OFFSET);
    return repID.wholenum;
}

# if 0
AFR_DetMjNo
detector_major_by_name( const char * tName ) {
    # define return_major_number_if_matches( name, code, dddCode )  \
    if( !strcmp( # name, tName ) ){ return code; } else
    for_all_detectors( return_major_number_if_matches )
    # undef return_major_number_if_matches
    {
        return 0;  /* unknown */
    }
}
# endif

AFR_DetFamID
AFR_family_id_by_name( const char * familyName ) {
    // todo: optimize me. Despite this function is not crucial for performance
    // right now, it is quite possible to become a bottleneck in a future.
    # define return_if_name_matches( fmName, fmCode, fmDescription ) \
    if( !strcmp( STRINGIFY_MACRO_ARG(fmName), familyName ) ) { return fmCode; } else
    for_all_detector_families( return_if_name_matches )
    # undef return_if_name_matches
    { return 0x0; }
}

/*
 * Table testing function
 */

static int
test_codes_for_uniqueness( uint8_t cCode, const char * cName ) {
    int
    # define define_static_code_constant( name, code, dddCode ) k_ ## name = code,
    for_all_detectors(define_static_code_constant)
    # undef define_static_code_constant
    dummy = 0;

    (void)(dummy);

    # define check_code( name, code, dddCode ) \
    if( strcmp( # name, cName ) && k_ ## name == cCode ) { return -1; } else
    for_all_detectors( check_code )
    # undef check_code
    {
        return 0;
    }
}

int
test_detector_table( FILE * fle ) {
    UByte i, j, k;
    union AFR_UniqueDetectorID id;
    if( 4 != sizeof(union AFR_UniqueDetectorID) ) {
        return -1;
    }

    /* Test ECAL enumeration */
    for(i = 0; i < 6; ++i) {
        for(j = 0; j < 6; ++j) {
            for(k = 0; k < 3; ++k ) {
                if( !k ) {
                    id.wholenum = compose_cell_identifier( d_ECAL0,   i, j );
                } else if( k == 1 ) {
                    id.wholenum = compose_cell_identifier( d_ECAL1,   i, j );
                } else if( k == 2 ) {
                    id.wholenum = compose_cell_identifier( d_ECALSUM, i, j );
                } else {
                    /* Will never get here. */
                    abort();
                }
                if( detector_get_x_idx(id) != i ) {
                    return -2;
                }
                if( detector_get_y_idx(id) != j ) {
                    return -3;
                }
                assert( detector_is_Subd(id.byNumber.major) );
                assert( detector_is_SADC(id.byNumber.major) );
                //assert( 0x4 == detector_family_num(id.byNumber.major) );
            }
        }
    }

    {  /* Dump detector table to stdout: */
        FILE * os = fle;
        fprintf(os, "Features:\n\ts - detector has transversal (x,y) segmentation;\n"
                    "\tW - detector is served by SADC chip;\n"
                    "\tA - detector is served by APV chip.\n"
                    "\tj - detector is served by APV chip and has joint wires.\n"
                    "\tX - detector is separated X/Y planes and represents X plane.\n"
                    "\tY - detector is separated X/Y planes and represents Y plane.\n"
                    "--------+------+----+---+----------------+--------\n"
                    " name   | code | DDD|Fam.code|   Fam.name|Features\n"
                    "--------+------+----+---+----------------+--------\n");
        /* TODO: move this stuff to string-generating function. */
        # define print_out_detector_entry( name, code, dddCode ) \
        fprintf( os, "%8s|0x%04x|%4d|0x%1x|%16s|%c %c %c %c",        \
            # name, \
            (int) code, \
            dddCode, \
            detector_family_num(code), \
            detector_family_name(detector_family_num(code)), \
            ( detector_is_SADC(code) ? 'W' : '-' ), \
            ( detector_is_APV(code) ? 'A' : '-' ), \
            ( detector_is_SADC(code) ? (( detector_is_Subd(code)  ? 's' : '-' )) :  \
                                        ( detector_is_Joint(code) ? 'j' : '-' )),   \
            ( detector_is_XYSep(code) ? (detector_is_X(code) ? 'X' : 'Y') : '-' )   \
            ); \
        if( detector_is_XYSep(code) ) {                                             \
            if( detector_is_Y( code ) ) { fprintf(os, " Y-complem. to 0x%x",        \
                detector_complementary_X_id( code ) ); }                            \
        } fprintf( os, "\n" );
        for_all_detectors( print_out_detector_entry );
        # undef print_out_detector_entry
    }
    {
        # define check_det_code( name, code, dddCode ) \
            if( test_codes_for_uniqueness( (uint8_t) (code), # name ) ) { return -6; }
        for_all_detectors(check_det_code)
        # undef check_det_code
    }
    return 0;
}

# ifdef DSuL
/*
 * Detectors selector micro-language (DSuL)
 */

int
test_dsul( FILE * fle ) {
    struct sV_DSuL_Expression ** exprPtr;
    struct sV_DSuL_Expression * exprs[] = {
            DSuL_compile_selector_expression( "ECAL0" ),
            DSuL_compile_selector_expression( "HCAL0" ),
            DSuL_compile_selector_expression( "ECAL{\"SUM\"}" ),
            DSuL_compile_selector_expression( "HCAL{>=1}" ),
            NULL
        };
    for( exprPtr = exprs; *exprPtr; ++exprPtr ) {
        dump_selector_expression( stdout, *exprPtr );
    }
    return 0;
}

static void
dump_mvar_index( FILE * f, const struct sV_DSuL_MVarIdxRangeLst * rangeList ) {
    /* TODO */
}

void
dump_selector_expression( FILE * f, const struct sV_DSuL_Expression * sExpr) {
    const struct sV_DSuL_Expression * cExpr;
    for( cExpr = sExpr;
         cExpr ; cExpr = cExpr->next ) {
        {   /* Selector level */
            const struct sV_DSuL_Selector * selector = &(cExpr->left);
            const char * detName = detector_name_by_code(selector->mjSelector.majorNumber);
            if( afNA64_global_unknownDetectorString == detName ) {
                /* Guess that is a family */
                const struct sV_DSuL_MjSelector * mjSelector = &(selector->mjSelector);
                fprintf( f, "%s {", detector_family_name(mjSelector->majorNumber) );
                dump_mvar_index(f, mjSelector->range);
                fprintf( f, "}" );
            } else {
                /* Guess that is a detector */
                fprintf( f, "%s", detName );
            }
            if( selector->range ) {
                /* Minor indexes range */
                fprintf( f, "[" );
                dump_mvar_index(f, selector->range);
                fprintf( f, "]" );
            }
        }
        if( cExpr->next ) {
            if( DSuL_Operators_and == cExpr->binop ) {
                fprintf( f, "+" );
            } else if( DSuL_Operators_andNot == cExpr->binop ) {
                fprintf( f, "!" );
            } else {
                /* unknown binop! Has never be happen */
                fprintf( f, "?" );
            }
        }
    }
    fprintf( f, "\n" );
}

# endif  /* DSuL */

