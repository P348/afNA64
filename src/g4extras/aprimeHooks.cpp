/*
 * Copyright (c) 2017 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "g4extras/aprimeHooks.hpp"
# include "app/app.h"

/**@file aprimeHooks.cpp
 * @brief Helpers keeping track of A' particle lifecycle.
 * 
 * This file conditionally provides run-time hooks for generated A' events.
 * Utilizes StromaV TrackingAction helper in order to trigger statistics
 * filling.
 * */

# if defined(AFNA64_DPhMC_FOUND) && defined(StromaV_GEANT4_MC_MODEL) && defined( StromaV_GEANT4_DYNAMIC_PHYSICS )

# include <goo_exception.hpp>

namespace na64 {
namespace aux {

bool
APrimeCreateSelector::_V_matches( const G4Track * trackPtr ) const {
    if( !_APrimeProductionProcess ) {
        _APrimeProductionProcess = DPhMC::APrimePhysics::process();
        if( !_APrimeProductionProcess ) {
            emraise( badArchitect, "A' physics was not constructed." );
        }
    }
    return trackPtr->GetCreatorProcess() == _APrimeProductionProcess;
}

APrimeCreateSelector::APrimeCreateSelector() :
            sV::ParticleStatisticsDictionary::iTrackSelector("A' creator"),
                    _APrimeProductionProcess(nullptr) {
    if( !_APrimeProductionProcess ) {
        _APrimeProductionProcess = DPhMC::APrimePhysics::process();
        sV_logw( "A' dynamic physics was not constructed before selector "
                 "construction.\n" );
    }
    # if 0
    // *sigh* Why the hell can't you just return const vector instance?!
    const G4ProcessVector & pv = *(G4ProcessManager::GetProcessList());
    for( unsigned int i = 0; i < pv.size(); ++i ) {
        if( DPhMC_APRIME_MIXING_PROCESS_NAME == pv[i]->GetProcessName() ) {
            _APrimeProductionProcess = pv[i];
            break;
        }
    }
    if( !_APrimeProductionProcess ) {
        emraise( badState, "Failed to look-up for A' E/M production "
            "process in Geant4 process list after searching in %d items. "
            "One has to enable A' E/M production process to use its "
            "create/destroy selector.",
            pv.size() );
    }
    # endif
}

void
APrimeKill::_V_fill_track( const sV::ParticleStatisticsDictionary::iTrackSelector & /*selector*/,
                    const G4Track * track ) {
    G4ThreeVector md = track->GetMomentumDirection();
    G4ThreeVector vertexPosition = track->GetPosition();

    // FIXME: we need current point, NOT A VERTEX!!!
    _ltp.kineticEnergy = track->GetKineticEnergy();
    _ltp.totalEnergy = track->GetTotalEnergy();

    _ltp.momentumX = md[0];
    _ltp.momentumY = md[1];
    _ltp.momentumZ = md[2];

    _ltp.lifeDistance = track->GetTrackLength();

    // World_PV, ScLayer_PV, WLayer_PV

    {   // Figure out whether particle passed outside the detector volume:
        //  Hereafter we call current volume the volume where the step has just
        //  gone through. Geometrical informations are available from
        //  preStepPoint. G4VTouchable and its derivates keep these geometrical
        //  information:
        G4VPhysicalVolume * thisV = track->GetVolume(),
                          * nextV = track->GetNextVolume()
                          ;
        const G4ThreeVector & presp = track->GetStep()->GetPreStepPoint()->GetPosition(),
                            & postsp = track->GetStep()->GetPostStepPoint()->GetPosition();

        _ltp.posX = postsp[0];
        _ltp.posY = postsp[1];
        _ltp.posZ = postsp[2];

        std::cout << "  XXX this physical volume name: "
                  << thisV->GetName() << std::endl
                  << "  XXX track status: "
                  << (int) track->GetTrackStatus() << std::endl
                  ;
        if( nextV ) {
            // If next volume does exist, it probably means that current volume
            // is NOT world that corresponds to case when decay happened inside
            // WCAL.
            std::cout << "  XXX next physical volume name: "
                      << nextV->GetName() << std::endl
                      ;
        }
        if( thisV->GetName() == "World_PV" ) {
            _ltp.outsideWCAL = true;
            std::cout << "  XXX considering particle decay OUTSIDE WCAL with ";
        } else {
            _ltp.outsideWCAL = false;
            std::cout << "  XXX considering particle decay IN WCAL with ";
        }
        std::cout << "Z="
                      << "(pre=" << presp[2] << ", post=" << postsp[2] << "), "
                      << "tl=" << track->GetTrackLength()
                      << std::endl
                      ;
    }

    // GetPosition()
    // GetTrackStatus() ?
    // Get[Current]Step()->GetPostStepPoint()->GetPosition()

    _aprimeInfo->Fill();
}

APrimeKill::APrimeKill() : _aprimeInfo(nullptr) {
    _aprimeInfo = new TTree( "aprimeDecayInfo",
                             "Info about A' production reaction");
    _aprimeInfo->Branch( "killVertex", &_ltp,
        "posX/D:posY/D:posZ/D"
        ":momentumX/D:momentumY/D:momentumZ/D"
        ":totalEnergy/D:kineticEnergy/D"
        ":lifeDistance/D"
        ":outsideWCAL/O" );
}

}  // namespace aux
}  // namespace na64

static sV::ParticleStatisticsDictionary::iTrackSelector *
_static_new_APrime_TA_selector() {
    return new na64::aux::APrimeCreateSelector();
}

__attribute__((constructor(156)))
static void _static_add_APrime_track_selector() {
    sV::ParticleStatisticsDictionary::self().add_selector_ctr(
            "A-prime", _static_new_APrime_TA_selector );
}

static sV::ParticleStatisticsDictionary::iTrackingStatistics *
_static_new_APrime_TA_kill_statistics() {
    return new na64::aux::APrimeKill();
}

__attribute__((constructor(156)))
static void _static_add_ParticleBornKinematics_stats() {
    sV::ParticleStatisticsDictionary::self().add_statistics_ctr(
            "killKinematics", _static_new_APrime_TA_kill_statistics );
}

# endif  // defined(AFNA64_DPhMC_FOUND) && defined(StromaV_GEANT4_MC_MODEL) && defined( StromaV_GEANT4_DYNAMIC_PHYSICS )

