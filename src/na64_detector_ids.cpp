/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "na64_detector_ids.hpp"

# include <StromaV/app/abstract.hpp>
# include <goo_exception.hpp>

# include <iomanip>
# include <sstream>
# include <cassert>

namespace na64 {
namespace aux {

DetectorMapping * DetectorMapping::_self = nullptr;

void
DetectorMapping::_assert_id_collision( bool ok, uint8_t n, const char * detName ) {
    const char stages[][32] = {
            "name -> DDD-code"  ,
            "DDD-code -> major" ,
            "name -> major"     ,
            "major -> name"
        };
    if( !ok ) {
        emraise( nonUniq, "Collision in dictionary #%d (%s), for detector %s.",
                 (int) n, stages[n], detName );
    }
}

DetectorMapping::DetectorMapping() :
            sV::aux::iDetectorIndex( this ),
            _correctionCnt_kept(0),
            _correctionCnt_simple(0),
            _correctionCnt_swapped(0),
            _correctionCnt_unknown(0) {
    // Fill static info:
    # define fill_entry( name, customCode, defaultDDDCode ) {                           \
        auto ir1 = _nameToDDDCode.emplace( # name, defaultDDDCode );                    \
        _assert_id_collision( ir1.second, 0, # name );                                  \
        auto ir2 = _dddCodeToMajor.emplace( defaultDDDCode, EnumScope::d_ ## name );    \
        _assert_id_collision( ir2.second, 1, # name );                                  \
        auto ir3 = _nameToMajor.emplace( # name, EnumScope::d_ ## name );               \
        _assert_id_collision( ir3.second, 2, # name );                                  \
        auto ir4 = _majorToName.emplace( EnumScope::d_ ## name, # name );               \
        _assert_id_collision( ir4.second, 3, # name );                                  \
    }
    for_all_detectors( fill_entry )
    # undef fill_entry

    // todo: ctr-macro?
    register_apv_mapper( "MM01X",    na64_APV_strip_mapper__micromegas_joints );
    register_apv_mapper( "MM01Y",    na64_APV_strip_mapper__micromegas_joints );
    register_apv_mapper( "MM02X",    na64_APV_strip_mapper__micromegas_joints );
    register_apv_mapper( "MM02Y",    na64_APV_strip_mapper__micromegas_joints );
    register_apv_mapper( "MM03X",    na64_APV_strip_mapper__micromegas_joints );
    register_apv_mapper( "MM03Y",    na64_APV_strip_mapper__micromegas_joints );
    register_apv_mapper( "MM04X",    na64_APV_strip_mapper__micromegas_joints );
    register_apv_mapper( "MM04Y",    na64_APV_strip_mapper__micromegas_joints );
    register_apv_mapper( "GM01X1__", na64_APV_strip_mapper__GEM_demultiplexing_scheme );
    register_apv_mapper( "GM01Y1__", na64_APV_strip_mapper__GEM_demultiplexing_scheme );
    register_apv_mapper( "GM02X1__", na64_APV_strip_mapper__GEM_demultiplexing_scheme );
    register_apv_mapper( "GM02Y1__", na64_APV_strip_mapper__GEM_demultiplexing_scheme );

    _fill_indexing_lexic();
}

void
DetectorMapping::_fill_indexing_lexic() {
    # define STRIDX_FILL( str, nm, ... ) { \
        AFR_DetFamID families[] = { __VA_ARGS__ }; \
        std::unordered_set<AFR_DetFamID> famsSet( families, families + sizeof(families)/16 ); \
        DetectorMjIndexLexicalEntry entry = { nm, famsSet }; \
        _indexingLexic.emplace( str, entry ); \
    }
    # define FAMILY_NAME(nm) EnumScope::DetectorFamilies::fam_ ## nm
    for_all_major_indexes( STRIDX_FILL, FAMILY_NAME )
    # undef STRIDX_FILL
}

void
DetectorMapping::register_apv_mapper( const std::string & detName, APVMapper mapperPtr ) {
    auto it = _nameToMajor.find( detName );
    if( _nameToMajor.end() == it ) {
        sV_loge( "Couldn't find registered APV-managed detector with name \"%s\".",
                     detName.c_str() );
    }
    auto insertionResult = _apvStripMappers.emplace( it->second, mapperPtr );
    if( !insertionResult.second ) {
        if( mapperPtr == insertionResult.first->second ) {
            sV_logw( "Ignoring repeatative insertion of APV wire-to-strip mapper for detector "
                     "\"%s\" (0x%x).",
                     detName.c_str(), it->second );
        }
    }
    auto bIt = _apvBoundaries.find( mapperPtr );
    if( _apvBoundaries.end() == bIt ) {
        size_t nWire = 0,
                       nJoints;
        const APVStripNo * stripsPtr;
        float stripsArr[2];

        APVBoundaries & xform = _apvBoundaries[mapperPtr];

        while( NULL != (stripsPtr = mapperPtr( nWire, &nJoints )) ) {
            for( size_t n = 0; n < nJoints; ++n ) {
                stripsArr[0] = stripsArr[1] = stripsPtr[n];
                xform.update_ranges( stripsArr );
            }
            ++nWire;
        }
        try {
            xform.recalculate_scales();
        } catch( goo::Exception & e ) {
            if( goo::Exception::badValue == e.code() ) {
                sV_loge( "While registering mapper for detector %s.\n", detName.c_str() );
            }
            throw;
        }
    }
}

const DetectorMapping::APVBoundaries &
DetectorMapping::apv_boundaries( EnumScope::MajorDetectorsCode mj ) const {
    auto mapperIt = _apvStripMappers.find( mj );
    if( _apvStripMappers.end() == mapperIt ) {
        emraise( badParameter, "Has no APV mapper for detector 0x%x.", mj );
    }
    auto xformIt = _apvBoundaries.find( mapperIt->second );
    if( _apvBoundaries.end() == xformIt ) {
        emraise( badArchitect, "Has no XForm for APV detector 0x%x.", mj );
    }
    return xformIt->second;
}

char
DetectorMapping::correct_entry( const char * nameStr, unsigned newDDDCode ) {
    // This routine corrects data in mapping tables:
    //  1. Name to DDD-code (just setting to new value);
    //  2. DDD-code to major (by re-inserting entry)
    auto nameToDDDIt = _nameToDDDCode.find( nameStr );
    if( _nameToDDDCode.end() != nameToDDDIt ) {
        DECLTYPE(_nameToMajor)::const_iterator nameToMajorIt =
                                                _nameToMajor.find( nameStr );
        if( _nameToMajor.end() == nameToMajorIt ) {
            emraise( badState, "Couldn't find <name-to-major> index for " \
                               "detector \"%s\" (DDD code 0x%x), " \
                               "while name is known to <name-to-DDD Code> " \
                               "set.", nameStr, newDDDCode );
        }
        // from here, name-to-DDD code is corrected, now need to correct DDD code-to-major:
        const AFR_DetMjNo major = nameToMajorIt->second;

        if( newDDDCode == nameToDDDIt->second ) {
            // Ok -- mapping not changed.
            ++_correctionCnt_kept;
            return CORRECTION_KEPT;
        }
        // Previous DDD-code, found by name:
        const unsigned oldDDDCode = nameToDDDIt->second;
        nameToDDDIt->second = newDDDCode;
        {  // correct native code -> major cache map
            CorrectionResult correctionResult;
            auto itPrevDDDToMajor = _dddCodeToMajor.find( oldDDDCode ),  // must be found
                 itToSwap = _dddCodeToMajor.find( newDDDCode )           // can be absent
                 ;
            assert( oldDDDCode != newDDDCode );
            assert( _dddCodeToMajor.end() != itPrevDDDToMajor );
            if( _dddCodeToMajor.end() == itToSwap ) {
                // new code does not correspond to any existing entry.
                _dddCodeToMajor.erase( itPrevDDDToMajor );
                _dddCodeToMajor.emplace( newDDDCode, major );
                ++_correctionCnt_simple;
                correctionResult = CORRECTION_SIMPLE;
                sV_log2( "Corrected native code of \"%s\" from 0x%x to 0x%x (major code is 0x%x).\n",
                     nameStr,
                     oldDDDCode,
                     newDDDCode, major );
            } else {
                // new code already correspond to existing entry. Swap the values:
                sV_log2( "Correcting native code of \"%s\" from 0x%x to 0x%x (major code is 0x%x) "
                    ": swapped with \"%s\", which native code is now 0x%x.\n",
                     nameStr,
                     oldDDDCode,
                     newDDDCode,
                     major, name_by_major( (EnumScope::MajorDetectorsCode) itToSwap->second ),
                     oldDDDCode );
                //std::swap( itPrevDDDToMajor, itToSwap2 );
                itPrevDDDToMajor->second = itToSwap->second;  // save this val just in case
                itToSwap->second = major;                     // new DDD code now refers to correct mj
                {  // swap it in name-to-ddd indexing;
                    auto mjToNameIt = _majorToName.find( itPrevDDDToMajor->second );
                    if( _majorToName.end() == mjToNameIt ) {
                        emraise( badState, "Detector table malformed (can not interpret error)." );
                    }
                    auto toSwapByNameIt = _nameToDDDCode.find( mjToNameIt->second );
                    if( _nameToDDDCode.end() == toSwapByNameIt ) {
                        emraise( badState, "Detector table malformed: can not swap \"%s\" entry with \"%s\": "
                                 "name \"%s\" is unknown!",
                                 nameStr,
                                 mjToNameIt->second.c_str(),
                                 mjToNameIt->second.c_str() );
                    }
                    toSwapByNameIt->second = oldDDDCode;
                }

                ++_correctionCnt_swapped;
                correctionResult = CORRECTION_SWAPPED;
            }
            return correctionResult;
        }
    } else {
        if( _unmapped.end() == _unmapped.find( newDDDCode ) ) {
            sV_loge( "Has no predefined mapping for \"%s\" detector with mapping ID 0x%x.\n",
                nameStr, newDDDCode );
            _unmapped.emplace( newDDDCode, nameStr );
        }
        ++_correctionCnt_unknown;
        return CORRECTION_NO_PREDEFINED;
    }
}

EnumScope::MajorDetectorsCode
DetectorMapping::major_by_name( const char * nameStr, bool warn ) const {
    auto it = _nameToMajor.find( nameStr );
    if( it == _nameToMajor.end() ) {
        if( warn ) {
            sV_loge( "Detector name \"%s\" is unforseen.\n", nameStr );
        }
        return EnumScope::d_unknown;
    }
    return (EnumScope::MajorDetectorsCode) it->second;
}

extern "C" const char afNA64_global_unknownDetectorString[16] = "unknown";

const char *
DetectorMapping::name_by_major( EnumScope::MajorDetectorsCode c ) const {
    auto it = _majorToName.find( c );
    if( _majorToName.end() == it ) {
        return afNA64_global_unknownDetectorString;
    }
    return it->second.c_str();
}

EnumScope::MajorDetectorsCode
DetectorMapping::major( EnumScope::DDD_DetectorsCode dddc ) const {
    auto it = _dddCodeToMajor.find( dddc );
    if( _dddCodeToMajor.end() == it ) {
        if( goo::app<sV::AbstractApplication>().verbosity() > 2 ) {
            auto unmappedIt = _unmapped.find( dddc );
            sV_log3( "Doubtful warning: couldn't resolve native code 0x%x "
                     "(%s). Please, consider adding it to the detector ids "
                     "declarations.\n",
                dddc, ( _unmapped.end() == unmappedIt ? "<unknown>" : unmappedIt->second.c_str() ) );
            sV_log3( "...to do it, append the for_all_[SADC/APV]_detectors() "
                    "X-Macros with new line entry.\n" );
        }
        return EnumScope::d_unknown;
    }
    return (EnumScope::MajorDetectorsCode) it->second;
}

void
DetectorMapping::dump_table( std::ostream & os ) {
    //bool errorsFound = false;
    std::map<DECLTYPE(_nameToMajor)::key_type,
             DECLTYPE(_nameToMajor)::mapped_type> sorted(_nameToMajor.begin(), _nameToMajor.end());
    os << std::right << std::setw(15) << "Det. name" << " | "
       << std::left  << std::setw(5) << "Mj. #" << " | "
       << std::left  << std::setw(5) << "Native ID"
       << std::endl
       << "----------------+-------+---------- ----------------"
       << std::endl
       ;
    for( auto it = sorted.begin(); it != sorted.end(); ++it ) {
        unsigned natCode = _nameToDDDCode[it->first];
        os << std::right << std::setw(15) << it->first << " | "
           << std::left  << std::setw(5) << std::hex << (int) it->second << " | "
           << std::left  << std::setw(5) << std::hex << (int) natCode
           ;
        //# ifndef NDEBUG
        auto checkIt = _dddCodeToMajor.find( natCode );
        if( _dddCodeToMajor.end() == checkIt ) {
            os << " (unknown nat.code->major)";
            //errorsFound = true;
        } else if( checkIt->second != it->second ) {
            os << " (not in mapping, " << std::hex << (int) checkIt->second << ")" ;
            //errorsFound = true;
        } else {
            os << " (ok)";
        }
        //# endif
        os << std::endl;
    }
    os << "unmapped -------+-------+---------- ----------------" << std::endl;
    for( auto it = _unmapped.begin(); it != _unmapped.end(); ++it ) {
        os << std::right << std::setw(15) << it->second << " | "
           << std::left  << std::setw(5)  << "?" << " | "
           << std::left  << std::setw(5) << std::hex << (int) it->first
           << std::endl;
    }
    os << "----------------+-------+---------- ----------------"
       << std::endl
       ;
    //if( errorsFound ) {
        //emraise( badState, "Errors found in corrected table." );
    //    sV_loge();
    //}
}

APVMapper
DetectorMapping::apv_strip_mapper_for( AFR_DetSignature sig ) const {
    auto it = _apvStripMappers.find(sig);
    if( _apvStripMappers.end() == it ) {
        emraise( notFound, "Couldn't resolve APV strip mapping function for 0x%x detector.", sig );
    }
    return it->second;
}

}  // namespace aux
}  // namespace na64

char
resolve_major_detector_number_mapping( const char * nameStr, unsigned dddCode ) {
    return na64::aux::DetectorMapping::self().correct_entry( nameStr, dddCode );
}

AFR_DetMjNo
detector_major_by_name( const char * nameStr ) {
    return na64::aux::DetectorMapping::self().major_by_name( nameStr );
}

EnumScope::MajorDetectorsCode
det_code_ddd_to_na64( EnumScope::DDD_DetectorsCode dddCodeVal ) {
    return na64::aux::DetectorMapping::self().major( dddCodeVal );
}

const char *
detector_name_by_code( EnumScope::MajorDetectorsCode tCode ) {
    return na64::aux::DetectorMapping::self().name_by_major(tCode);
}

const char *
ddd_detector_name_by_code( EnumScope::DDD_DetectorsCode tCode ) {
    return na64::aux::DetectorMapping::self().name_by_major(
            na64::aux::DetectorMapping::self().major( tCode )
        );
}

void
dump_mapping_table( FILE * fle ) {
    std::ostringstream oss;
    na64::aux::DetectorMapping::self().dump_table( oss );
    fputs( oss.str().c_str(), fle );
}

APVMapper
apv_get_mapper_for( union AFR_UniqueDetectorID did ) {
    return na64::aux::DetectorMapping::self().apv_strip_mapper_for( did.wholenum );
}

// Overrides weak aliases from StromaV
/////////////////////////////////////

AFR_DetMjNo
AFR_detector_major_by_name( const char * name ) {
    return na64::aux::DetectorMapping::self().major_by_name( name, false );
}

# ifdef DSuL

AFR_DetMjNo
AFR_compose_detector_major( AFR_DetFamID fmCode,
                            const struct sV_DSuL_MVarIndex * mjIdxsPtr ) {
    AFR_DetMjNo major = (fmCode << P38G4_MJ_DETID_OFFSET);
    if( !mjIdxsPtr ) {
        // Special case --- only family ID has to be returned.
        return major;
    }
    if( mjIdxsPtr->nDim > 2 ) {
        emraise( parserFailure, "NA64 experiment has no major indexes of "
            "dimension grater than two (%d provided).", (int) mjIdxsPtr->nDim );
    } else if( 1 == mjIdxsPtr->nDim ) {
        //uint8_t idx = 255;
        // String index ("SUM", "1X1__", etc). For special cases, one may
        // prepend the code below with `if( ... == dmCode ) { ... } else'
        // clause.
        //_indexingLexic.find(  );
        //{ emraise( notFound, "Can not parse major index %s.",
        //           mjIdxsPtr->components.strID ) }
        _TODO_  // TODO
    } else if( 2 == mjIdxsPtr->nDim ) {
        _TODO_  // TODO
    }
    return major;
}

void
AFR_decode_minor_to_indexes(    AFR_DetSignature detSignature,
                                struct sV_DSuL_MVarIndex * mvIdxPtr ) {
    //uint16_t nDim;
    //union {
    //    uint16_t x[7];
    //    char * strID;
    //} components;
    AFR_UniqueDetectorID s(detSignature);
    //s.wholenum = detSignature;
    switch( detector_family_num(s.byNumber.major) ) {
        case EnumScope::fam_ECAL :
        case EnumScope::fam_HCAL :
        case EnumScope::fam_WCAL :
        case EnumScope::fam_MUON : {
            mvIdxPtr->nDim = 1;
            mvIdxPtr->components.x[0] = 0x3 & s.byNumber.major;
        } break;
        case EnumScope::fam_HOD : {
            _TODO_  // TODO
        } break;
        case EnumScope::fam_MuMega : {
            _TODO_  // TODO
        } break;
        case EnumScope::fam_GEM : {
            _TODO_  // TODO
        } break;
        default:
        case EnumScope::fam_MISC : {
            _TODO_  // TODO
        } break;
    };
}

# endif

# if 0
enum MajorDetectorsCode
det_code_ddd_to_p348( enum DDD_DetectorsCode dddCodeVal ) {
    # define return_if_code_matches( name, code, dddCode ) \
    if( (dddCode) == dddCodeVal ) { return code; } else
    for_all_detectors( return_if_code_matches )
    # undef return_if_code_matches
    {
        return d_unknown;
    }
}
# endif

# if 0
AFR_DetMjNo
detector_major_by_name( const char * tName ) {
    # define return_major_number_if_matches( name, code, dddCode )  \
    if( !strcmp( # name, tName ) ){ return code; } else
    for_all_detectors( return_major_number_if_matches )
    # undef return_major_number_if_matches
    {
        return 0;  /* unknown */
    }
}
# endif

