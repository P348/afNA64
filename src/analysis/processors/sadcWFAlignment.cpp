/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/sadcWFAlignment.hpp"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include <goo_dict/parameters/path_parameter.hpp>

# include "analysis/sadcWF_supp.h"
# include "na64_uevent.hpp"

# include <TH1F.h>
# include <iomanip>

namespace na64 {
namespace dprocessors {
namespace aux {

//
// Align algorithms
//

# define wfshift_signature(fname)               \
int                                             \
fname(                                          \
        uint16_t * samples, uint8_t nsamples,   \
        float * zero1Ptr,   float * zero2Ptr,   \
        float threshold )

static int
mean_discrepancy(
        uint16_t * samples, uint8_t nsamples,
        float * zero1Ptr,   float * zero2Ptr,
        float threshold ) {
    double meanDelta = 0,
           cDelta,
           z1, meanZ1 = 0.,
           z2, meanZ2 = 0.,
           discrepancy;
    uint8_t nAccumulated;
    for( nAccumulated = 0;
         nAccumulated < nsamples;
         nAccumulated +=2 ) {
        z1 = samples[nAccumulated    ];
        z2 = samples[nAccumulated + 1];
        cDelta = 2*fabs(z1 - z2)/(z1 + z2);
        if( meanDelta ) {
            float tmpMean = meanDelta/nAccumulated;
            discrepancy = fabs(1 - fabs(tmpMean - cDelta)/tmpMean);
            if( discrepancy > threshold ) {
                //printf("# stop, since %f > %f\n", discrepancy, threshold);
                break;  // stop accumulation
            }
        }
        meanDelta += cDelta;
        meanZ1 += z1;
        meanZ2 += z2;
    }
    if( !nAccumulated || nAccumulated == nsamples ) return -1;
    //meanDelta /= nAccumulated;
    *zero1Ptr = meanZ1/(nAccumulated/2);
    *zero2Ptr = meanZ2/(nAccumulated/2);
    return nAccumulated;
}

static int
first_two(
        uint16_t * samples, uint8_t,
        float * zero1Ptr,   float * zero2Ptr,
        float) {
    *zero1Ptr = samples[0];
    *zero2Ptr = samples[1];
    return 2;
}

static int
first_four(
        uint16_t * samples, uint8_t,
        float * zero1Ptr,   float * zero2Ptr,
        float) {
    *zero1Ptr = (samples[0] + samples[2])/2;
    *zero2Ptr = (samples[1] + samples[3])/2;
    return 4;
}

static int
first_eight(
        uint16_t * samples, uint8_t,
        float * zero1Ptr,   float * zero2Ptr,
        float) {
    *zero1Ptr = (samples[0] + samples[2] + samples[4] + samples[6])/4;
    *zero2Ptr = (samples[1] + samples[3] + samples[5] + samples[7])/4;
    return 8;
}

//
// Processor class declaration
//

const char WFAligner::_st_meanFileFmt[] = "%10s\t%10x\t%10a\t%10a\t%5x %5x\t%10e\t%e\n";

const WFAligner::DynamicCorrectionMethod WFAligner::defaultAlignmentMethod = WFAligner::byMean;

//
// Processor class implementation
//

void
WFAligner::_V_print_brief_summary( std::ostream & os ) const {
    os << ESC_CLRGREEN "sadcWFAlign processor" ESC_CLRCLEAR ":" << std::endl
       << "  statistics collected ....... : " << (do_collect_stats() ? "yes" : "no") << std::endl;
    if( !do_collect_stats() ) {
        return;
    }
    os << "  # of unique detector IDs ... : " << _entries.size() << std::endl;
    os << "  input mean zeroes file ..... : " << ( _meansInFileName.empty()  ? "<none>" : _meansInFileName.c_str() ) << std::endl;
    os << "  output mean zeroes file .... : " << ( _meansOutFileName.empty() ? "<none>" : _meansOutFileName.c_str() ) << std::endl;
    os << "  discr. threshold ........... : " << _threshold << std::endl;
    if( _badZeroes ) {
        os << "  \"bad\" pedestals ............ :" << _badZeroes << std::endl;
    }
}

TH1F**
WFAligner::_V_new_entry( AFR_DetSignature id, TDirectory * ) {
    AFR_UniqueDetectorID uID(id);
    auto res = new TH1F * [2];
    for(uint8_t i = 0; i < 2; ++i) {
        char detNamebf[32], namebf[64], labelbf[128];
        snprintf_detector_name( detNamebf, 32, uID );
        //std::cout << std::hex << (int) uID.byNumber.major << ":"
        //          << std::hex << (int) uID.byNumber.minor << " "
        //          << " :: " << detNamebf << std::endl;
        snprintf( namebf, 64, "%s-ped%d", detNamebf, (int) i );
        snprintf( labelbf, 128, "Pedestals #%d for %s det.", (int) i, detNamebf );
        res[i] = new TH1F( namebf, labelbf, _hstPars.nBins, _hstPars.min, _hstPars.max );
    }
    return res;
}

void
WFAligner::_V_free_entry( AFR_DetSignature, TH1F** th ) {
    delete [] th;
}

WFAligner::WFAligner(
            const std::string & pn,
            const std::string & algo,
            const sV::aux::HistogramParameters1D & hstPars,
            const goo::filesystem::Path & meansInFileName,
            const goo::filesystem::Path & meansOutFileName,
            const std::string & dynamicCorrectionMethod,
            double threshold ) :
                    analysis::iSADCProcessor( pn ),
                    Catalogue("pedestals"),
                    _threshold( threshold ),
                    _hstPars(hstPars),
                    _doCollectStats(_hstPars.nBins),
                    _badZeroes(0),
                    _meansInFileName(  meansInFileName.interpolated() ),
                    _meansOutFileName( meansOutFileName.interpolated() ),
                    _dynamicAlignment( dynamicCorrectionMethod ) {
    if( algo == "first_two" ) {
        _zeroes_finder = first_two;
        sV_log2( "SADC pedestals will be find by first two samples.\n" );
    } else if( algo == "first_four" ) {
        _zeroes_finder = first_four;
        sV_log2( "SADC pedestals will be find by first four samples.\n" );
    } else if( algo == "first_eight" ) {
        _zeroes_finder = first_eight;
        sV_log2( "SADC pedestals will be find by first eight samples.\n" );
    } else if( algo == "mean_discrepancy" ) {
        _zeroes_finder = mean_discrepancy;
        sV_log2( "SADC pedestals will be find by first few samples lying inside "
        "discrepancy %e.\n", _threshold );
    } else {
        emraise( badParameter, "There is no algorithm \"%s\" for SADC amplitudes alignment.",
                 algo.c_str() );
    }
    if( !_meansInFileName.empty() ) {
        if( !_dynamicAlignment.empty() && "mean" != _dynamicAlignment && "disable" != _dynamicAlignment ) {
            sV_logw( "Processed parameterization redundant: "
            "both dynamic alignment option, and input pedestals file "
            "are provided. Pedestals file has precedence, dynamic alignment "
            "won't be performed.\n");
        }
        _alignmentMethod = disable;
        FILE * infile = fopen(_meansInFileName.c_str(), "r");
        if( !infile ) { return; }
        char line[256];
        AFR_DetSignature num, numUnused1, numUnused2;
        float z[2], unusedz[2];
        char detNameRead[11];
        while( fgets(line, sizeof(line), infile) ) {
            sscanf( line, _st_meanFileFmt, detNameRead, &num, z, z+1,
                    &numUnused1, &numUnused2, unusedz, unusedz + 1 );
            //assert( num = numUnused );
            _meanZeroes.emplace( num, std::pair<float, float>(z[0], z[1]) );
            //fprintf( stdout, _st_meanFileFmt, num, z[0], z[1], numUnused, unusedz[0], unusedz[1] );
        }
        fclose(infile);
    } else if( !_dynamicAlignment.empty() ) {
        if( "event" == _dynamicAlignment ) {
            _alignmentMethod = byEvent;
            sV_log2( "Dynamic alignment will be performed according to "
                     "current event data.\n" );
        } else if( "mean" == _dynamicAlignment ) {
            if( !do_collect_stats() ) {
                emraise( badState, "Statistics accumulation disabled while "
                    "mean zero correction requested." );
            }
            if(meansInFileName.interpolated().empty()) {
                emraise( badParameter, "Can not perform amplitude correction "
                    "by mean values since file containing them is not "
                    "specified by config." );
            }
            _alignmentMethod = byMean;
            sV_log2( "Dynamic alignment will be performed according to "
                        "accumulated mean.\n" );
        } else if( "disable" == _dynamicAlignment ) {
            _alignmentMethod = disable;
            sV_log2( "Dynamic alignment will not be performed.\n" );
        } else {
            sV_logw( "Default dynamic alignemnt method will be used as "
                         "could not interpret token \"%s\".\n", _dynamicAlignment.c_str() );
            _alignmentMethod = defaultAlignmentMethod;
        }
    } else {
        sV_logw( "Default dynamic alignemnt method will be used as "
                     "this parameter was not provided.\n" );
        _alignmentMethod = defaultAlignmentMethod;
    }
}

WFAligner::WFAligner( const goo::dict::Dictionary & dct ) :
        WFAligner(
            "SADCWFAligner",
            dct["algorithm"].as<std::string>(),
            dct["collect-zeroes"].as<sV::aux::HistogramParameters1D>(),
            dct["means-file-in"].as<goo::filesystem::Path>(),
            dct["means-file-out"].as<goo::filesystem::Path>(),
            dct["enable-dynamic-correction"].as<std::string>(),
            dct["threshold"].as<double>()
        ){}

WFAligner::~WFAligner() {}

sV::aux::iEventProcessor::ProcRes
WFAligner::_V_process_SADC_profile_event( na64::events::SADC_profile & sadcProf ){
    uint16_t samplesCache[32];
    float zero1, zero2;
    TH1F ** pedHsts = nullptr;

    assert( sadcProf.samples_size() == 32 );
    for( int nSample = 0;
         nSample < sadcProf.samples_size();
         ++nSample ) {
        samplesCache[nSample] = sadcProf.samples( nSample );
    }
    // find zeroes:
    _zeroes_finder( samplesCache, 32, &zero1, &zero2, _threshold );
    if( !_meanZeroes.empty() && _threshold > 0 ) {
        auto it = _meanZeroes.find( (AFR_DetSignature) sadcProf.detectorid() );
        if( _meanZeroes.end() == it ) {
            AFR_UniqueDetectorID detID(sadcProf.detectorid());
            sV_logw( "Mean pedestal comparison omitted for unknown det. id %#x (%s).\n",
                    detID.wholenum, detector_name_by_code( (EnumScope::MajorDetectorsCode) detID.byNumber.major ) );
        } else {
            if( fabs(zero1 - it->second.first)/it->second.first > _threshold ) {
                zero1 = it->second.first;
                sadcProf.mutable_suppinfo()->set_badzero1(true);
                ++_badZeroes;
            } else {
                sadcProf.mutable_suppinfo()->set_badzero1(false);
            }
            if( fabs(zero2 - it->second.second)/it->second.second > _threshold ) {
                zero2 = it->second.second;
                sadcProf.mutable_suppinfo()->set_badzero2(true);
                ++_badZeroes;
            } else {
                sadcProf.mutable_suppinfo()->set_badzero2(false);
            }
        }
    }
    if( do_collect_stats() ) {
        pedHsts = this->consider_entry(sadcProf.detectorid());
        pedHsts[0]->Fill(zero1);
        pedHsts[1]->Fill(zero2);
    }
    sadcProf.mutable_suppinfo()
          ->set_pedestal1(zero1);
    sadcProf.mutable_suppinfo()
          ->set_pedestal2(zero2);
    // Note, that dynamic alignment will only take place when
    // no input pedestals file provided. (TODO: what?!)
    if( _meanZeroes.empty() && disable != _alignmentMethod ) {
        switch( _alignmentMethod ) {
            case byMean : {
                zero1 = pedHsts[0]->GetMean(1);
                zero2 = pedHsts[1]->GetMean(1);
            } break;
            default:
                /* use current event */
                break;
        }
        // correct amplitudes by zeroes
        for( int nSample = 0;
             nSample < sadcProf.samples_size();
             nSample += 2) {
            sadcProf.set_samples( nSample,
                    samplesCache[nSample] - zero1
                );
        }
        for( int nSample = 1;
             nSample < sadcProf.samples_size();
             nSample += 2) {
            sadcProf.set_samples( nSample,
                    samplesCache[nSample] - zero2
                );
        }
    }

    return RC_CORRECTED;
}

void WFAligner::_print_histogram_diagnosis(
        const std::pair<Catalogue::Parent::HashKey,
                        Catalogue::Parent::CategorizedEntry> & pair ) const {
    const AFR_UniqueDetectorID detID( pair.first );
    const char * detName = detector_name_by_code( (EnumScope::MajorDetectorsCode)
                                   detID.byNumber.major );

    const Int_t underflowBinNo = 0,
                overflowBinNo = pair.second[0]->GetSize() - 1
          ;
    assert( pair.second[0]->IsBinOverflow( overflowBinNo ) 
         && pair.second[1]->IsBinOverflow( overflowBinNo ) );
    assert( pair.second[0]->IsBinUnderflow( underflowBinNo ) 
         && pair.second[1]->IsBinUnderflow( underflowBinNo ) );

    # define _check_histogram_overflow_and_underflow( n, tp )                                       \
    if( pair.second[n]->GetBinContent(tp ## BinNo) ) {                                              \
        sV_logw( "Found %d entries (%d%% of %d) in " #tp " bin for SADC %x:%x:" #n " (%s)."     \
            "It may be a hardware issue or zero binning intervals have to be "                      \
            "changed.\n",                                                                           \
            (int) pair.second[n]->GetBinContent(tp ## BinNo),                                       \
            (int) (100*pair.second[n]->GetBinContent(tp ## BinNo)/double(pair.second[n]->GetEntries())), \
            (int) pair.second[n]->GetEntries(),                                                     \
            (int) detID.byNumber.major, (int) detID.byNumber.minor,                                 \
            detName ); }

    _check_histogram_overflow_and_underflow( 0, overflow )
    _check_histogram_overflow_and_underflow( 0, underflow )
    _check_histogram_overflow_and_underflow( 1, overflow )
    _check_histogram_overflow_and_underflow( 1, underflow )
}

void
WFAligner::_V_finalize() const {
    if( !_meansOutFileName.empty() ) {
        FILE * ofile = fopen( _meansOutFileName.c_str(), "w" );
        if( !ofile ) {
            sV_loge( "Couldn't open file `%s' for writing.\n", _meansOutFileName.c_str() );
            return;
        }
        for( auto it = _entries.begin(); it != _entries.end(); ++it ) {
            const AFR_UniqueDetectorID detID( it->first );
            const char * detName = detector_name_by_code( (EnumScope::MajorDetectorsCode)
                                                          detID.byNumber.major );
            fprintf( ofile, _st_meanFileFmt,
                detName,
                detID.wholenum,
                it->second[0]->GetMean(1), it->second[1]->GetMean(1),   // < 
                detID.byNumber.major, detID.byNumber.minor,             // < here we writing pedestals data twice,
                it->second[0]->GetMean(1), it->second[1]->GetMean(1));  // < both in hexidecimal and human-readable formats.
            _print_histogram_diagnosis( *it );
        }
        fclose(ofile);
    }
}

StromaV_ANALYSIS_PROCESSOR_DEFINE_MCONF( WFAligner, "SADCWFAligner" ) {
    //
    // Own config section.
    goo::dict::Dictionary ownDict( "SADCWaferormAlignment",
        "Performs alignment of paired SADC amplitudes considering data about "
        "pedestal values." );
    ownDict.insertion_proxy()
        .p<std::string>( "algorithm",
                "Available options are: mean_discrepancy / first_two / "
                "first_four / first_eight.",
            "mean_discrepancy" )
        .p<sV::aux::HistogramParameters1D>("collect-zeroes",
                "Parameters of histograms filled with pedestals.",
            sV::aux::HistogramParameters1D(100, 0, 5000) )
        .p<double>( "threshold",
                "Zeroes sensitivity threshold --- number to be compared with "
                "|A_0 - A_i|/A_i. Used in \"badZero\" marking and in for "
                "\"mean_discrepancy\" algorithm.",
            0.1 )
        .p<goo::filesystem::Path>( "means-file-in",
                "Input file for previously found zeroes means in ASCII"
                "fmt: <hex:detID> <hex:z1-mean> <hex:z2-mean>."
                "Additionaly, decimal floating point numbers pair can be "
                "provided for readability.",
            "" )
        .p<goo::filesystem::Path>("means-file-out",
                "Output file for found zeroes means in ASCII fmt: "
                "<hex:detID> <hex:z1-mean> <hex:z2-mean>",
            "" )
        .p<std::string>( "enable-dynamic-correction",
                "When no SADC pedestal file provided, align waveforms "
                "dynamically taking into account data that was accumulated "
                "with previous events. Available options are: \"event\" "
                "(immediately use pedestals found for particular event) "
                "\"mean\" (use accumulated mean values for particular channel) "
                "and \"disable\". Note, that dynamic correction only takes "
                "place when no input pedestal file is provided.",
            "disable" )
    ;

    //
    // Define injection mapping from common to own section.
    # define M_CMN_PRFX "analysis.processors.na64.SADC.WaveformAlignment."
    goo::dict::DictionaryInjectionMap inj;
    inj ( "algorithm",                   M_CMN_PRFX "algorithm" )
        ( "collect-zeroes",              M_CMN_PRFX "collect-zeroes" )
        ( "threshold",                   M_CMN_PRFX "threshold" )
        ( "means-file-in",               M_CMN_PRFX "means-file-in" )
        ( "means-file-out",              M_CMN_PRFX "means-file-out" )
        ( "enable-dynamic-correction",   M_CMN_PRFX "enable-dynamic-correction" )
        ;
    # undef M_CMN_PRFX
    return std::make_pair( ownDict, inj );
}

}  // namespace aux
}  // namespace dprocessors
}  // namespace na64

# endif  // defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

