/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES) && \
                               defined(AFNA64_RECO_FOUND)
# include "analysis/processors/biplot.hpp"

namespace na64 {
namespace dprocessors {

Biplotter::Biplotter ( const std::string & pn,
                       const goo::dict::Dictionary & dct) :
                        ExpEventProcessor(pn) {
    sV::aux::HistogramParameters2D hstPars =
    dct["biplot-histogram"].as<sV::aux::HistogramParameters2D>();
    _biplot = new TH2F("biplot", "ECAL vs HCAL energy distribution",
                    hstPars.nBins[0], hstPars.min[0], hstPars.max[0],
                    hstPars.nBins[1], hstPars.min[1], hstPars.max[1]);
}

Biplotter::~Biplotter() {
    //delete _biplot;
}

sV::aux::iEventProcessor::ProcRes
Biplotter::_V_process_event_payload(NA64ExperimentalEvent & event) {
    double ecalEnergy = 0, hcalEnergy = 0;
    for ( int sadc = 0; sadc < event.sadc_data_size(); sadc++) {
        const na64::events::SADC_profile * sadcProfile = &(event.sadc_data(sadc));
        AFR_UniqueDetectorID detectorIDUnion( sadcProfile->detectorid());
        // TODO avoid str comparison
        if ( EnumScope::d_ECAL0 == detectorIDUnion.byNumber.major ||
             EnumScope::d_ECAL1 == detectorIDUnion.byNumber.major ) {
            ecalEnergy += sadcProfile->edep();
        }
        else if ( EnumScope::d_HCAL0 == detectorIDUnion.byNumber.major ||
                  EnumScope::d_HCAL1 == detectorIDUnion.byNumber.major ||
                  EnumScope::d_HCAL2 == detectorIDUnion.byNumber.major ||
                  EnumScope::d_HCAL3 == detectorIDUnion.byNumber.major ) {
            hcalEnergy += sadcProfile->edep();
        }
    }
    //std::cout << "===ECAL: " << ecalEnergy << " HCAL: " << hcalEnergy <<std::endl;
    assert(_biplot != nullptr);
    _biplot->Fill(ecalEnergy, hcalEnergy);
    return true;
}

StromaV_ANALYSIS_PROCESSOR_DEFINE_MCONF( Biplotter, "biplotter" ) {
    goo::dict::Dictionary biplotHistConf( "biplotter",
        "Makes a standard TH2F histogram (ECAL vs HCAL energy)" );
    biplotHistConf.insertion_proxy()
        .p<sV::aux::HistogramParameters2D>( "biplot-histogram",
            "Energy binning boundaries for the standard biplot histogram.",
            sV::aux::HistogramParameters2D(250, 0, 250, 250, 0, 250) )
        ;
    goo::dict::DictionaryInjectionMap injM;
        injM( "biplot-histogram",            "analysis.processors.na64.SADC.biplot-histogram" )
        ;

    return std::make_pair( biplotHistConf, injM);
}

}  // namespace dprocessors
}  // namespace na64

# endif  // defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES) &&
         // defined(AFNA64_RECO_FOUND)

