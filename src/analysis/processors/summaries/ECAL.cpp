/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "afNA64_config.h"

# if defined(RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)
# include "analysis/processors/summaries/ECAL.hpp"

namespace sV {
namespace dprocessors {
namespace evd {

bool
ECALPreproc::_V_process_event( Event * event ) {
    if( event->has_simulated() ) {
        _TODO_  // TODO: process simulated event
    } else {
        return AnalysisApplication::iSADCProcessor::_V_process_event( event );
    }
}

bool
ECALPreproc::_V_process_experimental_event( ::sV::events::ExperimentalEvent * expEve ) {
    bool res = AnalysisApplication::iSADCProcessor::_V_process_experimental_event( expEve );
    //_cache_evdMG->mutable_maxdep()->set_amplitude( _cache_maxEDep );
    return res;
}

bool
ECALPreproc::_V_process_SADC_profile_event( ::na64::events::SADC_profile * sadcProfMutablePtr ) {
    ::na64::events::SADC_profile & sadcProf = *sadcProfMutablePtr;
    // Ignore non-ECAL SADCs:
    AFR_UniqueDetectorID detID( sadcProf.detectorid() );
    if( detector_family_num( detID.byNumber.major ) != EnumScope::fam_ECAL ) {
        return true;
    }
    assert( sadcProf.samples_size() == 32 );
    float ampSum = 0.;

    // if sum is already calculated, use it; otherwise calculate it here
    if(!( sadcProf.has_suppinfo() && (ampSum = sadcProf.suppinfo().sum()) )) {
        for( int nSample = 0; nSample < sadcProf.samples_size(); ++nSample ) {
            ampSum += sadcProf.samples( nSample );
        }
    }

    auto cacheECALIt = _cache_evdMG.find( detID.byNumber.major );
    // If there is no cache yet, create one:
    if( _cache_evdMG.end() == cacheECALIt ) {
        cacheECALIt = _cache_evdMG.emplace(
                detID.byNumber.major,
                mixins::PBEventApp::c_event()
                    .mutable_eventdisplaymessage()
                    ->add_summary()
            ).first;
        cacheECALIt->second->set_detectorid( detID.byNumber.major );
    }
    events::DetectorSummary & sm = *(cacheECALIt->second);
    events::DetectorSummary * thisSummary = sm.mutable_subgroup()->add_summary();
    thisSummary->set_detectorid( detID.wholenum );
    thisSummary->mutable_edep()->set_amplitude( ampSum );

    if( sm.subgroup().maxdep().amplitude() < ampSum ) {
        sm.mutable_subgroup()->mutable_maxdep()->set_amplitude( ampSum );
    }

    return true;
}

void
ECALPreproc::_V_finalize_event_processing( Event * ) {
    // Clear ECAL subgroup cache ptr for this event.
    _cache_evdMG.clear();
}

void
ECALPreproc::_V_print_brief_summary( std::ostream & ) const {
}

void
ECALPreproc::_V_finalize() const {
}

StromaV_DEFINE_DATA_PROCESSOR( ECALPreproc ) {
    return new ECALPreproc();
} StromaV_REGISTER_DATA_PROCESSOR(
    ECALPreproc,
    "evd-ECAL",
    "Event display preprocessor for electromagnetic calorimeter (ECAL)." )

}  // namespace evd
}  // namespace dprocessors
}  // namespace sV

# endif  // defined(RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

