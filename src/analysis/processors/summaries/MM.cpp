/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/summaries/MM.hpp"

# if defined(RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

namespace sV {
namespace dprocessors {
namespace evd {

//
// IMPLEMENTATION
////////////////

bool
MicroMegaPreprocessor::_V_process_APV_samples( ::na64::events::APV_sampleSet * apvSet ) {
    bool doContinue = true;
    AFR_UniqueDetectorID dID( apvSet->detectorid() );
    // Omit further evaluation if this is not a micromega detector or APV
    // cluster set is empty:
    if( EnumScope::fam_MuMega != detector_family_num( dID.byNumber.major ) ) {
        //sV_log2( "MicroMegaPreprocessor::_V_process_APV_samples(): omitting non-MM 0x%x / %d.\n",
        //                dID.wholenum, apvSet->clusters().clusters_size() );  // XXX
        return doContinue;
    }

    if( 0 == apvSet->clusters().clusters_size() ) {
        // emty cluster set --- nothing to do.
        ++_emptySets;
        return doContinue;
    }
    ++_consideredSets;

    // New mutable event summary instance to fill clusters excerpt:
    ::na64::events::DetectorSummary * mmSummaryPtr = goo::app<mixins::PBEventApp>()
                .c_event()
                .mutable_eventdisplaymessage()
                ->add_summary();

    mmSummaryPtr->set_detectorid( dID.wholenum );

    // clusterMsg is located here:
    //  Event
    //  / experimental : ExperimentalEvent
    //  / apv_data : APV_sampleSet
    //  / clusters : ClustersArray
    //  / <i> : APV_Cluster
    if( disable == _bestSelectionAlgorithm ) {
        // Get the clusters, choosing few most reliable ones.
        for(int i = 0; i < apvSet->clusters().clusters_size(); ++i) {
            const events::APV_Cluster & oldCluster = apvSet->clusters().clusters(i);
            events::APV_Cluster * newClPtr = mmSummaryPtr->mutable_points()->add_clusters();
            newClPtr->CopyFrom( oldCluster );
            ++_nSent;
            ++_nConsidered;
        }
    } else if( maxAmplitude == _bestSelectionAlgorithm ) {
        int nMax = 0;
        double aMax = apvSet->clusters().clusters(0).trackpoint().amplitude();
        for(int i = 1; i < apvSet->clusters().clusters_size(); ++i) {
            const events::APV_Cluster & cl = apvSet->clusters().clusters(i);
            if( cl.trackpoint().amplitude() > aMax ) {
                aMax = cl.trackpoint().amplitude();
                nMax = i;
            }
            ++_nConsidered;
        }
        mmSummaryPtr->mutable_points()->add_clusters()->CopyFrom(
            apvSet->clusters().clusters( nMax ) );
        ++_nSent;
    } else if( maxReliability == _bestSelectionAlgorithm ) {
        bool niceCluster = false;
        int nBest = 0;
        double bestRatioDiscrepancy =
            std::fabs( apvSet->clusters().clusters(0).xyratio() - 1. );

        for( uint8_t i = 0; i < 4; ++i ) {
            if( apvSet->clusters().clusters(0).ratios(i) < -.8
             || apvSet->clusters().clusters(0).ratios(i) >  .8 ) {
                niceCluster = false;
            }
        }

        for(int i = 1; i < apvSet->clusters().clusters_size(); ++i) {
            const events::APV_Cluster & cl = apvSet->clusters().clusters(i);
            // Now, choose the cluster with XY ratio closest to 1 and
            // a ratios between -0.8 and 0.8
            if( std::fabs( cl.xyratio() - 1. ) < bestRatioDiscrepancy ) {
                bool chooseThis = true;
                for( uint8_t i = 0; i < 4; ++i ) {
                    if( cl.ratios(i) < -.8 || cl.ratios(i) > .8 ) {
                        chooseThis = false;
                        break;
                    }
                }
                if( chooseThis ) {
                    bestRatioDiscrepancy = std::fabs( cl.xyratio() - 1. );
                    nBest = i;
                }
            }
            ++_nConsidered;
        }
        if( niceCluster ) {
            mmSummaryPtr->mutable_points()->add_clusters()->CopyFrom(
                    apvSet->clusters().clusters( nBest ) );
            ++_nSent;
        } else {
            ++_suspiciousClustersOmitted;
        }
    } else {
        emraise( badState, "Cluster selection algorithm is set to wrong value (uninitialized?)." );
    }
    return doContinue;
}

void
MicroMegaPreprocessor::_V_finalize_event_processing( Event * ) {
    // Reset in-event caches if need.
    // ...
}

void
MicroMegaPreprocessor::_V_finalize() const {
    // Clear all if need.
    // ...
}

void
MicroMegaPreprocessor::_V_print_brief_summary( std::ostream & os ) const {
    os << ESC_CLRGREEN "Clusters for MM" ESC_CLRCLEAR ":" << std::endl
       << "  clusters considered ........ : " << _nConsidered << std::endl
       << "  clusters sent .............. : " << _nSent << std::endl
       << "  sets considered ............ : " << _consideredSets << std::endl
       << "  empty sets met ............. : " << _emptySets << " ("
                << (0 == _consideredSets + _emptySets ? 0 : 100*double(_emptySets)/(_consideredSets + _emptySets) )
                << "%%)" << std::endl
    ;
    if( maxReliability == _bestSelectionAlgorithm ) {
        os << "  suspicious clusters ignored  : " << _suspiciousClustersOmitted << std::endl;
    }
}

//
// REGISTER PROCESSOR
////////////////////

StromaV_DEFINE_CONFIG_ARGUMENTS {
    po::options_description mmSummaryOpts( "APV preprocessing summary opts for Micromegas" );
    { mmSummaryOpts.add_options()
        ("evd-summary.MM.bestCluster",
            po::value<std::string>()->default_value("disable"),
            "Name for an algorithm selecting the best cluster. "
            "Possible values: disable (sends all) / maxAmplitude / maxReliability." )
        ;
    }
    return mmSummaryOpts;
}
StromaV_DEFINE_DATA_PROCESSOR( MicroMegaPreprocessor ) {
    const std::string aStr = goo::app<AnalysisApplication>().cfg_option<std::string>(
                                    "evd-summary.MM.bestCluster" );
    MicroMegaPreprocessor::BestClusterSelection bcsAlgo;
    if( "disable" == aStr ) {
        bcsAlgo = MicroMegaPreprocessor::disable;
    } else if( "maxAmplitude" == aStr ) {
        bcsAlgo = MicroMegaPreprocessor::maxAmplitude;
    } else if( "maxReliability" == aStr ) {
        bcsAlgo = MicroMegaPreprocessor::maxReliability;
    } else {
        sV_logw( "Couldn't interpret best cluster selection algorithm "
                     "name \"%s\" for micromegas. Will disable it at all." );
        bcsAlgo = MicroMegaPreprocessor::disable;
    }
    return new MicroMegaPreprocessor( bcsAlgo );
} StromaV_REGISTER_DATA_PROCESSOR(
    MicroMegaPreprocessor,
    "evd-MM",
    "Event display preprocessor for Micromegas." )

}  // namespace evd
}  // namespace dprocessors
}  // namespace sV

# endif  // defined(RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

