/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/sadcWFReconstruction.hpp"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include "na64_uevent.hpp"
# include "analysis/sadcWF_supp.h"

# include <StromaV/app/cvalidators.hpp>

# include <TProfile.h>
# include <TH2F.h>
# include <TH1F.h>

namespace na64 {
namespace dprocessors {
namespace aux {

// DistributionProfile
/////////////////////

DistributionProfile::DistributionProfile(
            const std::string & nameSuffix,
            const std::string & labelSuffix,
            sV::aux::HistogramParameters2D pars ) {
    _profilePtr = new TProfile(
            (nameSuffix + "-prof").c_str(),
            (labelSuffix + " profile").c_str(),
            pars.nBins[0], pars.min[0], pars.max[0] );
    _distributionPtr = new TH2F(
            (nameSuffix + "-dstr").c_str(),
            (labelSuffix + " profile distribution").c_str(),
            pars.nBins[0], pars.min[0], pars.max[0],
            pars.nBins[1], pars.min[1], pars.max[1] );
}

void
DistributionProfile::fill( float x, float y ) {
    _profilePtr->Fill(x, y);
    _distributionPtr->Fill(x, y);
}

//
// ReconstructionStatistics
//////////////////////////

ReconstructionStatistics::ReconstructionStatistics(
        const std::string & detectorName,
        const sV::aux::HistogramParameters2D & maxHstmsPars,
        const sV::aux::HistogramParameters2D & meanHstmsPars,
        const sV::aux::HistogramParameters1D & relDstHstmPars,
        const sV::aux::HistogramParameters1D & stdDeviationHstPars,
        const sV::aux::HistogramParameters1D & sumDstHstPars,
        const sV::aux::HistogramParameters1D & timeDispersionPars
        ) :
           absMax("absMax" + detectorName, "Abs. max values of " + detectorName, maxHstmsPars),
           mean("mean" + detectorName, "Mean values of " + detectorName, meanHstmsPars) {
    _reliabilityDistribution = new TH1F(
            ("rel" + detectorName).c_str(), ("Reliability on " + detectorName).c_str(),
            relDstHstmPars.nBins, relDstHstmPars.min, relDstHstmPars.max );
    _stdDeviation = new TH1F(
            ("stdDev-" + detectorName).c_str(), ("Std. deviation on " + detectorName).c_str(),
            stdDeviationHstPars.nBins, stdDeviationHstPars.min, stdDeviationHstPars.max );
    _sumDistribution = new TH1F(
            ("sumDistrib-" + detectorName).c_str(), ("Sum distribution of " + detectorName).c_str(),
            sumDstHstPars.nBins, sumDstHstPars.min, sumDstHstPars.max );
    _timeDispersion = new TH1F(
            ("timeDisp-" + detectorName).c_str(), ("Time dispersion on " + detectorName).c_str(),
            timeDispersionPars.nBins, timeDispersionPars.min, timeDispersionPars.max );
}

void
ReconstructionStatistics::consider_exp_sadc_stats(
                AFR_UniqueDetectorID /*uid*/,
                const na64::events::SADC_suppInfo & sInfo ) {
    if( sInfo.absmaxnbin() || sInfo.absmaxval() ) {
        absMax.fill( sInfo.absmaxnbin(), sInfo.absmaxval() );
    }
    if( sInfo.mean() || sInfo.weightedmean() ) {
        mean.fill( sInfo.weightedmean()*32, sInfo.mpv() );
    }
    if( sInfo.reliability() ) {
        _reliabilityDistribution->Fill( sInfo.reliability() );
    }
    if( sInfo.stddeviation() ) {
        _stdDeviation->Fill( sInfo.stddeviation() );
    }
    if( sInfo.sum() ) {
        _sumDistribution->Fill( sInfo.sum() );
    }
    if( sInfo.timedispersion() ) {
        _timeDispersion->Fill( sInfo.timedispersion() );
    }
}

// IMPLEM
////////

SADC_WF_Reconstruction::SADC_WF_Reconstruction( const std::string & pn ) :
            na64::analysis::iSADCProcessor(pn),
            sV::mixins::DetectorCatalogue<ReconstructionStatistics *>("Fitting") {
    _nConsidered
        = _nRefused_nonNumCh
        = _nRefused_badAlgo
        = _nRefused_negativeSum
        = 0;
    samples.n = 32;
    samples.samples = (SADCSamples *) malloc( samples.n*sizeof(SADCSamples) );
    samples.sigma   = (double *)      malloc( samples.n*sizeof(double) );
    ::allocate_SADCWF_cache( &_chParsReentrant, 32 );
    _fitting_function = ::moyal;  // TODO
}

SADC_WF_Reconstruction::SADC_WF_Reconstruction( const goo::dict::Dictionary & ) : 
            na64::analysis::iSADCProcessor( "SADCWFReconstruction" ),
            sV::mixins::DetectorCatalogue<ReconstructionStatistics *>("Fitting") {
    _TODO_  // TODO
}

SADC_WF_Reconstruction::~SADC_WF_Reconstruction() {
    free( samples.samples );
    free( samples.sigma );
    ::free_SADCWF_cache( &_chParsReentrant, 32 );
}

ReconstructionStatistics *
SADC_WF_Reconstruction::_V_new_entry( AFR_DetSignature id, TDirectory * famDir ) {
    using sV::aux::HistogramParameters1D;
    using sV::aux::HistogramParameters2D;
    using sV::AnalysisApplication;
    char detectorNameBuffer[64];
    snprintf_detector_name(
            detectorNameBuffer,
            sizeof(detectorNameBuffer),
            AFR_UniqueDetectorID(id) );
    famDir->mkdir(detectorNameBuffer)->cd();
    # define M_CMN_PRFX "analysis.processors.na64.SADC.WaveformReconstruction."
    return new ReconstructionStatistics(
            detectorNameBuffer,
            goo::app<AnalysisApplication>() \
                .cfg_option<HistogramParameters2D>( M_CMN_PRFX "abs-max-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<HistogramParameters2D>( M_CMN_PRFX "mean-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<HistogramParameters1D>( M_CMN_PRFX "reliab-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<HistogramParameters1D>( M_CMN_PRFX "std-dev-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<HistogramParameters1D>( M_CMN_PRFX "sum-hst"),
            goo::app<AnalysisApplication>()
                    .cfg_option<HistogramParameters1D>( M_CMN_PRFX "time-disp-hst")
        );
    # undef M_CMN_PRFX
}

void
SADC_WF_Reconstruction::_V_free_entry( AFR_DetSignature, ReconstructionStatistics * ptr ) {
    delete ptr;
}

void  // TODO: header for extern usage
translate_ch_pars2uevent(
            const struct SADCWFCharacteristicParameters & chPars,
            ::na64::events::SADC_suppInfo & sInfo ) {
    sInfo.set_absmaxnbin( chPars.absMaxNBin );
    sInfo.set_absmaxval( chPars.absMaxVal );
    sInfo.set_mean( chPars.mean );
    sInfo.set_weightedmean( chPars.weightedMean );
    sInfo.set_mpv( chPars.mpv );
    sInfo.set_sum( chPars.sum );
    sInfo.set_stddeviation( chPars.stdDeviation );
    sInfo.set_reliability( chPars.reliability );
    sInfo.set_timedispersion( chPars.timeDispersion );
}

sV::aux::iEventProcessor::ProcRes
SADC_WF_Reconstruction::_V_process_SADC_profile_event( ::na64::events::SADC_profile & sadcProfile ){
    // initialize caches:
    //AFR_UniqueDetectorID uID {
    //    .wholenum = (UShort) sadcProfile->detectorid() };
    const uint8_t nSamples = sadcProfile.samples_size();
    assert(32 == nSamples);
    for( uint8_t nSample = 0;
         nSample < nSamples;
         ++nSample ) {
        samples.samples[nSample] = sadcProfile.samples( nSample );
    }
    //
    int rc = ::calculate_characteristics( samples.samples, samples.n, &_chParsReentrant );
    _nConsidered++;
    if( rc < 0 ) {
        if( -1 == rc ) {
            _nRefused_badAlgo++;
        } else if( -2 == rc ) {
            _nRefused_negativeSum++;
        } else if( -3 == rc ) {
            _nRefused_nonNumCh++;
        }
        return RC_ACCOUNTED;
    }
    auto statsPtr = this->consider_entry( sadcProfile.detectorid() );
    translate_ch_pars2uevent( _chParsReentrant, *(sadcProfile.mutable_suppinfo()) );
    statsPtr->consider_exp_sadc_stats( sadcProfile.detectorid(), sadcProfile.suppinfo() );
    // TODO: for Moyal model function only
    {
        fittingParameters.moyalPars.p[0] = _chParsReentrant.mpv;
        fittingParameters.moyalPars.p[1] = _chParsReentrant.weightedMean*samples.n;
        fittingParameters.moyalPars.p[2] = 4.44*_chParsReentrant.timeDispersion;
        for( SADCSamplesSize i = 0; i < samples.n; ++i ) {
            samples.sigma[i] = _chParsReentrant.cache->linearity[i];
        }
    }
    // fit
    ::fit_SADC_samples(
            &samples,
            _fitting_function,
            &fittingParameters,
            NULL );
    // TODO: for moyal function only
    {
        na64::events::SADC_ModelParameters & mp = *(sadcProfile.mutable_suppinfo()->mutable_fittingmodel());
        mp.mutable_moyal()->set_p1( fittingParameters.moyalPars.p[0] );
        mp.mutable_moyal()->set_p2( fittingParameters.moyalPars.p[1] );
        mp.mutable_moyal()->set_p3( fittingParameters.moyalPars.p[2] );
    }
    // TODO: use calibration data
    //statsPtr->consider_exp_sadc_stats( sadcProfile->detectorid(), sadcProfile->suppinfo() );

    return RC_CORRECTED;
}

void
SADC_WF_Reconstruction::_V_print_brief_summary( std::ostream & os ) const {
    os << ESC_CLRGREEN "sadcWFReconstruction proc" ESC_CLRCLEAR ":" << std::endl
       << "Spectra processed: " << _nConsidered << ", from them are refused:" << std::endl
       << "  by algorithmic problem ..... : " << (_nRefused_badAlgo ? 100*((double) _nRefused_badAlgo)/_nConsidered : 0.)
           << "% (" << _nRefused_badAlgo << ")"  << std::endl
       << "  by negative sum ............ : " << (_nRefused_negativeSum ? 100*((double) _nRefused_negativeSum)/_nConsidered : 0.)
           << "% (" << _nRefused_negativeSum << ")"  << std::endl
       << "  with non-numerical value ... : " << (_nRefused_nonNumCh ? 100*((double) _nRefused_nonNumCh)/_nConsidered : 0.)
           << "% (" << _nRefused_nonNumCh << ")"  << std::endl
    ;
}

StromaV_ANALYSIS_PROCESSOR_DEFINE_MCONF( SADC_WF_Reconstruction, "SADCWFReconstruction" ) {
    goo::dict::Dictionary ownDict( "SADC-WF-Reconstruction",
        "Options for events handler \"SADCWFReconstruction\" that computes "
        "distribution parameters and fits waveforms." );
    ownDict.insertion_proxy()
        .p<std::string>( "function",
                "Available options are: moyal / ... "
                "Model function to fit WF.",
            "moyal" )
        .flag( "collect-stats",
                "Do collect statistics." )
        .p<sV::aux::HistogramParameters2D>( "abs-max-hst",
                "2D histogram parameters for histograming absolute maximums "
                "of waveforms.",
            sV::aux::HistogramParameters2D(160, 0, 32,  80, 0, 6000) )
        .p<sV::aux::HistogramParameters2D>( "mean-hst",
                "2D histogram parameters for histograming mean values "
                "of waveforms.",
            sV::aux::HistogramParameters2D(160, 0, 32,  80, 0, 4000) )
        .p<sV::aux::HistogramParameters1D>("reliab-hst",
                "1D histogram parameters for histograming \"reliability\" "
                "values.",
            sV::aux::HistogramParameters1D(200, 0, 1.) )
        .p<sV::aux::HistogramParameters1D>( "std-dev-hst",
                "1D histogram parameters for histograming standard deviation "
                "values.",
            sV::aux::HistogramParameters1D(200, 0, 4000) )
        .p<sV::aux::HistogramParameters1D>( "sum-hst",
                "1D histogram parameters for histograming sum values.",
            sV::aux::HistogramParameters1D(200, 0, 5e4) )
        .p<sV::aux::HistogramParameters1D>( "time-disp-hst",
                "1D histogram parameters for histograming time dispersion "
                "values.",
            sV::aux::HistogramParameters1D(200, 0, 6) )
        // ...
        ;
    # define CMN_PRFX "analysis.processors.na64.SADC.WaveformReconstruction."
    goo::dict::DictionaryInjectionMap inj;
    inj ( "function",           CMN_PRFX "function" )
        ( "collect-stats",      CMN_PRFX "collect-stats" )
        ( "abs-max-hst",        CMN_PRFX "abs-max-hst" )
        ( "mean-hst",           CMN_PRFX "mean-hst" )
        ( "reliab-hst",         CMN_PRFX "reliab-hst" )
        ( "std-dev-hst",        CMN_PRFX "std-dev-hst" )
        ( "sum-hst",            CMN_PRFX "sum-hst" )
        ( "time-disp-hst",      CMN_PRFX "time-disp-hst" )
        ;
    return std::make_pair(ownDict, inj);
    # undef CMN_PRFX
}

}  // namespace aux
}  // namespace dprocessors
}  // namespace na64

# endif  // defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

