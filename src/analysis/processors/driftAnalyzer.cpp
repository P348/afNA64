/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/driftAnalyzer.hpp"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

namespace na64 {
namespace dprocessors {

DriftAnalyzer::DriftAnalyzer( const std::string & pn ) :
               SADCProcessor( pn ),
               _currentSpill(0),
               _spillNumberSetted(false) {
               // sV::mixins::DetectorCatalogue<DriftStatistics *>("Fitting") {
    _detMap = new std::unordered_map< AFR_DetSignature, SumStruct * >();
    _detSet = new std::set< AFR_DetFamID >( detectorsToAnalyze,
                                            detectorsToAnalyze +
                                            sizeof( detectorsToAnalyze )/
                                            sizeof( AFR_DetFamID ));
}

DriftAnalyzer::~DriftAnalyzer() {
}

SumStruct::SumStruct( AFR_DetSignature id ) : sumSpill(0),
                                              sumEvent(0),
                                              entries(0) {
    char detNameBf[32], nameSpillBf[64], nameEvBf[64], labelSpillBf[128],
         labelEvBf[128];
    snprintf_detector_name( detNameBf, sizeof(detNameBf), id );
    snprintf( nameSpillBf, 64, "%s-bySpill", detNameBf );
    snprintf( nameEvBf, 64, "%s-byEv", detNameBf );
    snprintf( labelSpillBf, 128, "Spill # vs %s sum energy;#spill;Energy", detNameBf );
    snprintf( labelEvBf, 128, "%s sum energy vs # of evs;Energy, Gev;#events;", detNameBf );
    sumSpillHist = new TH1F ( nameSpillBf, labelSpillBf, 50, 0, 50 );
    sumEventHist = new TH1F ( nameEvBf, labelEvBf, 4000, 0, 400);
}

/* This method retrive detector id, check for existence of this detector
 * in the map. If it is available pushes new entry, if not creates new
 * entry.
 */
sV::aux::iEventProcessor::ProcRes
DriftAnalyzer::_V_process_SADC_profile_event(
                                ::na64::events::SADC_profile & sadcProfile) {
    # if 1
    AFR_UniqueDetectorID detectorIDUnion( sadcProfile.detectorid());

    auto itSet = _detSet->find(
                    detector_family_num( detectorIDUnion.byNumber.major ) );
    if ( itSet != _detSet->end() ) {
        auto itMap = _detMap->find( detectorIDUnion.wholenum );
        if ( itMap != _detMap->end() ) {
            itMap->second->sumSpill += sadcProfile.edep();
            itMap->second->sumEvent += sadcProfile.edep();
        } else {
            auto itNewEntry =
                _add_new_detector_entry( detectorIDUnion.wholenum );
            itNewEntry->second->sumSpill += sadcProfile.edep();
            itNewEntry->second->sumEvent += sadcProfile.edep();
        }
    }
    # endif
    return RC_ACCOUNTED;
}

// Gross. Rework!
bool DriftAnalyzer::_drop_spill() {
    _fill_histos_after_spill();
# if 0  // XXX
for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
    std::cout << it->first << it->second;
    }
# endif
    _clear_map_after_spill();
    return true;
}

sV::aux::iEventProcessor::ProcRes
DriftAnalyzer::_V_process_event_payload( NA64ExperimentalEvent & event ) {
    // Call the parent's method.
    SADCProcessor::_V_process_event_payload( event );
    // Check begin of new spill. If a new spill starts do drop information
    // about previous spill. Check for map emptiness needs for first spill
    // when initial _currentSpill = 0 (!=1). In this case we dont need the drop
    // std::cout << "Spill number " << _currentSpill << " " << event->spillno() << std::endl;
    // std::cout.flush();
    if ( !_spillNumberSetted ) {
        _currentSpill = event.spillno();
        _spillNumberSetted = true;
    }

    if ( _currentSpill != event.spillno() ) {
        _currentSpill = event.spillno();
        _drop_spill();
        std::cout << "Spill number " << _currentSpill << " "
                  << event.spillno() << std::endl;
    }
    return RC_ACCOUNTED;
}

sV::aux::iEventProcessor::ProcRes
DriftAnalyzer::_V_finalize_event_processing( Event & ) {
    _add_event_entries();
    _fill_histos_after_event();
    _clear_map_after_event();
    return RC_ACCOUNTED;
}

void DriftAnalyzer::_V_finalize() const {
}

void DriftAnalyzer::_clear_map_after_spill() {
    for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
        it->second->sumSpill = 0;
        it->second->entries = 0;
    }
}

void DriftAnalyzer::_clear_map_after_event() {
    for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
        it->second->sumEvent = 0;
    }
}

void DriftAnalyzer::_add_event_entries() {
    for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
        it->second->entries += 1;
    }
}

std::unordered_map < AFR_DetSignature, SumStruct * >::iterator
                    DriftAnalyzer::_add_new_detector_entry( AFR_DetSignature id ) {
    SumStruct * sumStruct = new SumStruct( id );
    _detMap->emplace( id, sumStruct );
    return _detMap->find( id );  // TODO return from emplace's pair
}

void DriftAnalyzer::_fill_histos_after_spill() {
    for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
        double meanEnergy = it->second->sumSpill/it->second->entries;
        it->second->sumSpillHist->AddBinContent( _currentSpill, meanEnergy );
    }
}

void DriftAnalyzer::_fill_histos_after_event() {
    for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
        it->second->sumEventHist->Fill( it->second->sumEvent );
    }
}
// TODO: uncomment after _V_process_event_payload will be implemented.
# if 1
StromaV_ANALYSIS_PROCESSOR_DEFINE( DriftAnalyzer, "driftAnalyzer" ) {
    return goo::dict::Dictionary( NULL,
            "Provides facility for PMT's drift analysis." );
}
# endif

}  // namespace na64
}  // namespace dprocessors

# endif  // if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

