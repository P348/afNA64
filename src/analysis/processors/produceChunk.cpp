/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/produceChunk.hpp"
# include "event.pb.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

namespace na64 {
namespace dprocessors {

ProduceChunk::ProduceChunk ( const std::string & pn ) :
               ExpEventProcessor ( pn ) {
    _oStream = new std::ofstream ("/tmp/testChunk.dat", std::ios::out | std::ios::binary );
}

ProduceChunk::~ProduceChunk () {
    _oStream->close();
    delete _oStream;
}

sV::aux::iEventProcessor::ProcRes
ProduceChunk::_V_process_event_payload( NA64ExperimentalEvent & event) {
    _oStream->write ( event.rawevent().c_str(), event.rawevent().size() );
    return RC_ACCOUNTED;
}


// TODO: apparently Bogdan adopted this in another branch...
# if 0
StromaV_DEFINE_DATA_PROCESSOR ( produceChunk ) {
    return new ProduceChunk ( "produceChunk" );
} StromaV_REGISTER_DATA_PROCESSOR ( produceChunk,
        "produceChunk",
        "Produce a small piece of data (e.g. for tests) from whole chunk.\
        Use with -n [ --max-events-to-read ] arg option to control # of events\
        Output file: /tmp/testChunk.dat" )
# endif

}  // namespace na64
}  // namespace dprocessors

# endif  // if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

