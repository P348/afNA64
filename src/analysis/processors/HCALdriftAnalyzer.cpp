/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/HCALdriftAnalyzer.hpp"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

namespace na64 {
namespace dprocessors {
namespace HCALdriftAnalyzer {

HCALdriftAnalyzer::HCALdriftAnalyzer( const std::string & pn ) :
               SADCProcessor ( pn ),
               _currentSpill (0),
               _spillNumberSetted (false),
               _eventProcessed (false) {
               // sV::mixins::DetectorCatalogue<DriftStatistics *>("Fitting") {
    AFR_DetFamID detectorsToAnalyze[] = { EnumScope::fam_HCAL };
    // _hcalTree = new TTree ("hcalDrift", "HCAL tree");
    _detSet = new std::set< AFR_DetFamID >( detectorsToAnalyze,
                                            detectorsToAnalyze +
                                            sizeof( detectorsToAnalyze )/
                                            sizeof( AFR_DetFamID ));
}

HCALdriftAnalyzer::~HCALdriftAnalyzer() {
    _drop_spill();
}

SpillStruct::SpillStruct( AFR_DetMjNo mjNo ) :
                            sumSpill (0),
                            rms      (0),
                            sumEvent (0),
                            entries  (0),
                            spillNo  (0) {
    char detNameBf[32];
    snprintf( detNameBf, 32, "%s",
                detector_name_by_code( (EnumScope::MajorDetectorsCode)mjNo ) );
    tree = new TTree ( detNameBf, detNameBf );
}

/* This method retrive detector id, check for existence of this detector
 * in the map. If it is available pushes new entry, if not creates new
 * entry.
 */
sV::aux::iEventProcessor::ProcRes
HCALdriftAnalyzer::_V_process_SADC_profile_event(
                                ::na64::events::SADC_profile & sadcProfile) {
    # if 1
    AFR_UniqueDetectorID detectorIDUnion( sadcProfile.detectorid());
    auto itSet = _detSet->find(
                    detector_family_num( detectorIDUnion.byNumber.major ) );
    if ( itSet != _detSet->end() ) {
        auto itMap = _detMap.find( detectorIDUnion.byNumber.major );
        if ( itMap != _detMap.end() ) {
            itMap->second->sumSpill += sadcProfile.edep();
            itMap->second->sumEvent += sadcProfile.edep();
        } else {
            auto itNewEntry =
                _add_new_detector_entry( detectorIDUnion.byNumber.major );
            itNewEntry->second->sumSpill += sadcProfile.edep();
            itNewEntry->second->sumEvent += sadcProfile.edep();
        }
    }
    # endif
    return RC_ACCOUNTED;
}

// Gross. Rework!
bool
HCALdriftAnalyzer::_drop_spill() {
    _fill_tree_after_spill();
    # if 0  // XXX
    for ( auto it = _detMap.begin(); it != _detMap.end(); it++ ) {
        std::cout << it->first << it->second;
    }
    # endif
    _clear_map_after_spill();
    return true;
}

sV::aux::iEventProcessor::ProcRes
HCALdriftAnalyzer::_V_process_event_payload( NA64ExperimentalEvent & event ) {
    // Call the parent's method.
    SADCProcessor::_V_process_event_payload( event );
    _eventProcessed = true;
    // Check begin of new spill. If a new spill starts do drop information
    // about previous spill. Check for map emptiness needs for first spill
    // when initial _currentSpill = 0 (!=1). In this case we dont need the drop
    // std::cout << "Spill number " << _currentSpill << " " << event->spillno() << std::endl;
    // std::cout.flush();
    if ( !_spillNumberSetted ) {
        _currentSpill = event.spillno();
        _spillNumberSetted = true;
    }

    if ( _currentSpill != event.spillno() ) {
        _drop_spill();
        _currentSpill = event.spillno();
        // XXX std::cout << "Spill number " << _currentSpill << " "
        // XXX           << event->spillno() << std::endl;
    }
    return RC_ACCOUNTED;
}

sV::aux::iEventProcessor::ProcRes
HCALdriftAnalyzer::_V_finalize_event_processing( Event & ) {
    if (_eventProcessed) {
        _add_event_entries();
        _eventProcessed = false;
    }
    return RC_ACCOUNTED;
}

void HCALdriftAnalyzer::_V_finalize() const {
    // _create_histos_after_processing();
}

# if 0
void HCALdriftAnalyzer::_create_histos_after_processing() const {
    for ( auto it = _detMap.begin(); it != _detMap.end(); it++ ) {
        char detNameBf[32], nameSpillBf[64], labelSpillBf[128];
        snprintf( detNameBf, 32, "%s",
                detector_name_by_code( (EnumScope::MajorDetectorsCode)(it->first) ) );
        snprintf( nameSpillBf, 64, "%s-bySpill", detNameBf );
        snprintf( labelSpillBf, 128, "Spill # vs %s sum energy;#spill;Energy", detNameBf );

        std::map<int, double> spillMapOrd( it->second->spillMap.begin(),
                                           it->second->spillMap.end() );
        std::cout << "XXX size min max " << spillMapOrd.size()
                  << " " << spillMapOrd.begin()->first
                  << " " << spillMapOrd.rbegin()->first <<std::endl;
        TH1F * spillHist = new TH1F ( nameSpillBf, labelSpillBf,
                                                   spillMapOrd.size(),
                                                   spillMapOrd.begin()->first,
                                                   spillMapOrd.rbegin()->first );
        std::cout << "XXX spill map" << std::endl;
        for ( auto itMapOrd = spillMapOrd.begin();
                   itMapOrd != spillMapOrd.end(); itMapOrd++ ) {
            std::cout << itMapOrd->first << " " << itMapOrd->second << std::endl;
            spillHist->SetBinContent( itMapOrd->first, itMapOrd->second );
        }
    }
}
# endif

void HCALdriftAnalyzer::_clear_map_after_spill() {
    for ( auto it = _detMap.begin(); it != _detMap.end(); it++ ) {
        it->second->sumSpill = 0;
        it->second->entries = 0;
    }
}

void HCALdriftAnalyzer::_add_event_entries() {
    for ( auto it = _detMap.begin(); it != _detMap.end(); it++ ) {
        SpillStruct & ss = *(it->second);
        ss.entries += 1;
        ss.edepsList.push_back( ss.sumEvent );
        ss.sumEvent = 0;
    }
}

std::unordered_map < AFR_DetMjNo, SpillStruct * >::iterator
                    HCALdriftAnalyzer::_add_new_detector_entry( AFR_DetMjNo mjNo ) {
    SpillStruct * sumStruct = new SpillStruct ( mjNo );
    sumStruct->tree->Branch ("sumSpill", &(sumStruct->sumSpill), "sumSpill/D" );
    sumStruct->tree->Branch ("rms",      &(sumStruct->rms),      "rms/D" );
    sumStruct->tree->Branch ("entries",  &(sumStruct->entries),  "entries/I" );
    sumStruct->tree->Branch ("spillNo",  &(sumStruct->spillNo),  "spillNo/I" );

    return (_detMap.emplace( mjNo, sumStruct )).first;
    // return _detMap.find( mjNo );  // TODO return from emplace's pair
}

void HCALdriftAnalyzer::_fill_tree_after_spill() {
    for ( auto it = _detMap.begin(); it != _detMap.end(); it++ ) {
        SpillStruct & ss = *(it->second);
        const std::list<double> & lst = ss.edepsList;

        ss.sumSpill /= ss.entries;
        ss.spillNo = _currentSpill;
        // calculate rms
        for ( auto itList = lst.begin();
                   itList != lst.end();
                   itList++ ) {
            ss.rms += pow ( *itList - ss.sumSpill, 2 );
            ss.rms = sqrt ( ss.rms/lst.size() );
        }
        it->second->tree->Fill();
    }
}

// TODO: apparently Bogdan adopted this in another branch...
# if 0
StromaV_DEFINE_DATA_PROCESSOR ( HCALdriftAnalyzer ) {
    return new HCALdriftAnalyzer ( "HCALdriftAnalyzer" );
} StromaV_REGISTER_DATA_PROCESSOR ( HCALdriftAnalyzer,
        "HCALdriftAnalyzer",
        "Provides facility for HCAL (by module) pmt's drift analysis." )  // TODO expand info
# endif

}  // namespace HCALdriftAnalyzer
}  // namespace na64
}  // namespace dprocessors

# endif  // if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

