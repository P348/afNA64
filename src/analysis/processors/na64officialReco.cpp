/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES) && \
                               defined(AFNA64_RECO_FOUND)

# include "analysis/processors/na64officialReco.hpp"
# include "StromaV/detector_ids.h"

# include <goo_dict/parameters/path_parameter.hpp>

namespace na64 {
namespace dprocessors {

OfficialReco::OfficialReco (const std::string & pn, const std::string & mapDir) :
              ExpEventProcessor( pn ) {
    // create DaqEventsManager. We need it for RunP348Reco function.
    // For RunP348Reco_single_event we still need manager for access to chip
    // digits.
    _daqManager = new CS::DaqEventsManager();
    _daqManager->SetMapsDir(mapDir);
    _lastEventRunNumber = -1;

    _outfile = new std::ofstream ("/tmp/testChunk.dat", std::ofstream::binary);  // XXX
}

OfficialReco::OfficialReco( const goo::dict::Dictionary & dct ) :
            OfficialReco( "officialReco", dct["map-dir"].as<goo::filesystem::Path>() ) {}

OfficialReco::~OfficialReco() {
    _outfile->close();  // XXX
    delete _daqManager;
}

sV::aux::iEventProcessor::ProcRes
OfficialReco::_V_process_event_payload(NA64ExperimentalEvent & expPayload) {
    // get raw event and provide it to DaqManager. Now we have to store
    // this raw event simultaneously
    // with proto event consists the same event in gpb form. 10.02.1017
    _daqManager->SetDaqEvent(
            // Crutch:
            const_cast<CS::uint8*>(
                reinterpret_cast<const CS::uint8*>(expPayload.rawevent().c_str())
            ) );
    _outfile->write (expPayload.rawevent().c_str(), expPayload.rawevent().size() );  // XXX
    if( _daqManager->GetEvent().GetBuffer()!=NULL &&
        _daqManager->GetEvent().GetRunNumber() != _lastEventRunNumber ) {
            _lastEventRunNumber = _daqManager->GetEvent().GetRunNumber();
            _daqManager->GetEvent1Run().NewRun( _lastEventRunNumber );
            _daqManager->GetMaps().Clear();
            _daqManager->GetDaqOptions().Clear();
            // Crutch:
            auto & detectorsAll = const_cast<std::vector<std::string> &>(
                                                _daqManager->GetDetectors());
            detectorsAll.clear();
            if( !_daqManager->GetMapsDir().empty() ) {
                if( !CS::Chip::ReadMaps(
                            _lastEventRunNumber,
                            _daqManager->GetMapsDir(),
                            _daqManager->GetMaps(),
                            _daqManager->GetDaqOptions(),
                            detectorsAll ) ) {
                    _TODO_  // TODO: warning
                }
            } else {
                _TODO_  // TODO: badParameter exception
            }
    }
    _daqManager->GetEvent().SetEvent1Run( &_daqManager->GetEvent1Run() );

    if ( !_daqManager->DecodeEvent() ) {
        std::cerr << "decode procedure - failed!" << std::endl;  // TODO: warning
        return true;
    }
    //for( auto & digitPair : _daqManager->GetEventDigits() ) {
    //    digitPair.second->Print();
    //}
    const RecoEvent recoEvent = RunP348Reco_single_event(
                                            _daqManager->GetEvent(),
                                            _daqManager->GetEventDigits());
    // Process reco event: flag correction in proto event (from reco event we
    // can obtain information about LED and random triggered events), expantion
    // of the SADC_profiles in proto event with time, amplitude, energy etc.
    _process_reco_event( recoEvent, expPayload );
    // TODO ??? ExpEventProcessor::_V_process_event_payload( expPayload );

    // Any GetXXX() method will extract
    // appropritiate data from the buffer. SetDaqEvent does not copy information
    // from the buffer.
    return RC_CORRECTED;
}

void OfficialReco::_process_reco_event( const RecoEvent & recoEvent,
                                        NA64ExperimentalEvent & event) {
    // flag correction in proto event
    if( recoEvent.isCalibration ) {
        // XXX
        //std::cout << "event type: " <<
        //    events::ExperimentalEvent_Payload_NonPhysEventType_Name(event->nonphyseventtype())
        //    << std::endl;
        // Actually event already has nonphys type (Calibration) for
        // run number < 630. But we need flag cooreection for runs < 630.
        event.set_is_physical(false);
        event.set_nonphyseventtype(
            ::na64::events::ExperimentalEvent_Payload_NonPhysEventType_Calibration);
        //std::cout << "LED event" << std::endl;  // XXX
    }
    if( recoEvent.isRandom ) {
        event.set_is_physical(false);
        event.set_nonphyseventtype(
            ::na64::events::ExperimentalEvent_Payload_NonPhysEventType_RandomTriggered);
        //XXX std::cout << "Rnd triggered event" << std::endl;
    }
    for ( int sadc = 0; sadc < event.sadc_data_size(); sadc++ ) {
        na64::events::SADC_profile * sadcProfile = event.mutable_sadc_data(sadc);
        AFR_UniqueDetectorID detectorIDUnion( sadcProfile->detectorid());
        // if( EnumScope::fam_ECAL == detector_family_num( detectorIDUnion.byNumber.major ) ) {
        //    std::cout << "XXX ECAL located"
        //}  // XXX
        int x = detector_get_x_idx(detectorIDUnion);
        int y = detector_get_y_idx(detectorIDUnion);
        if ( EnumScope::d_ECAL0 == detectorIDUnion.byNumber.major ) {
            sadcProfile->set_edep(recoEvent.ECAL[0][x][y].energy);
            sadcProfile->set_time(recoEvent.ECAL[0][x][y].t0);
        }
        else if ( EnumScope::d_ECAL1 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.ECAL[1][x][y].energy);
                sadcProfile->set_time(recoEvent.ECAL[1][x][y].t0);
                // XXX
                //std::cout << "ECAL1 set edep " << recoEvent.ECAL[1][x][y].energy << std::endl;
        }
        else if ( EnumScope::d_HCAL0 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.HCAL[0][x][y].energy);
                sadcProfile->set_time(recoEvent.HCAL[0][x][y].t0);
            }
        else if ( EnumScope::d_HCAL1 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.HCAL[1][x][y].energy);
                sadcProfile->set_time(recoEvent.HCAL[1][x][y].t0);
        }
        else if ( EnumScope::d_HCAL2 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.HCAL[2][x][y].energy);
                sadcProfile->set_time(recoEvent.HCAL[2][x][y].t0);
        }
        else if ( EnumScope::d_HCAL3 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.HCAL[3][x][y].energy);
                sadcProfile->set_time(recoEvent.HCAL[3][x][y].t0);
        }
        else if ( EnumScope::d_MUON0 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.MUON[0].energy);
                sadcProfile->set_time(recoEvent.MUON[0].t0);
        }
        else if ( EnumScope::d_MUON1 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.MUON[1].energy);
                sadcProfile->set_time(recoEvent.MUON[1].t0);
        }
        else if ( EnumScope::d_MUON2 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.MUON[2].energy);
                sadcProfile->set_time(recoEvent.MUON[2].t0);
        }
        else if ( EnumScope::d_MUON3 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.MUON[3].energy);
                sadcProfile->set_time(recoEvent.MUON[3].t0);
        }
        else if ( EnumScope::d_S1 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.S1.energy);
                sadcProfile->set_time(recoEvent.S1.t0);
        }
        else if ( EnumScope::d_S2 == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.S2.energy);
                sadcProfile->set_time(recoEvent.S2.t0);
        }
        else if ( EnumScope::d_SRD == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.SRD[x].energy);
                sadcProfile->set_time(recoEvent.SRD[x].t0);
        }
        else if ( EnumScope::d_VETO == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.VETO[x].energy);
                sadcProfile->set_time(recoEvent.VETO[x].t0);
        }
        else if ( EnumScope::d_WCAL == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.WCAL.energy);
                sadcProfile->set_time(recoEvent.WCAL.t0);
        }
        else if ( EnumScope::d_VTEC == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.VTEC.energy);
                sadcProfile->set_time(recoEvent.VTEC.t0);
        }
        else if ( EnumScope::d_VTWC == detectorIDUnion.byNumber.major ) {
                sadcProfile->set_edep(recoEvent.VTWC.energy);
                sadcProfile->set_time(recoEvent.VTWC.t0);
        }
        // XXX
        // std::cout << detName << ", x = " << x << ", y = " << y << std::endl;
    }

}

StromaV_ANALYSIS_PROCESSOR_DEFINE_MCONF( OfficialReco, "officialReco" ) {
    goo::dict::Dictionary ofReco( "officialReco",
        "Processor forwarding \"official\" NA64 reconstruction procedure." );
    ofReco.insertion_proxy()
        .p<goo::filesystem::Path>( "map-dir",
                "Directory with DDD XML documents describing detector mapping." )
        ;
    goo::dict::DictionaryInjectionMap injM;
        injM( "map-dir",            "analysis.data-sources.na64.ddd.map-dir" )
        ;
    return std::make_pair( ofReco, injM );
}

}  // namespace dprocessors
}  // namespace na64


# endif  // defined(RPC_PROTOCOLS) && defined(ANALYSIS_ROUTINES) ...

