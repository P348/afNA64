/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/ECALsum.hpp"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

namespace na64 {
namespace dprocessors {
namespace ECALsum {

ECALsum::ECALsum( const std::string & pn ) :
               SADCProcessor( pn ),
               _currentSpill(0),
               _spillNumberSetted(false) {
               // sV::mixins::DetectorCatalogue<DriftStatistics *>("Fitting") {
    AFR_DetFamID detectorsToAnalyze[] = { EnumScope::fam_ECAL };
    _detSet = new std::set< AFR_DetFamID >( detectorsToAnalyze,
                                            detectorsToAnalyze +
                                            sizeof( detectorsToAnalyze )/
                                            sizeof( AFR_DetFamID ));
    _sumStruct = new SumStruct();
}

SumStruct::SumStruct () : sumEvent(0) {
    sumHist = new TH1F ( "totalEdep",
                        "ECAL, energy vs # of events;energy, GeV;# of events",
                        4000, 0, 400 );
}

ECALsum::~ECALsum() {
}

/* This method retrive detector id, check for existence of this detector
 * in the map. If it is available pushes new entry, if not creates new
 * entry.
 */
sV::aux::iEventProcessor::ProcRes
ECALsum::_V_process_SADC_profile_event(
                                ::na64::events::SADC_profile & sadcProfile) {
    if ( sadcProfile.edep() > 0 ) {
        # if 1
        AFR_UniqueDetectorID detectorIDUnion( sadcProfile.detectorid() );
        // TODO selecter?
        auto itSet = _detSet->find(
                        detector_family_num( detectorIDUnion.byNumber.major ) );
        if ( itSet != _detSet->end() &&
             ( EnumScope::d_ECAL0 == detectorIDUnion.byNumber.major ||
               EnumScope::d_ECAL1 == detectorIDUnion.byNumber.major ) ) {
            _sumStruct->sumEvent += sadcProfile.edep();
            // XXX:
            auto insertionResult = _sumStruct->nDigits.emplace(
                    sadcProfile.detectorid(), sadcProfile.edep());
            if( !insertionResult.second ) {
                insertionResult.first->second += sadcProfile.edep();
            }
            // XXX
        }
        # endif
    }
    return RC_ACCOUNTED;
}

sV::aux::iEventProcessor::ProcRes
ECALsum::_V_process_event_payload( NA64ExperimentalEvent & event ) {
    // Call the parent's method.
    return SADCProcessor::_V_process_event_payload( event );
}

sV::aux::iEventProcessor::ProcRes
ECALsum::_V_finalize_event_processing( Event & ) {
    _fill_histos_after_event();
    _clear_struct_after_event();
    return RC_ACCOUNTED;
}

void ECALsum::_V_finalize() const {
    # if 0
    for( const auto & p : _sumStruct->nDigits ) {
        AFR_UniqueDetectorID detectorIDUnion( p.first );
        char fullName[32];
        snprintf_detector_name( fullName, 32, detectorIDUnion );
        //std::string name(fullName);
        std::cout << "  XXX " << fullName << " : "
                  << std::dec << p.second << std::endl;
    }
    # endif
}

void ECALsum::_clear_struct_after_event() {
    _sumStruct->sumEvent = 0;}

void ECALsum::_fill_histos_after_event() {
    _sumStruct->sumHist->Fill( _sumStruct->sumEvent );
}

// TODO: apparently Bogdan adopted this in another branch...
# if 0
StromaV_DEFINE_DATA_PROCESSOR ( ECALsum ) {
    return new ECALsum ( "ECALsum" );
} StromaV_REGISTER_DATA_PROCESSOR ( ECALsum,
        "ECALsum",
        "Provide energy distribution for ECAL (full)." )  // TODO expand info
# endif

}  // namespace ECALsum
}  // namespace na64
}  // namespace dprocessors

# endif  // if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

