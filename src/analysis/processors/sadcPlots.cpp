/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

# include "na64_detector_ids.h"
# include "na64_uevent.hpp"
# include "analysis/event_interfaces.hpp"

# include <StromaV/app/cvalidators.hpp>
# include <StromaV/app/analysis.hpp>
# include <StromaV/app/analysis_cat_mx.tcc>

# include <TH1F.h>
# include <TH2F.h>
# include <TFile.h>
# include <TDirectory.h>
# include <TProfile.h>
# include <TSystem.h>

namespace na64 {
namespace dprocessors {
namespace aux {

class SADC_WFPlotsCollector : public na64::analysis::iSADCProcessor,
                              public sV::mixins::DetectorCatalogue< std::pair<TProfile *, TH2F *> > {
protected:
    virtual void _V_print_brief_summary( std::ostream & os ) const override;
    virtual ProcRes _V_process_SADC_profile_event( na64::events::SADC_profile & ) override;
    std::pair<TProfile *, TH2F *> _V_new_entry( AFR_DetSignature id, TDirectory * ) override;
    void _V_free_entry( AFR_DetSignature, std::pair<TProfile *, TH2F *> ) override;

    const sV::aux::HistogramParameters2D _sadcHstPars;
    bool _doCollectStats;
public:
    SADC_WFPlotsCollector(
                    const std::string & pn,
                    const sV::aux::HistogramParameters2D & sadcHstPars,
                    const std::string & dirName="SADCProfiles" ) :
                na64::analysis::iSADCProcessor(pn),
                sV::mixins::DetectorCatalogue< std::pair<TProfile *, TH2F *> >(dirName),
                _sadcHstPars(sadcHstPars),
                _doCollectStats(sadcHstPars.nBins[0]) {}
    SADC_WFPlotsCollector( const goo::dict::Dictionary & dct ) :
        SADC_WFPlotsCollector( "SADC-Waveform-plots",
                dct["store-ampls-histogram"].as<sV::aux::HistogramParameters2D>() ) {}
};


std::pair<TProfile *, TH2F *>
SADC_WFPlotsCollector::_V_new_entry( AFR_DetSignature id, TDirectory * ) {
    std::pair<TProfile *, TH2F *> res;
    {
        AFR_UniqueDetectorID uID(id);
        char detNamebf[32], namebf[64], labelbf[128];
        snprintf_detector_name( detNamebf, 32, uID );
        snprintf( namebf,  64, "%s-profile", detNamebf );
        snprintf( labelbf, 128, "SADC profile for %s detector", detNamebf );
        res.first = new TProfile(
                namebf,
                labelbf,
                _sadcHstPars.nBins[0],
                _sadcHstPars.min[0],
                _sadcHstPars.max[0] );
        snprintf( namebf,  64, "%s-dst-profile", detNamebf );
        snprintf( labelbf, 128, "SADC distribution profile for %s detector", detNamebf );
        res.second = new TH2F(
                namebf,
                labelbf,
                _sadcHstPars.nBins[0],
                _sadcHstPars.min[0],
                _sadcHstPars.max[0],
                _sadcHstPars.nBins[1],
                _sadcHstPars.min[1],
                _sadcHstPars.max[1]
                );
    }
    return res;
}

void
SADC_WFPlotsCollector::_V_free_entry( AFR_DetSignature, std::pair<TProfile *, TH2F *> /*th*/ ) {
    // ... TODO
}

void
SADC_WFPlotsCollector::_V_print_brief_summary( std::ostream & ) const {
    // ... TODO
}

sV::aux::iEventProcessor::ProcRes
SADC_WFPlotsCollector::_V_process_SADC_profile_event( na64::events::SADC_profile & sadcProf ) {
    std::pair<TProfile *, TH2F *> & tPair = consider_entry( sadcProf.detectorid() );
    for( UByte i = 0; i < 32; ++i ) {
        tPair.first->Fill(i,  sadcProf.samples( i ));
        tPair.second->Fill(i, sadcProf.samples( i ));
    }
    return RC_ACCOUNTED;
}

StromaV_ANALYSIS_PROCESSOR_DEFINE_MCONF( SADC_WFPlotsCollector, "sadcProfiles" ) {
    goo::dict::Dictionary sadcPlotsConf( "sadcProfiles",
        "Collects waveform distribution samples from ADC detectors." );
    sadcPlotsConf.insertion_proxy()
        .p<sV::aux::HistogramParameters2D>( "store-ampls-histogram",
            "Amplitude binning boundaries for time-vs-SADC histogram.",
            sV::aux::HistogramParameters2D(100, 0, 5000, 100, 0, 500) )
        ;
    goo::dict::DictionaryInjectionMap injM;
        injM( "store-ampls-histogram",            "analysis.processors.na64.SADC.stats.store-ampls-histogram" )
        ;
    return std::make_pair( sadcPlotsConf, injM );
}

}  // namespace aux
}  // namespace dprocessors
}  // namespace na64

# endif  // defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

