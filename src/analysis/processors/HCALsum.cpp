/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/HCALsum.hpp"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

namespace na64 {
namespace dprocessors {
namespace HCALsum {

HCALsum::HCALsum ( const std::string & pn ) :
               SADCProcessor ( pn ),
               _currentSpill (0),
               _spillNumberSetted (false),
               _eventProcessed (false) {
               // sV::mixins::DetectorCatalogue<DriftStatistics *>("Fitting") {
    AFR_DetFamID detectorsToAnalyze[] = { EnumScope::fam_HCAL };
    _detMap = new std::unordered_map< AFR_DetMjNo, SumStruct * >();
    _detSet = new std::set< AFR_DetFamID >( detectorsToAnalyze,
                                            detectorsToAnalyze +
                                            sizeof( detectorsToAnalyze )/
                                            sizeof( AFR_DetFamID ));
}

HCALsum::~HCALsum() {
}

SumStruct::SumStruct( AFR_DetMjNo mjNo ) : sumEvent(0) {
    char detNameBf[32], nameEventBf[64], labelEventBf[128];
    snprintf( detNameBf, 32, "%s", detector_name_by_code( (EnumScope::MajorDetectorsCode)mjNo ) );
    snprintf( nameEventBf, 64, "%s-total", detNameBf );
    snprintf( labelEventBf, 128,
                "%s energy vs # of events;energy, GeV;# of events", detNameBf );
    sumEventHist = new TH1F ( nameEventBf, labelEventBf, 500, 0, 500 );
}


/* This method retrive detector id, check for existence of this detector
 * in the map. If it is available pushes new entry, if not creates new
 * entry.
 */
sV::aux::iEventProcessor::ProcRes
HCALsum::_V_process_SADC_profile_event
                            ( ::na64::events::SADC_profile & sadcProfile) {
    # if 1
        AFR_UniqueDetectorID detectorIDUnion( sadcProfile.detectorid());

        auto itSet = _detSet->find(
                        detector_family_num( detectorIDUnion.byNumber.major ) );
        if ( itSet != _detSet->end() ) {
            auto itMap = _detMap->find( detectorIDUnion.byNumber.major );
            if ( itMap != _detMap->end() ) {
                itMap->second->sumEvent += sadcProfile.edep();
            } else {
                auto itNewEntry =
                    _add_new_detector_entry( detectorIDUnion.byNumber.major );
                itNewEntry->second->sumEvent += sadcProfile.edep();
            }
        }
    # endif
    return RC_ACCOUNTED;
}

sV::aux::iEventProcessor::ProcRes
HCALsum::_V_process_event_payload( NA64ExperimentalEvent & event ) {
    _eventProcessed = true;
    // Call the parent's method.
    return SADCProcessor::_V_process_event_payload( event );
}

sV::aux::iEventProcessor::ProcRes
HCALsum::_V_finalize_event_processing( Event & ) {
    if ( _eventProcessed ) {
        _fill_histos_after_event();
        _clear_map_after_event();
        _eventProcessed = false;
    }
    return RC_ACCOUNTED;
}

void HCALsum::_V_finalize() const {}

void HCALsum::_clear_map_after_event() {
    for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
        it->second->sumEvent = 0;
    }
}

std::unordered_map < AFR_DetMjNo, SumStruct * >::iterator
                    HCALsum::_add_new_detector_entry( AFR_DetMjNo mjNo ) {
    SumStruct * sumStruct = new SumStruct( mjNo );
    _detMap->emplace( mjNo, sumStruct );
    return _detMap->find( mjNo );  // TODO return from emplace's pair
}

void HCALsum::_fill_histos_after_event() {
    for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
        it->second->sumEventHist->Fill( it->second->sumEvent );
    }
}

// TODO: apparently Bogdan adopted this in another branch...
# if 0
StromaV_DEFINE_DATA_PROCESSOR ( HCALsum ) {
    return new HCALsum ( "HCALsum" );
} StromaV_REGISTER_DATA_PROCESSOR ( HCALsum,
        "HCALsum",
        "Provide energy distribution for HCAL (for each module)." )  // TODO expand info
# endif

}  // namespace HCALsum
}  // namespace na64
}  // namespace dprocessors

# endif  // if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

