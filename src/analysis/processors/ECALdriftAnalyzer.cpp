/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/processors/ECALdriftAnalyzer.hpp"

# if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

namespace na64 {
namespace dprocessors {
namespace ECALdriftAnalyzer {

ECALdriftAnalyzer::ECALdriftAnalyzer( const std::string & pn ) :
               SADCProcessor( pn ),
               _currentSpill(0),
               _spillNumberSetted(false) {
               // sV::mixins::DetectorCatalogue<DriftStatistics *>("Fitting") {
    AFR_DetFamID detectorsToAnalyze[] = { EnumScope::fam_ECAL };
    _detSet = new std::set< AFR_DetFamID >( detectorsToAnalyze,
                                            detectorsToAnalyze +
                                            sizeof( detectorsToAnalyze )/
                                            sizeof( AFR_DetFamID ));
    _sumStruct = new SumStruct();
    _ecalTree = new TTree ("ecalDrift", "ECAL tree");
    _ecalTree->Branch ("sumSpill", &_sumStruct->sumSpill, "sumSpill/D" );
    _ecalTree->Branch ("entries", &_sumStruct->entries, "entries/I" );
    _ecalTree->Branch ("spillNo", &_sumStruct->spillNo, "spillNo/I" );
}

SumStruct::SumStruct () : sumSpill(0),
                          entries(0) {
}

ECALdriftAnalyzer::~ECALdriftAnalyzer() {
}

/* This method retrive detector id, check for existence of this detector
 * in the map. If it is available pushes new entry, if not creates new
 * entry.
 */
sV::aux::iEventProcessor::ProcRes
ECALdriftAnalyzer::_V_process_SADC_profile_event(
                                ::na64::events::SADC_profile & sadcProfile) {
    # if 1
        AFR_UniqueDetectorID detectorIDUnion( sadcProfile.detectorid());
        // TODO selecter?
        auto itSet = _detSet->find(
                        detector_family_num( detectorIDUnion.byNumber.major ) );
        if ( itSet != _detSet->end() &&
             ( EnumScope::d_ECAL0 == detectorIDUnion.byNumber.major ||
               EnumScope::d_ECAL1 == detectorIDUnion.byNumber.major) ) {
            _sumStruct->sumSpill += sadcProfile.edep();
        }
    # endif
    return RC_ACCOUNTED;
}

// Gross. Rework!
bool ECALdriftAnalyzer::_drop_spill() {
    _fill_tree_after_spill();
# if 0  // XXX
for ( auto it = _detMap->begin(); it != _detMap->end(); it++ ) {
    std::cout << it->first << it->second;
    }
# endif
    _clear_struct_after_spill();
    return true;
}

sV::aux::iEventProcessor::ProcRes
ECALdriftAnalyzer::_V_process_event_payload( NA64ExperimentalEvent & event ) {
    // Call the parent's method.
    SADCProcessor::_V_process_event_payload( event );
    // Check begin of new spill. If a new spill starts do drop information
    // about previous spill. Check for map emptiness needs for first spill
    // when initial _currentSpill = 0 (!=1). In this case we dont need the drop
    // std::cout << "Spill number " << _currentSpill << " " << event->spillno() << std::endl;
    // std::cout.flush();
    if ( !_spillNumberSetted ) {
        _currentSpill = event.spillno();
        _spillNumberSetted = true;
    }

    if ( _currentSpill != event.spillno() ) {
        _drop_spill();
        _currentSpill = event.spillno();
        // XXX std::cout << "Spill number " << _currentSpill << " "
        // XXX           << event->spillno() << std::endl;
    }
    return true;
}

sV::aux::iEventProcessor::ProcRes
ECALdriftAnalyzer::_V_finalize_event_processing( Event & ) {
    _add_event_entries();
    return RC_ACCOUNTED;
}

void ECALdriftAnalyzer::_V_finalize() const {
}

/*
void ECALdriftAnalyzer::_create_histos_after_processing() const {
    std::map<int, double> spillMapOrd( _sumStruct->spillMap.begin(),
                                       _sumStruct->spillMap.end() );
    TH1F * spillHist = new TH1F ( "totalSpill",
                           "ECAL, spill# vs mean energy;#spill;mean energy",
                           spillMapOrd.size(),
                           spillMapOrd.begin()->first,
                           spillMapOrd.end()->first );
    for ( auto itMapOrd = spillMapOrd.begin();
               itMapOrd != spillMapOrd.end(); itMapOrd++ ) {
        spillHist->AddBinContent( itMapOrd->first, itMapOrd->second );
    }
}
*/

void ECALdriftAnalyzer::_clear_struct_after_spill() {
    _sumStruct->sumSpill = 0;
    _sumStruct->entries = 0;
}

void ECALdriftAnalyzer::_add_event_entries() {
    _sumStruct->entries += 1;
}

void ECALdriftAnalyzer::_fill_tree_after_spill() {
    _sumStruct->sumSpill /= _sumStruct->entries;
    _sumStruct->spillNo = _currentSpill;
    _ecalTree->Fill();
}

// TODO: apparently Bogdan adopted this in another branch...
# if 0
StromaV_DEFINE_DATA_PROCESSOR ( ECALdriftAnalyzer ) {
    return new ECALdriftAnalyzer ( "ECALdriftAnalyzer" );
} StromaV_REGISTER_DATA_PROCESSOR ( ECALdriftAnalyzer,
        "ECALdriftAnalyzer",
        "Provides facility for ECAL(sum) pmt's drift analysis." )  // TODO expand info
# endif

}  // namespace ECALdriftAnalyzer
}  // namespace na64
}  // namespace dprocessors

# endif  // if defined(StromaV_RPC_PROTOCOLS) && defined(StromaV_ANALYSIS_ROUTINES)

