/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "afNA64_config.h"

# if defined(StromaV_RPC_PROTOCOLS) && defined(na64ee_SUPPORT_DDD_FORMAT)

# include "analysis/dsources/ddd.hpp"

# include <goo_exception.hpp>
# include <goo_dict/parameters/path_parameter.hpp>
# include <StromaV/utils.h>

# include "uevent.hpp"

# include <Chip.h>
# include <ChipAPV.h>   // MM, GEM
# include <ChipSADC.h>  // Calorimeters
# include <ChipF1.h>    // Straw

# include <goo_dict/dict.hpp>

# include "analysis/apvConstraints.h"

# include <fstream>

namespace afNA64 {
namespace dsources {
namespace ddd {

using ::sV::AbstractApplication;

namespace aux {

EventsTranscoder::EventsTranscoder( CS::DaqEventsManager & extMngrPtr, bool keepRaw ) :
            EventsTranscoder( &extMngrPtr, false, keepRaw ) { }

EventsTranscoder::EventsTranscoder( const std::string & mapsDirPath, bool keepRaw ) :
            EventsTranscoder( new CS::DaqEventsManager(
                        /* name ............. */"afNA64-reader",
                        /* signal handler ... */ false ), true,
            keepRaw ) {
    if( mapsDirPath.empty() ) {
        emraise( badParameter, "Map file dir provided path string is empty." );
    }
    get_ddd_manager().SetMapsDir( mapsDirPath );
}

EventsTranscoder::~EventsTranscoder() {
    if( _ownsManager && _managerPtr ) {
        delete _managerPtr;
    }
    _managerPtr = nullptr;
}

void
EventsTranscoder::reset_counters() {
    _eventsNonPhysicalOmitted
        = _eventsNonPhysicalProcessed
        = _eventsDecodingFailureOmitted
        = _digitsProcessed
        = _digitsIgnored
        = _digitsDataTranslationUnimplemented
        = 0;
}

bool
EventsTranscoder::transcode( Event *& eventPtr,
                             CS::DaqEvent & event ) {
    // TODO: DaqEventsManager pisses of when we make him deal with detectors
    // mapping while no events read. Have to use it with care...
    if( !_detectorsTableValidated ) {
        // get mapping structure
        set_up_detector_mapping();
        if( goo::app<::sV::AnalysisApplication>().verbosity() > 1 ) {
            dump_mapping_table( stdout );
            test_detector_table( stdout );
        }
    }
    // Turn uni-event pointed by eventPtr to experimental one by obtaining
    // pointer to its experimental instance (oneof GPB qualifier):

    //::sV::events::ExperimentalEvent & expEve_ = *(eventPtr->mutable_experimental());
    //::na64::events::ExperimentalEvent_Payload expEve;  // XXX
    //# warning "need template caches at pipeline level"
    //_TODO_  // ^^^ TODO: template caches at pipeline level
    ExpEventPayload & expEve = experimental_payload_ref();
    expEve.Clear();
    if( _keepRaw ) {
        expEve.set_rawevent( event.GetBuffer(), event.GetLength() );
    }
    if( CS::DaqEvent::PHYSICS_EVENT != event.GetType() ) {
        ++_eventsNonPhysicalProcessed;
        if( !_treatNonPhysical ) {
            ++_eventsNonPhysicalOmitted;
            return false;  // select only events corresponds to real physics
        } else {
            if( !_transcode_non_physical( eventPtr, event ) ) {
                ++_eventsNonPhysicalOmitted;
                return false;
            }
        }
    } else {
        expEve.set_is_physical( true );
    }
    CS::DaqEventsManager & m = get_ddd_manager();
    // Should be called before any real data acquizition
    // of event (however, may appear after determining event type).
    if( !m.DecodeEvent() ) {
        ++_eventsDecodingFailureOmitted;
        return false;  // event decoding failure
    }
    //
    for( auto it  = m.GetEventDigits().begin();
              it != m.GetEventDigits().end();
              ++it, ++_digitsProcessed ) {
        const CS::Chip::Digit * d = it->second;
        auto dddCode = (EnumScope::DDD_DetectorsCode) d->GetDetID().GetNumber();
        auto detectorMjID = det_code_ddd_to_na64( dddCode );
        if( !detectorMjID ) {
            // unresolved detector.
            ++_digitsDataTranslationUnimplemented;
            continue;
        }
        // Try to found detector id in translation dictionary:
        if( _detIDMap.find( d->GetDetID().GetNumber() ) == _detIDMap.end() ) { // if not found:
            // Obtain and remember name (for further debug):
            _detIDMap[d->GetDetID().GetNumber()] = d->GetDetID().GetName();
            if( _doDetectorNameValidation ) { // if det. table needs to be validated:
                const std::string supposedName( ddd_detector_name_by_code( dddCode ) );
                if( d->GetDetID().GetName() != supposedName ) {
                    emraise( corruption,
                        "For DDD detector code %x expected name is \"%s\" while real is \"%s\" "
                        "Det. table needs correction.",
                        d->GetDetID().GetNumber(),
                        supposedName.c_str(),
                        d->GetDetID().GetName().c_str());
                }
            }
            _detTranslationDict[d->GetDetID().GetNumber()] = detectorMjID;
        }

        if( detector_is_SADC(detectorMjID) ) {  // if SADC feature is on for this detector:
            if( do_omit_chip( eChipSADC ) ) {
                ++_digitsIgnored;
                continue;
            }
            const CS::ChipSADC::Digit * sadc = static_cast<const CS::ChipSADC::Digit*>(d);
            //                          ^^^^- this is a digit from
            //                          sampling ADC front-end (calorimeters)
            //                          following is the description of the
            //                          data available in the digit:
            // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipSADC.h#L150
            AFR_UniqueDetectorID did =
                    compose_cell_identifier(detectorMjID, sadc->GetX(), sadc->GetY());
            auto newSADCData = expEve.add_sadc_data();
            if(     _doDetectorNameValidation
                && !detector_is_Subd( detectorMjID )
                && ( sadc->GetX() || sadc->GetY() )) {
                emraise( corruption,
                    "Found x=%d/y=%d for detector %s which is not supposed to have transversal "
                    "segmentatation according to afNA64's internal detector table. Table needs "
                    "correction (internal name: %s, native code 0x%x, mapped mj. code 0x%x).",
                    sadc->GetX(),
                    sadc->GetY(),
                    d->GetDetID().GetName().c_str(),
                    detector_name_by_code( detectorMjID ),
                    d->GetDetID().GetNumber(),
                    det_code_ddd_to_na64( (EnumScope::DDD_DetectorsCode) d->GetDetID().GetNumber() ) );
            }
            const std::vector<CS::uint16>& wave = sadc->GetSamples();
            for( size_t i = 0; i < wave.size(); ++i ) {
                newSADCData->add_samples( wave[i] );
            }
            newSADCData->set_detectorid( did.wholenum );
            assert( newSADCData->IsInitialized() );
            // TODO: check, if this major corresponds to d_TRIG and ...
            // and mark the whole event as LED.
        } else if( detector_is_APV(detectorMjID) ) {
            if( do_omit_chip( eChipAPV ) ) {
                ++_digitsIgnored;
                continue;
            }
            const CS::ChipAPV::Digit * apv = static_cast<const CS::ChipAPV::Digit*>(d);
            //                         ^^^- this is a digit from APV front-end (Micromegas, GEMs)
            // following is the description of the data available in the digit
            // https://gitlab.cern.ch/P348/p348-daq/blob/master/coral/src/DaqDataDecoding/src/ChipAPV.h#L50
            // APV is analog multiplexor chip originally developed in CMS experiment.
            // APV readout have 3 samples

            // In the micromegas several strips from entire set are usually
            // unioned into one wire conducting current to APV chip.
            const CS::uint16 wireNo = apv->GetChannel();
            const CS::uint32 * rawQ = apv->GetAmplitude();

            AFR_UniqueDetectorID did(0);
            did.byNumber.major = detectorMjID;

            auto newAPVData = expEve.add_apv_data();
            newAPVData->set_detectorid( did.wholenum );
            auto apvRawData = newAPVData->add_rawdata();
            apvRawData->set_wireno( wireNo );
            // APV always produces 3 sample
            for( uint8_t i = 0; i < APV_NSamples; ++i ) {
                apvRawData->add_amplitudesamples( rawQ[i] );
            }
            assert( newAPVData->IsInitialized() );
        } else {
            ++_digitsDataTranslationUnimplemented;
        }
    }
    {  // Filling basic experimental event header
        const std::pair<time_t, uint32_t> time  = event.GetTime();
        const uint32_t spillNo                  = event.GetBurstNumber(),
                       runNo                    = event.GetRunNumber(),
                       evNoInRun                = event.GetEventNumberInRun(),
                       evNoInSpill              = event.GetEventNumberInBurst(),
                       trigger                  = event.GetTrigger()
                       ;
        uint32_t triggerNo, errorCode;
        try {
            triggerNo = event.GetTriggerNumber();
        } catch ( const char * eCStr ) {
            // Apparently "DaqEvent::Header::GetTriggerNumber(): no information.":
            triggerNo = UINT32_MAX;
        }
        try {
            errorCode = event.GetErrorCode();
        } catch ( const char * eCStr ) {
            // Apparently "DaqEvent::Header::GetErrorCode(): no information.":
            errorCode = UINT32_MAX;
        }
        expEve.set_spillno(     spillNo );
        expEve.set_runno(       runNo );
        expEve.set_evnoinrun(   evNoInRun );
        expEve.set_evnoinspill( evNoInSpill );
        expEve.set_triggerno(   triggerNo );
        expEve.set_errorcode(   errorCode );
        expEve.set_trigger(     trigger );
        expEve.set_timestruct( (void *) &(time.first), sizeof(time_t) );
        expEve.set_timenum( time.second );
    }
    eventPtr
        ->mutable_experimental()
        ->mutable_payload()
        ->PackFrom( expEve );
    return true;
}

bool
EventsTranscoder::_transcode_non_physical( Event *& /*eventPtr*/,
                                           CS::DaqEvent & event ) {
    # if 1
    ExpEventPayload & expEve = experimental_payload_ref();
    # else
    ::sV::events::ExperimentalEvent & expEve_ =
                                        *(eventPtr->mutable_experimental());
    ::na64::events::ExperimentalEvent_Payload expEve;  // XXX
    # warning "need template caches at pipeline level"
    _TODO_  // ^^^ TODO: template caches at pipeline level
    # endif

    expEve.set_is_physical( false );
    switch( event.GetType() ) {
        # define set_nphys_e_type( typeBrief )                                  \
        expEve.set_nonphyseventtype(                                            \
            ::na64::events::ExperimentalEvent_Payload_NonPhysEventType_ ## typeBrief    \
        );
        case CS::DaqEvent::START_OF_RUN : {
            //printf( "StartOfRun" );
            set_nphys_e_type( RunStart );
        } break;
        case CS::DaqEvent::END_OF_RUN : {
            //printf( "EndOfRun" );
            set_nphys_e_type( RunEnd );
        } break;
        case CS::DaqEvent::START_OF_RUN_FILES : {
            //printf( "StartOfRunFiles" );
            set_nphys_e_type( RunFilesStart );
        } break;
        case CS::DaqEvent::END_OF_RUN_FILES : {
            //printf( "EndOfRunFiles" );
            set_nphys_e_type( RunFilesEnd );
        } break;
        case CS::DaqEvent::START_OF_BURST : {
            //printf( "StartOfBurst" );
            set_nphys_e_type( BurstStart );
        } break;
        case CS::DaqEvent::END_OF_BURST : {
            //printf( "EndOfBurst" );
            set_nphys_e_type( BurstEnd );
        } break;
        case CS::DaqEvent::CALIBRATION_EVENT : {
            //printf( "CalibrationEvent" );
            set_nphys_e_type( Calibration );
        } break;
        case CS::DaqEvent::EVENT_FORMAT_ERROR : {
            //printf( "EventFormatError" );
            set_nphys_e_type( FormatError );
            return false;
        } break;
        # if 0
        // TODO: do we need that? Defined in DATE/commonDefs/event.h
        case START_OF_DATA : {
            printf( "StartOfData" );
        } break;
        case END_OF_DATA : {
            printf( "EndOfData" );
        } break;
        case SYSTEM_SOFTWARE_TRIGGER_EVENT : {
            printf( "SystemSoftwareTriggerEvent" );
        } break
        case DETECTOR_SOFTWARE_TRIGGER_EVENT : {
            printf( "DetectorSoftwareTriggerEvent" );
        } break;
        # endif
        default:
            set_nphys_e_type( unknown );
            sV_loge( "Unknown DDD event type code: %d.\n", (int) event.GetType() );
            return false;
    };
    return true;
}

CS::DaqEventsManager &
EventsTranscoder::get_ddd_manager() {
    return *_managerPtr;
}

const CS::DaqEventsManager &
EventsTranscoder::get_ddd_manager() const {
    return *_managerPtr;
}

void
EventsTranscoder::set_up_detector_mapping() {
    const CS::Chip::Maps & maps = get_ddd_manager().GetMaps();
    if( maps.size() ) {
        sV_log2( "Run detector ID mapping corrections for %zu entries...\n",
                 maps.size() );
    } else {
        sV_logw( "DaqEventsManager::GetMaps() returned container of zero "
                 "length. Unable to perform detectros IDs corrections.\n" );
    }
    for( auto it = maps.begin(); maps.end() != it; ++it ) {
        const CS::Chip::Digit * d = it->second;
        // This method corrects the detector numbering table
        // according to one that read out from mapping files.
        resolve_major_detector_number_mapping( d->GetDetID().GetName().c_str(),
                                               d->GetDetID().GetNumber() );
    }
    _detectorsTableValidated = true;
}

}  // namespace aux

// EvManagerHandle
/////////////////

/** Constructs associated instance of DaqEventsManager internally and
 * initializes it with name "afNA64-reader" and prevents it from setting up
 * its signal header.
 *
 * The filename parameter is forward directly to
 * `DaqEventsManager::AddDataSource()` method and thus can accept `@...`
 * sockets when RFIO/libshift support was enabled in DDD build.
 *
 * The `mapsDirName` parameter has to be set for correct event decoding. It
 * will be forwarded to `DaqEventsManager::SetMapsDir()` method.
 * */
EvManagerHandle::EvManagerHandle(
            const std::list<goo::filesystem::Path> & filenames,
            const goo::filesystem::Path & mapsDirPath,
            size_t maxEvents,
            const std::vector<std::string> & omitChips,
            bool keepRaw,
            bool enableProgressbar,
            bool /*nonPhys*/,  // TODO support them!
            bool /*validateDetectorNames*/ ) :
        Parent( 0x0 ),
        EventsTranscoder( mapsDirPath.interpolated(), keepRaw ),
        ASCII_Entry( goo::aux::iApp::exists() ?
                        &goo::app<AbstractApplication>() : nullptr,
                        4 ),
        _lastEvReadingWasGood(false),
        _pbParameters(nullptr),
        _srcfilepaths(filenames.begin(), filenames.end()),
        _maxEventsNumber(maxEvents),
        _eventsRead(0)
{
    // check TODO EventsTranscoder::non_phys_transcode( nonPhys );
    // progressbar stuff
    if( enableProgressbar && _maxEventsNumber ) {
        _pbParameters = new PBarParameters;
        bzero( _pbParameters, sizeof(PBarParameters) );
        _pbParameters->mtrestrict = 250;
        _pbParameters->length = 80;
        _pbParameters->full = _maxEventsNumber;
    }
    for( auto it  = omitChips.begin(); omitChips.end() != it; ++it ) {
        if( "SADC" == *it ) {
            do_omit_chip( eChipSADC );
            sV_log2( "Note: " ESC_CLRYELLOW "SADC" ESC_CLRCLEAR
                " chip won't be transcoded.\n" );
        } else if( "APV" == *it ) {
            do_omit_chip( eChipAPV );
            sV_log2( "Note: " ESC_CLRYELLOW "APV" ESC_CLRCLEAR
                " chip won't be transcoded.\n" );
        } else if( "F1" == *it ) {
            do_omit_chip( eChipF1 );
            sV_log2( "Note: " ESC_CLRYELLOW "F1" ESC_CLRCLEAR
                " chip won't be transcoded.\n" );
        } else {
            sV_logw( "Unknown chip name specified for omission: %s.\n",
                it->c_str() );
        }
    }
}

# if 1
EvManagerHandle::EvManagerHandle( const goo::dict::Dictionary & dct ) :
    EvManagerHandle::EvManagerHandle(
        std::list<goo::filesystem::Path>(
            goo::app<AbstractApplication>().app_options_list<goo::filesystem::Path>("input-file").begin(),
            goo::app<AbstractApplication>().app_options_list<goo::filesystem::Path>("input-file").end() ),
        dct["map-dir"].as<goo::filesystem::Path>().interpolated(),
        goo::app<AbstractApplication>().app_option<size_t>("max-events-to-read"),
        std::vector<std::string>(
            dct["omit-chip"].as_list_of<std::string>().begin(),
            dct["omit-chip"].as_list_of<std::string>().end()
        ),
        dct["keep-raw"].as<bool>(),
        dct["progressbar"].as<bool>(),
        dct["enable-non-phys"].as<bool>(),
        dct["test-det-table"].as<bool>()
    ) {}
# else
EvManagerHandle::EvManagerHandle( const goo::dict::Dictionary & dct )  :
        Parent( 0x0 ),
        EventsTranscoder( dct["map-dir"].as<goo::filesystem::Path>().interpolated() ),
        ASCII_Entry( goo::aux::iApp::exists() ?
                        &goo::app<AbstractApplication>() : nullptr,
                        4 ),
        _lastEvReadingWasGood(false),
        _pbParameters(nullptr),
        _srcfilepaths(
                goo::app<AbstractApplication>().app_options_list<goo::filesystem::Path>("input-file").begin(),
                goo::app<AbstractApplication>().app_options_list<goo::filesystem::Path>("input-file").end() ),
        _maxEventsNumber( goo::app<AbstractApplication>().app_option<size_t>("max-events-to-read") ),
        _eventsRead(0) {
    _TODO_  // TODO
}
# endif

bool
EvManagerHandle::_make_univ_event( Event *& eventPtr ) {
    bool r = EventsTranscoder::transcode( eventPtr, current_event() );
    _update_stat();
    return r;
}

void
EvManagerHandle::_V_print_brief_summary( std::ostream & os ) const {
    os << ESC_CLRGREEN "ddd data source" ESC_CLRCLEAR ":" << std::endl
       << "  events read ................ : " << _eventsRead << " / " << _maxEventsNumber << std::endl
       << "    digits considered ........ : " << n_digits_processed()
            << ", " << ((double)n_digits_processed()/_eventsRead) << " dig-s/ev." << std::endl
       << "    digits unknown/unused .... : " << n_digits_omitted()
            << ", " << 100*((double) n_digits_omitted()) / n_digits_processed() << "%" << std::endl
       << "    digits tr-n unimplem. .... : " << n_digits_omitted_transcoding_unimplemented()
            << ", " << 100*((double) n_digits_omitted_transcoding_unimplemented()) / n_digits_processed() << "%" << std::endl
       << "  non-phys. ev-s omitted ..... : " << n_events_omitted_non_phys()
            << ", " << 100*((double) n_events_omitted_non_phys())/_eventsRead << "%" << std::endl
       << "  unable to decode ........... : " << n_events_decoding_failed()
            << ", " << 100*((double) n_events_decoding_failed())/_eventsRead << "%" << std::endl
       ;
}

bool
EvManagerHandle::_V_is_good() {
    if( _pbParameters ) {
        redraw_pbar(_eventsRead, _pbParameters);
    }
    return _lastEvReadingWasGood && ( _maxEventsNumber != 0 ? _eventsRead < _maxEventsNumber : true );
}

void
EvManagerHandle::_V_next_event( Event *& ePtr ) {
    ePtr = &(::sV::mixins::PBEventApp::c_event());
    ePtr->Clear();
    do {
        try {
            _lastEvReadingWasGood = get_ddd_manager().ReadEvent();
            current_event();
        } catch ( const char * cstr ) {
            _lastEvReadingWasGood = false;
            sV_loge( "Third-party event manager raised a C-string: \"%s\".\n", cstr );
        } catch ( ... ) {
            _lastEvReadingWasGood = false;
            sV_loge( "Third-party event manager raised an unrecognized exception.\n" );
        }
        if( !_lastEvReadingWasGood ) {
            return;
        }
    } while( !_make_univ_event( ePtr ));
    ++_eventsRead;
}

EvManagerHandle::Event *
EvManagerHandle::_V_initialize_reading() {
    for( auto cFileName : _srcfilepaths ) {
        get_ddd_manager().AddDataSource( cFileName.interpolated() );
    }
    # if 0
    if( goo::app<AnalysisPipeline>().verbosity() > 2 ) {
        goo::app<ThreadedAnalysisApplication>().acquire_logstream_mutex(); {
            _manager->Print( goo::app<AnalysisPipeline::Parent>().ls() );
        } goo::app<ThreadedAnalysisApplication>().release_logstream_mutex();
    }
    # endif
    Event * eventPtr;
    _V_next_event( eventPtr );
    return eventPtr;
}

void
EvManagerHandle::_V_finalize_reading() {
    get_ddd_manager().Clear();
}

EvManagerHandle::~EvManagerHandle() {
    if( _pbParameters ) {
        delete _pbParameters;
    }
}

void
EvManagerHandle::_update_stat() {
    if( can_acquire_display_buffer() ) {
        char ** lines = my_ascii_display_buffer();
        assert( lines[0] && lines[1] && lines[2] && lines[3] && !lines[4] );
        const ExpEventPayload & expEve = experimental_payload_ref();

        time_t timeStruct;
        memcpy( &timeStruct, expEve.timestruct().c_str(), sizeof(time_t) );

        char timeStr[32];
        strftime( timeStr, sizeof(timeStr),
              "%Y-%m-%d %H:%M:%S",
              localtime(&timeStruct) );

        snprintf( lines[0], ::sV::aux::ASCII_Display::LineLength,
            "Run # %d, spill# %d, ev# %d (%d in spill)",
            expEve.runno(), expEve.spillno(), expEve.evnoinrun(),
            expEve.evnoinspill() );
        snprintf( lines[1], ::sV::aux::ASCII_Display::LineLength,
            " Event time : %s / %u", timeStr, expEve.timenum() );
        snprintf( lines[2], ::sV::aux::ASCII_Display::LineLength,
            " Evs ...... : %zu, read (< %zu, max), %zu non-phys, %zu dec.fail",
            _eventsRead, _maxEventsNumber,
            n_events_omitted_non_phys(),
            n_events_decoding_failed() );
        snprintf( lines[3], ::sV::aux::ASCII_Display::LineLength,
            " Digits ... : %zu processed / %zu ignored / %zu unimplem",
            n_digits_processed(),
            n_digits_omitted(),
            n_digits_omitted_transcoding_unimplemented() );
    }
}

CS::DaqEvent &
EvManagerHandle::current_event() {
    return get_ddd_manager().GetEvent();
}

const CS::DaqEvent &
EvManagerHandle::current_event() const {
    return get_ddd_manager().GetEvent();
}

StromaV_EVENTS_SEQUENCE_DEFINE_MCONF( EvManagerHandle, "ddd" ) {
    goo::dict::Dictionary dddEvMgrHandleConf( "ddd",
        "A shimming class wrapping the DaqDataDecoding library (part of CORAL). "
        "Implements an iEventSequence interface of StromaV library providing "
        "functions for serial reading of events stored in a chunk file or "
        "transmitted via RFIO socket." );
    dddEvMgrHandleConf.insertion_proxy()
        .p<goo::filesystem::Path>("map-dir",
            "The purpose of map files is to set correspondance between input "
            "channels of front-end electronics cards and experiment detectors "
            "channel (See DaqDataDecoding library docs for further "
            "explaination).", "", goo::app<sV::AbstractApplication>().path_interpolator() )
        .flag("progressbar",
            "Displays simple ASCII progressbar on stdout.")
        .flag("test-det-table",
            "Tests internal detector table. Only useful for debug development.")
        .p<bool>("enable-non-phys",
            "Whether to perform processing of events marked as \"non-physical\".",
            false )
        .list<std::string>("omit-chip",
            //::sV::po::value<std::vector<std::string> >()->multitoken()->zero_tokens(),
            "Chips to be omitted (excluded from further treatment).")
        .flag( "keep-raw",
            "Appends u-event message with raw DDD event data." )
        //("ddd.list-chips",  // TODO
        //    po::value<std::vector<std::string>>(),
        //    "Prints list of supported chips/digits.")
        ;
    goo::dict::DictionaryInjectionMap injM;
        injM( "map-dir",            "analysis.data-sources.na64.ddd.map-dir" )
            ( "progressbar",        "analysis.data-sources.na64.ddd.progressbar" )
            ( "test-det-table",     "analysis.data-sources.na64.ddd.test-det-table" )
            ( "enable-non-phys",    "analysis.data-sources.na64.ddd.enable-non-phys" )
            ( "omit-chip",          "analysis.data-sources.na64.ddd.omit-chip" )
            ( "keep-raw",           "analysis.data-sources.na64.ddd.keep-raw" )
            ;
    return std::make_pair( dddEvMgrHandleConf, injM );
}

//
// Chunk

Chunk::Chunk( std::istream & streamRef,
              const na64ee::ChunkID & sid,
              TypesDictionary & dictRef ) :
                iEventSequence( sV::aux::iEventSequence::randomAccess
                              | sV::aux::iEventSequence::identifiable ),
                md::ddd::MDTraits::iEventSource( sid,
                                                 dictRef,
                                                 iEventSequence::randomAccess
                                               | iEventSequence::identifiable ),
                ASCII_Entry( goo::aux::iApp::exists() ?
                    &goo::app<AbstractApplication>() : nullptr, 4 ),
                _streamRef(streamRef) {}

Chunk::Chunk( std::shared_ptr<std::istream> & streamPtr,
              const na64ee::ChunkID & sid,
              TypesDictionary & dictRef ) : Chunk( *streamPtr, sid, dictRef ) {
    //_streamSharedPtr.reset( *streamPtr );
    _streamSharedPtr = streamPtr;
}


Chunk::~Chunk() {
}

//
// Sequential event iteration
// (implementation of sV::AnalysisPipeline::iEventSequence)

bool
Chunk::_V_is_good() {
    return _streamRef;
}

void
Chunk::_V_next_event( Event *& ) {
    _TODO_  // TODO
}

Chunk::Event *
Chunk::_V_initialize_reading() {
    if( _streamRef ) {
        _streamRef.clear();  // clear EOF flag
        _streamRef.seekg( 0, _streamRef.beg );
    }

    _TODO_  // TODO
}

void
Chunk::_V_finalize_reading() {
    _TODO_  // TODO
}

void
Chunk::_V_print_brief_summary( std::ostream & ) const {
    _TODO_  // TODO
}

//
// Random access support
// (implementation of sV::iEventSource iface)

Chunk::Event *
Chunk::_V_md_event_read_single(
                            const Metadata & /*md*/,
                            const EventID & /*eid*/ ) {
    _TODO_  // TODO
}

std::unique_ptr<sV::aux::iEventSequence>
Chunk::_V_md_event_read_range(
                            const Metadata & /*md*/,
                            const EventID & /*eidFrom*/,
                            const EventID & /*eidTo*/ ) {
    _TODO_  // TODO
}

std::unique_ptr<sV::aux::iEventSequence>
Chunk::_V_md_event_read_list(
                            const Metadata & /*md*/,
                            const std::list<EventID> & /*eidsList*/ ) {
    _TODO_  // TODO
}

//
// Identification support
// (implementation of sV::mixins::iIdentifiableEventSource)

std::string
Chunk::_V_textual_id( const na64ee::ChunkID * chIDPtr ) const {
    if( !chIDPtr ) {
        return "<null>";
    }
    std::ostringstream ss;
    ss << *chIDPtr;
    return ss.str();
}

}  // namespace ddd
}  // namespace dsources
}  // namespace sV

# endif  // defined(StromaV_RPC_PROTOCOLS) && defined(SUPPORT_DDD_FORMAT)

