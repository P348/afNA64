/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* \file sadcWF_supp.c
 *
 * This source searches the characteristic parameters of SADC waveform y_i = y(x_i), e.g.:
 *  - mean value { <f(x)>, x_mean }
 *  - absolute max value { max(f_i(x_i)), x_i }
 *  - max approximated with bins taken into account by
 *    discrepancy criteria
 *
 * For details see \ref SADCWFCharacteristicParameters struct description.
 *
 *  Gnuplot snippet:
 *  \code
 *  plot '/tmp/one.dat' u 1:2 w boxes title 'Waveform',
 *       '/tmp/one.dat' u 1:3 w linespoints title 'WF deriv.',
 *       '/tmp/one.dat' u 1:4 w lines title 'Linearity'
 *  \endcode
 *
 * Note that linearity estimation can be used further as correction weights during
 * minimization of fitting functionals. Linearity criteria turns to 1.
 * when the point is near to linear approximation of its two (TODO:?)
 * neighbours.
 *
 * \see SADCWFCharacteristicParameters
 */

# include "afNA64_config.h"

# ifdef StromaV_ANALYSIS_ROUTINES

# include <stdlib.h>
# include <string.h>
# include <assert.h>
# include <math.h>
# include "analysis/sadcWF_supp.h"
# include <stdio.h>

# ifdef STANDALONE_BUILD
#   include "analysis/sadcWF_fit.h"
# endif

void
allocate_SADCWF_cache( struct SADCWFCharacteristicParameters * p, SADCSamplesSize nnodes ) {
    p->cache = malloc( sizeof(struct SADCWFCache) );
    bzero( p->cache, sizeof(struct SADCWFCache) );
    p->cache->derivatives = malloc( sizeof(SADCSamples)*(nnodes-1) );
    p->cache->linearity = malloc( sizeof(float)*(nnodes) );
}

void
free_SADCWF_cache( struct SADCWFCharacteristicParameters * p, SADCSamplesSize nnodes ) {
    free(p->cache->derivatives);
    free(p->cache);
}

int
calculate_characteristics( const SADCSamples * samples,
                           const SADCSamplesSize nSamples,
                           struct SADCWFCharacteristicParameters * p ) {
    assert( p->cache );
    uint8_t i, nextIdx, prevIdx, wMIdx;
    float deviation, linearityMax;
    double linearityFactor = 0., integralLinearityCoeff = 0.;
    /* Step #1 : calculate sum and derivative */
    p->sum = 0.;
    p->absMaxVal = p->absMinVal = samples[0];
    p->absMaxNBin = p->absMinNBin = 0;
    p->timeDispersion = 0.;
    for( i = 0; i < nSamples; i++ ) {
        p->sum += samples[i];
        if(!i) continue;  /* do not eval further code for first bin */
        p->cache->derivatives[i-1] = samples[i] - samples[i-1];
        if( p->absMaxVal < samples[i] ) {
            p->absMaxVal = samples[i];
            p->absMaxNBin = i;
        } else if( p->absMinVal > samples[i] ) {
            p->absMinVal = samples[i];
            p->absMinNBin = i;
        }
    }
    linearityMax = 2*(fabs(p->absMaxVal) + fabs(p->absMinVal));
    /* Validity checks #1 {{{ */
    {
        if(    !isfinite(p->sum)
            || !isfinite(linearityMax) ) {
            return -3;
        }
        if( p->sum < 0 ) {
            return -2;
        }
        if( linearityMax <= 0 ) {
            return -1;
        }
    }
    /* }}} */
    p->mean = p->sum/nSamples;
    /* Step #2 : calculate weighted mean and linearity */
    p->weightedMean = 0.;
    p->stdDeviation = 0.;
    for( i = 0; i < nSamples; i++ ) {
        p->weightedMean += (samples[i] / p->sum)*i;
        deviation = samples[i] - p->mean;
        p->stdDeviation += deviation*deviation;
        prevIdx = i - 1;
        nextIdx = i + 1;
        if( nextIdx == nSamples ) {
            p->cache->linearity[i] = (1 - fabs(fabs(2*samples[nSamples-2] - samples[nSamples-3]) - samples[i])
                                                / linearityMax);
        } else if( !i ) {
            p->cache->linearity[i] = (1 - fabs(fabs(2*samples[1] - samples[2]) - samples[i])
                                                / linearityMax);
        } else {
            p->cache->linearity[i] = (1 - fabs(fabs(samples[prevIdx] + samples[nextIdx])/2 - samples[i])
                                                / linearityMax);
        }
        # ifndef NDEBUG
        if( !(   p->cache->linearity[i] >= 0.
              && p->cache->linearity[i] <= 1.
              && isfinite(p->cache->linearity[i]) ) ) {
            fprintf( stderr, "Bad linearity value at %d:%e (sum = %e, max = %e, min = %e): %e.\n",
                    (int) i,
                    samples[i],
                    p->sum,
                    p->absMaxVal,
                    p->absMinVal,
                    p->cache->linearity[i] );
            return -1;
        }
        # endif
        linearityFactor += p->cache->linearity[i];
    }
    p->weightedMean /= nSamples-1;
    p->stdDeviation /= nSamples-1;
    linearityFactor /= nSamples;
    p->stdDeviation  = sqrt(p->stdDeviation);
    wMIdx = (uint8_t) (nSamples*p->weightedMean);
    //printf("$%d\n", (int) wMIdx);
    if(wMIdx >= nSamples) { wMIdx = nSamples - 1; }
    p->mpv = samples[wMIdx];
    /* Validity checks #2 {{{ */
    {
        if(    !isfinite(p->weightedMean)
            || !isfinite(p->stdDeviation) ) {
            return -3;
        }
    }
    /* }}} */
    /* Normalize linearity / calc weighted time dispersion */
    for( i = 0; i < nSamples; i++ ) {
        p->timeDispersion = pow( p->weightedMean - i, 2 )*samples[i]/p->mpv;
        //printf( "%e\n", p->timeDispersion );
        p->cache->linearity[i] *= p->cache->linearity[i];
        integralLinearityCoeff += p->cache->linearity[i];
    }
    p->timeDispersion = sqrt(p->timeDispersion);
    p->timeDispersion /= nSamples;
    p->reliability = 1 - nSamples/fabs(p->absMaxVal - p->weightedMean*nSamples);
    p->reliability *= integralLinearityCoeff/nSamples;
    # ifndef NDEBUG
    if( p->reliability > 1. ) {
        fprintf(stderr, "Bad reliability (1 < %e) = [(1 - 32/(%e - %e*32) = %e)*%e/32]\n",
                p->reliability,
                p->absMaxVal, p->weightedMean, p->reliability,
                integralLinearityCoeff );
    }
    # endif
    /* Validity checks #3 {{{ */
    {
        if(    !isfinite(p->timeDispersion)
            || !isfinite(p->reliability) ) {
            return -3;
        }
    }
    /* }}} */
    return 0;
}

# ifdef STANDALONE_BUILD
static SADCSamples
srcSamples[][32] = {{
     0, 0, -4, -1,          -2, -1, -5, -1,
     -3, -1, -5, -1,        -4, -3, -6, 33,
     188, 508, 877, 1138,   1158, 1085, 931, 783,
     620, 489, 371, 284,    211, 163, 121, 97
}, {
     0, 0, 0, -2,   0, -2, 2, 1,
     1, 1, 2, -1,   3,  1, 1, 59,
     355, 844, 1382, 1790,   1900, 1758, 1547, 1285,
     1026, 801, 617, 463,    346,   255, 189,  142
}, {
    0.25, 0.25, 0.25, 0.25,         0.25, 0.25, -0.75, -0.75,
    0.25, 1.25, 0.25, 0.25,         1.25, 0.25, 0.25, 1.25,
    1.25, 4.25, 21.25, 63.25,       67.25, 51.25, 69.25, 92.25,
    138.25, 192.25, 222.25, 225.25, 221.25, 208.25, 181.25, 155.2
}, { /* Interesting one -- pile-up with wrong selected pedestals (low amp.) */
    0.5, 0.5, -0.5, -0.5,       -5.5, -1.5, -9.5, -2.5,
    -11.5, -4.5, -9.5, -1.5,    -9.5, -0.5, -5.5, 3.5,
    -5.5, 4.5, -0.5, 6.5,       0.5,  6.5, 1.5, 7.5,
    -0.5, 6.5, 0.5, 7.5,        0.5, 4.5, -0.5, 5.5
}, {
    0, 0, 2, 0,             0, 0, 1, 0,
    0, -1, -1, 0,           0, -2, 1, 0,
    -1, 0, 4, 6,            18, 32, 45, 59,
    78, 99, 125, 153,       171, 180, 182, 175
}};

int
main(int argc, char * argv[]) {
    const uint8_t nSeriesMax = (int) (sizeof(srcSamples)/(32*sizeof(SADCSamples))) - 1;
    if( argc != 4 ) {
        fprintf(stderr, "Syntax:\n\t$ %s <nSample> <supp.output> <fit.output>\n0 <= nSample <= %d\n",
            argv[0],
            (int) nSeriesMax);
        return EXIT_FAILURE;
    }
    const int nSourceSamplesSeries = atoi(argv[1]);
    if( nSourceSamplesSeries < 0
            || nSourceSamplesSeries > (int) nSeriesMax ) {
        fprintf( stderr, "Please, specify 0 <= nSample <= %d\n",
            (int) nSeriesMax);
        return EXIT_FAILURE;
    }
    /* supp info pars */
    SADCSamples * tSamples = srcSamples[ nSourceSamplesSeries ];
    SADCSamplesSize nSamples = 32;
    SADCSamplesSize i;
    struct SADCWFCharacteristicParameters p;
    /* fitting pars */
    struct SADCWF_FittingInput sadcWFFitIn = {
            .n = nSamples,
            .samples = tSamples,
            malloc( nSamples*sizeof(double) )
            /*.sigma = p.cache->linearity*/
        };
    union SADCWF_FittingFunctionParameters uFP;
    FILE * suppOutF = fopen( argv[2], "w" ),
         * fitOutF = fopen( argv[3], "w" )
         ;

    allocate_SADCWF_cache( &p, nSamples );
    calculate_characteristics(
            tSamples,
            nSamples,
            &p );

    for( i = 0; i < nSamples; i++ ) {
        fprintf( suppOutF,
                "%d %e %e %e\n",
                (int) i,
                tSamples[i],
                (i ? p.cache->derivatives[i-1] : 0),
                (1-p.cache->linearity[i])*p.absMaxVal );
        sadcWFFitIn.sigma[i] = p.cache->linearity[i];
    }
    fprintf( suppOutF, "# abs. maximum #bin .... %d\n", (int) p.absMaxNBin );
    fprintf( suppOutF, "# mean ................. %e\n", p.mean );
    fprintf( suppOutF, "# weighted mean ........ %e\n", p.weightedMean*nSamples );
    fprintf( suppOutF, "# most probable value .. %e\n", p.mpv );
    fprintf( suppOutF, "# sum .................. %e\n", p.sum );
    fprintf( suppOutF, "# standard deviation ... %e\n", p.stdDeviation );
    fprintf( suppOutF, "# max def reliability .. %e\n", p.reliability );
    fprintf( suppOutF, "# weighted time disp. .. %e\n", p.timeDispersion );

    /* longitudal scale ..... */ uFP.moyalPars.p[0] = p.mpv;
    /* maximum position ..... */ uFP.moyalPars.p[1] = p.weightedMean*nSamples;
    /* width ................ */ uFP.moyalPars.p[2] = 4.44*p.timeDispersion;

    printf( "p1=%e, p2=%e, p3=%e\n",
            uFP.moyalPars.p[0], 
            uFP.moyalPars.p[1],
            uFP.moyalPars.p[2]);  /* XXX */

    int fittingRC = fit_SADC_samples(
            &sadcWFFitIn,
            moyal,
            &uFP,
            fitOutF
        );
    if( fittingRC ) {
        fprintf(stderr, "Fitting error: %d\n", fittingRC);
    }

    free_SADCWF_cache( &p, nSamples );
    fclose( suppOutF );
    fclose( fitOutF );

    return EXIT_SUCCESS;
}
# endif

# endif  /* StromaV_ANALYSIS_ROUTINES */

