/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * Author: Bogdan Vasilishin <togetherwithra@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "analysis/event_interfaces.hpp"

# ifdef ANALYSIS_ROUTINES

//template<> na64::analysis::NA64ExperimentalEvent *
//    ::sV::aux
//    ::iEventPayloadProcessor<na64::analysis::NA64ExperimentalEvent>
//    ::_reentrantExpEventPtr = nullptr;

namespace na64 {
namespace analysis {

static bool
consider_interim_result( sV::aux::iEventProcessor::ProcRes & global,
                         sV::aux::iEventProcessor::ProcRes local,
                         std::vector<int> & discriminatedIndexes,
                         int index ) {
    using sV::aux::iEventProcessor;
    if( (!( local & iEventProcessor::CONTINUE_PROCESSING)) ) {
        // Abort current processing and propagate abort-all flag.
        global &= ~iEventProcessor::CONTINUE_PROCESSING;
        return false;  // break;
    }
    if( local & iEventProcessor::ABORT_CURRENT ) {
        // Abort processing of current event and propagate abort flags.
        global |= iEventProcessor::ABORT_CURRENT;
        return false;  // break;
    }
    if( local & iEventProcessor::DISCRIMINATE ) {
        // Delete sample from set if need.
    }
    return true;
}

iSADCProcessor::ProcRes
iSADCProcessor::_V_process_event_payload( NA64ExperimentalEvent & expEvent ) {
    using sV::aux::iEventProcessor;
    ProcRes result = iEventProcessor::CONTINUE_PROCESSING | iEventProcessor::NOT_MODIFIED;
    if( ! expEvent.sadc_data_size() ) {
        return result;
    }
    std::vector<int> toDelete;
    toDelete.reserve(expEvent.sadc_data_size());
    for( int nSADCData = 0;
             nSADCData < expEvent.sadc_data_size();
             ++nSADCData ) {
        ProcRes locRC = _V_process_SADC_profile_event(
                                *expEvent.mutable_sadc_data(nSADCData) );
        if( !consider_interim_result( result, locRC, toDelete, nSADCData ) ) {
            break;
        }
    }
    if( toDelete.size() == (size_t) expEvent.sadc_data_size() ) {
        // All payloads have to be discriminated.
        expEvent.clear_sadc_data();
    } else if( !toDelete.empty() ) {
        // Delete discriminated payloads: swap all candidates to the end of
        // repeated field and delete subrange.
        int latestIdx = expEvent.sadc_data_size() - 1;
        for( auto it = toDelete.rbegin(); it != toDelete.rend(); ++it ) {
            int idx = *it;
            if( latestIdx != idx ) {
                expEvent.mutable_sadc_data()->SwapElements( idx,  latestIdx );
            }
            --latestIdx;
        }
        expEvent.mutable_sadc_data()->DeleteSubrange(
                                ++latestIdx, toDelete.size() );
    }
    /* unused */ _V_finalize_processing_sadc_event( expEvent );
    return result;
}

iAPVProcessor::ProcRes
iAPVProcessor::_V_process_event_payload( NA64ExperimentalEvent & expEvent ) {
    ProcRes result = RC_ACCOUNTED;
    if( ! expEvent.apv_data_size() ) {
        return result;
    }
    std::vector<int> toDelete;
    toDelete.reserve(expEvent.apv_data_size());
    for( int nAPVData = 0;
             nAPVData < expEvent.apv_data_size();
             ++nAPVData ) {
        ProcRes locRC = _V_process_APV_samples( *expEvent.mutable_apv_data(nAPVData) );
        if( !consider_interim_result( result, locRC, toDelete, nAPVData ) ) {
            break;
        }
    }
    /* unused */ _V_finalize_processing_apv_event( expEvent );
    return result;
}

}  // analysis
}  // namespace na64

# endif  // ANALYSIS_ROUTINES

