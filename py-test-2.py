#!/usr/bin/env python2

from __future__ import print_function

import sys
sys.path.append('/home/crank/Projects/CERN/NA64-meta/install/lib/python2.7/site-packages')

import AFNA64.Processors
import AFNA64.DataSources

import json, tempfile

def ls_objs_dir( mod ):
    ls_ = dir( mod )
    ls = sorted(list(ls_))
    for entry in ls:
        if "cvar" == entry:
            print( "    + cvar" )
            print_c_variables_in_module( mod.cvar )
        elif "_" != entry[0] and "_swigregister" != entry[-13:]:
            print( "    -", entry )

class TestingStore( AFNA64.DataSources.iDDDMetadataStore ):
    def __init__(self):
        super( TestingStore, self ).__init__()

    def get_metadata_for(self, sid):
        print( "get_metadata_for() invoked!" )
        return None

    def erase_metadata_for(self, sid):
        print( "erase_metadata_for() invoked!" )

    def put_metadata(self, sid, md):
        print( "put_metadata() invoked!" )

ls_objs_dir( AFNA64.DataSources.iDDDMetadataStore )

if "__main__" == __name__:
    mdDct = AFNA64.DataSources.MetadataDict()
    dddMdType = AFNA64.DataSources.DDDMetadataType()
    mdDct.register_metadata_type( dddMdType )

    store = TestingStore()
    dddMdType.add_store( store )

    artefact = open( '/data/cern/p348/2016/cdr01005-001445.dat', 'r' )
    c = AFNA64.DataSources.DDDChunk(
        artefact,
        AFNA64.DataSources.ChunkID( 1445, 5 ),
        mdDct )
    md = c.metadata( )

